package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 8/11/2016.
 */
public class TokenPojo implements Cloneable{
    @SerializedName("token")
    private String token;

    public TokenPojo(String token) {
        this.token = token;
    }

    public TokenPojo() {

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    protected TokenPojo clone() throws CloneNotSupportedException {
        return (TokenPojo)super.clone();
    }
}
