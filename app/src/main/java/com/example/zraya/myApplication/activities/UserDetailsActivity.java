package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.adapters.OrderListTabAdapter;
import com.example.zraya.myApplication.beans.UserInfoPojo;

/**
 * Created by Rajesh on 05-Jul-16.
 */
public class UserDetailsActivity extends AppCompatActivity {
    private ViewPager mViewPager;
    public String mUserID;
    public CardView mUserDetailsLayout;
    private OrderListTabAdapter mOrderListAdapter;
    private UserInfoPojo mUserInfoPojo;
    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_orders_list);
        mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);

        setViewPagerDisplay();
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTextColorResource(R.color.color_white);
        setOnclickListener();
    }

    private void setViewPagerDisplay() {
        Intent intent = getIntent();
        mUserID = (String) intent.getSerializableExtra(Constants.USER_ID);
        mUserInfoPojo = (UserInfoPojo) intent.getSerializableExtra(Constants.BUNDLE_ITEM_COLLECTION);

        updateUIUserInfo(mUserInfoPojo);

        String tabTitles[] = new String[]{Constants.TAB_ON_GOING_ORDERS, Constants.TAB_COMPLETED};
        mOrderListAdapter = new OrderListTabAdapter(getFragmentManager());
        mOrderListAdapter.setTabs(tabTitles);
        UserInfoPojo userInfo = new UserInfoPojo();
        mOrderListAdapter.setUserDetails(userInfo);
        mOrderListAdapter.setUserID(mUserID);
        mViewPager.setAdapter(mOrderListAdapter);
    }

    private void updateUIUserInfo(UserInfoPojo aUserInfo) {
        mToolbarTitle.setText(aUserInfo.getUser_name() + " Orders List");
        mUserDetailsLayout = (CardView) findViewById(R.id.card_view);
//        mUserDetailsLayout.setBackgroundColor(getResources().getColor(R.color.black_blue));
    }

    private void setOnclickListener() {
        mToolBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
