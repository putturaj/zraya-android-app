package com.example.zraya.myApplication.activities;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.NavigationDrawer;
import com.example.zraya.myApplication.beans.AdminContentDetailsPojo;

import java.util.HashMap;

/**
 * Created by raj on 26/7/16.
 *
 * @author raj
 */
public class BaseDrawerActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationDrawer navigationDrawer;
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int resourceId) {
        super.setContentView(resourceId);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            setNavigationDrawer(toolbar);
        }
    }

    protected void setNavigationDrawer(Toolbar toolbar) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationDrawer = (NavigationDrawer) findViewById(R.id.nv_navigation_drawer);
        navigationDrawer.setNavigationDrawer(mDrawerLayout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    public void showNavigationDrawer(boolean visible) {
        if (visible) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            getSupportActionBar().setLogo(R.drawable.arrow_back);
            getSupportActionBar().setIcon(R.drawable.arrow_back);
            getSupportActionBar().setHomeButtonEnabled(false); // disable the button
            getSupportActionBar().setDisplayHomeAsUpEnabled(false); // remove the left caret
            getSupportActionBar().setDisplayShowHomeEnabled(true); // remove the icon
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void showAdmincontent(AdminContentDetailsPojo adminContentDetailsPojo) {
        navigationDrawer.showAdminContent(adminContentDetailsPojo);
    }

    public void showUserDetails(HashMap<String, String> aUser) {
        if (!aUser.isEmpty()) {
            navigationDrawer.displayUserInfoInNavigationDrawer(aUser);
        }
    }
}
