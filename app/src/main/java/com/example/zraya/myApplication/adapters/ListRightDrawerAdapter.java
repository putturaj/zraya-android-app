package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.listeners.OnDrawerItemClickListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class ListRightDrawerAdapter extends ArrayAdapter<String> {

    private ArrayList<String> mStatusArrayList;
    Context mContext;
    OnDrawerItemClickListener onDrawerItemClickListener;

    public ListRightDrawerAdapter(ArrayList<String> data, Context context,
                                  OnDrawerItemClickListener aOnDrawerItemClickListener) {
        super(context, R.layout.nav_right_header_main, data);
        this.mStatusArrayList = data;
        this.mContext = context;
        this.onDrawerItemClickListener = aOnDrawerItemClickListener;
    }

    // View lookup cache
    private static class ViewHolder {
        CircleImageView circleImageView;
        TextView name, favAdded;
        ImageView isfav;
        LinearLayout newsItemLayout;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());

            convertView = inflater.inflate(R.layout.nav_right_header_main, parent, false);

            LinearLayout drawerItemsContainer = (LinearLayout) convertView.findViewById(R.id.layout_right_drawer);
            viewHolder.circleImageView = (CircleImageView) convertView.findViewById(R.id.news_btc_img);
            viewHolder.name = (TextView) convertView.findViewById(R.id.news_btc_text);
            viewHolder.isfav = (ImageView) convertView.findViewById(R.id.btn_news_btc_add);
            viewHolder.favAdded = (TextView) convertView.findViewById(R.id.fav_added);
            viewHolder.newsItemLayout = (LinearLayout) convertView.findViewById(R.id
                    .drawer_news_layout_id);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        updateViewHolder(viewHolder, dataModel);
        setOnclickListener(viewHolder, dataModel);
        return convertView;
    }

    private void updateViewHolder(ViewHolder viewHolder, String dataModel) {
        viewHolder.name.setText(dataModel);


//        String newsSiteLogoUrl = dataModel.getNewsSiteImageURL();//
//        if (null != newsSiteLogoUrl && newsSiteLogoUrl != "") {
//            Picasso.with(mContext).load(newsSiteLogoUrl).fit()
//                    .into(viewHolder.circleImageView);
//        } else {
//            Picasso.with(mContext).load(R.drawable.crypto_currency_logo).fit()
//                    .into(viewHolder.circleImageView);
//        }
    }


    private void setOnclickListener(final ViewHolder viewHolder, final String statusSelected) {
        viewHolder.newsItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDrawerItemClickListener.onItemClick(viewHolder.name, statusSelected);
            }
        });


//        viewHolder.isfav.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//            }
//
//                if(dataModel.isFav)
//
//            {
//                viewHolder.favAdded.setVisibility(View.VISIBLE);
//                viewHolder.isfav.setVisibility(View.GONE);
//            } else
//
//            {
//                viewHolder.favAdded.setVisibility(View.GONE);
//                viewHolder.isfav.setVisibility(View.VISIBLE);
//            }
//                onDrawerItemClickListener.onFavClick(viewHolder.isfav,dataModel);
//
//        }
//        });

//        viewHolder.favAdded.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dataModel.isFav = (!dataModel.isFav);
//
//                if (dataModel.isFav) {
//                    viewHolder.favAdded.setVisibility(View.VISIBLE);
//                    viewHolder.isfav.setVisibility(View.GONE);
//                } else {
//                    viewHolder.favAdded.setVisibility(View.GONE);
//                    viewHolder.isfav.setVisibility(View.VISIBLE);
//                }
//                onDrawerItemClickListener.onFavClick(viewHolder.isfav, dataModel);
//            }
//        });
    }


}
