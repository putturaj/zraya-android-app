package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class SofaPojo extends IJRModelData {
    @SerializedName("product_family")
    private String product_family;

    @SerializedName("width")
    private String width;

    @SerializedName("product_type")
    private String product_type;

    @SerializedName("height_sofa")
    private String height_sofa;

    @SerializedName("amount")
    private String amount;

    @SerializedName("id")
    private String id;

    @SerializedName("height_cushion")
    private String height_cushion;

    @SerializedName("frame_color")
    private String frame_color;

    @SerializedName("productid")
    private String productid;

    @SerializedName("cushion_color")
    private String cushion_color;

    @SerializedName("imgurls")
    private String imgurls;

    @SerializedName("cushion_meterial")
    private String cushion_meterial;

    @SerializedName("description")
    private String description;

    @SerializedName("imggif")
    private String imggif;

    @SerializedName("name")
    private String name;

    @SerializedName("base_meterial")
    private String base_meterial;

    @SerializedName("length")
    private String length;

    public String getDescription() {
        return description;
    }

//    public String getImageUrls() {
//        return imageUrls;
//    }
//
//    public void setImageUrls(String imageUrls) {
//        this.imageUrls = imageUrls;
//    }
//
//    @SerializedName("imageUrls")
//    private String imageUrls;

    public String getProduct_family() {
        return product_family;
    }

    public void setProduct_family(String product_family) {
        this.product_family = product_family;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getHeight_sofa() {
        return height_sofa;
    }

    public void setHeight_sofa(String height_sofa) {
        this.height_sofa = height_sofa;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight_cushion() {
        return height_cushion;
    }

    public void setHeight_cushion(String height_cushion) {
        this.height_cushion = height_cushion;
    }

    public String getFrame_color() {
        return frame_color;
    }

    public void setFrame_color(String frame_color) {
        this.frame_color = frame_color;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getCushion_color() {
        return cushion_color;
    }

    public void setCushion_color(String cushion_color) {
        this.cushion_color = cushion_color;
    }

    public String getImgurls() {
        return imgurls;
    }

    public void setImgurls(String imgurls) {
        this.imgurls = imgurls;
    }

    public String getCushion_meterial() {
        return cushion_meterial;
    }

    public void setCushion_meterial(String cushion_meterial) {
        this.cushion_meterial = cushion_meterial;
    }

    public String getDiscription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImggif() {
        return imggif;
    }

    public void setImggif(String imggif) {
        this.imggif = imggif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase_meterial() {
        return base_meterial;
    }

    public void setBase_meterial(String base_meterial) {
        this.base_meterial = base_meterial;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "ClassPojo [product_family = " + product_family + ", width = " + width + ", product_type = " + product_type + ", height_sofa = " + height_sofa + ", amount = " + amount + ", id = " + id + ", height_cushion = " + height_cushion + ", frame_color = " + frame_color + ", productid = " + productid + ", cushion_color = " + cushion_color + ", imgurls = " + imgurls + ", cushion_meterial = " + cushion_meterial + ", description = " + description + ", imggif = " + imggif + ", name = " + name + ", base_meterial = " + base_meterial + ", length = " + length + "]";
    }
}