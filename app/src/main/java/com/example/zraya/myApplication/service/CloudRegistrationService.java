package com.example.zraya.myApplication.service;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.beans.TokenPojo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

public class CloudRegistrationService extends FirebaseInstanceIdService {
    private UserSessionManager userSessionManager;
    private RequestQueue queue;
    private TokenPojo tokenObject;

    public CloudRegistrationService() {
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(final String refreshedToken) {
        System.out.println("Server Generate Token : " + refreshedToken);
        userSessionManager = new UserSessionManager(getApplicationContext());
        userSessionManager.saveToken(getApplicationContext(), refreshedToken);


        queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringPostRequest = new StringRequest(Request.Method.POST, Constants.PATH_TO_SERVER_IMAGE_UPLOAD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                tokenObject = gson.fromJson(response, TokenPojo.class);

                if (null == tokenObject) {
                    Toast.makeText(getApplicationContext(), "Can't send a token to the server", Toast.LENGTH_LONG).show();
                    userSessionManager.saveNotificationSubscription(false);
                } else {
                    userSessionManager.saveNotificationSubscription(true);
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Constants.TOKEN_TO_SERVER, refreshedToken);
                return params;
            }
        };
        queue.add(stringPostRequest);
    }
}
