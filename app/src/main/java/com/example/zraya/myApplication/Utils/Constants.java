package com.example.zraya.myApplication.Utils;

/**
 * Created by Rajesh on 23-Apr-16.
 */
public class Constants {

    public static final String DISPLAY_MESSAGE = "RAJESH : Zraya : ";
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    public static final String FORWARD_SLASH = "/";
    public static final String EQUAL_CHAR = "=";
    public static final String DOT = ".";
    public static final String EXCLAMATION_MARK = "!";
    public static final String COLLON = ":";
    public static final int SPLASH_TIME = 2000;
    public static final String SEPERATOR = ", ";
    public static final String COMMA_CHARACTER = ",";
    public static final String PIPE_CHARACTER = "|";
    public static final String BOOLEAN_TRUE = "true";
    public static final String BOOLEAN_FALSE = "false";

    public static final String RESPONSE_TRUE = "1";
    public static final String RESPONSE_FALSE = "0";
    public static final String RESPONSE_DUPLICATE = "2";

    public static final String ZERO_STRING = "0";

    public static final String USER = "0";
    public static final String ADMIN = "1";
    public static final String MARCHANT = "2";

    //  Sliding Tabs names
    public static final String TAB_ON_GOING_ORDERS = "On-Going";
    public static final String TAB_COMPLETED = "Completed";
    public static final String TAB_CANCELLED = "Cancelled";

    public static final String TAB_USERS = "Users";
    public static final String TAB_ORDERS = "Orders";
    public static final String MERCHANT_LIST = "merchants";


    public static final String TAB_USERS_ORDERS = "user_orders_list";


    // Product Categories Vaalues
    public static final String CATEGORY_CUSTOM_OFFICE_TABLE = "1";
    public static final String CATEGORY_CUSTOM_WHITE_BOARDS = "2";
    public static final String CATEGORY_OFFICE_CHAIR = "3";
    public static final String CATEGORY_OFFICE_TABLE = "4";
    public static final String CATEGORY_WHITEBOARD = "5";
    public static final String CATEGORY_HOME_CHAIR = "6";
    public static final String CATEGORY_SOFAS = "7";
    public static final String CATEGORY_WORKSTATION = "10";
    public static final String CATEGORY_CUSTOM_WORKSTATION = "9";
    public static final String CATEGORY_PEDESTAL = "11";
    public static final String CUSTOM_SOFA = "15";
    public static final String CAFETARIA_TABLE = "16";
    public static final String COFFEE_TABLE = "17";
    public static final String DIRECTOR_DESK = "18";
    public static final String CATEGORY_CUSTOM_ORDER = "19";

    //Default Image Url
    public static final String DEFAULT_OFFICE_TABLE_IMG_URL = "http://zraya.com/img/OfficeTable/3x9.389.png";
    public static final String DEFAULT_WHITE_BOARD_IMG_URL = "http://zraya.com/" +
            "img/Whiteboard/ceramic-glass-board.jpeg";
    public static final String DEFAULT_OFFICE_CHAIR_IMG_URL = "http://zraya" +
            ".com/img/OfficeChair/WEMM 1126.png";
    public static final String DEFAULT_SOFA_IMG_URL = "http://zraya.com/" +
            "img/Sofa/single-arm.jpg";
    public static final String DEFAULT_WORKSTATION_IMG_URL = "http://zraya.com/" +
            "img/workstation-images/ws2.jpeg";
    public static final String DEFAULT_PEDESTAL_IMG_URL = "http://zraya.com/" +
            "img/file-storage/white-wooden-cabinets.jpg";
    public static final String DEFAULT_CAFETARIA_TABLE_IMG_URL = "http://zraya.com/" +
            "img/cofeetable/RectangleCoffeetable.jpg";
    public static final String DEFAULT_COFFEE_TABLE_IMG_URL = "http://zraya.com/" +
            "img/cofeetable/coffee_table_black.png";
    public static final String DEFAULT_DIRECTOR_DESK_IMG_URL = "http://zraya.com/" +
            "img/director_desk/ceo-desk.jpg";


    public static final String TAB_LOCATION = "LOCATION";


    //Ordered Table Manufacture status
    public static final String ITEM_STATUS_ORDERED = "ordered";
    public static final String ITEM_STATUS_ASSIGNED = "assigned";
    public static final String ITEM_STATUS_PURCHASED = "purchase";
    public static final String ITEM_STATUS_CUTTING = "cutting";
    public static final String ITEM_STATUS_ASSEMBLE = "assemble";
    public static final String ITEM_STATUS_POLISH = "polish";
    public static final String ITEM_STATUS_QA = "qa";
    public static final String ITEM_STATUS_DELIVER = "deliver";
    public static final String ITEM_STATUS_COMPLETED = "completed";
    public static final String ITEM_STATUS_CANCELLED = "Cancelled";


    //HTTP End points
    public static final String baseUrl = "http://zraya.com/";
    public static final String ORDER_LIST_ONGOING_URL = "http://zraya.com/AppRequest/getAllOnGoingOrders.php";
    public static final String ORDER_LIST_COMPLETED_URL = "http://zraya.com/AppRequest/getAllCompletedOrders.php";
    public static final String ORDER_LIST_CANCELLED_URL = "http://zraya.com/AppRequest/getAllCancelledOrders.php";
    public static final String ORDER_DETAIL_URL = "http://zraya.com/AppRequest/getOrderStatus.php";
    public static final String GCM_ORDER_DETAIL_URL = "http://zraya.com/AppRequest/getGcmOrderDetals.php";
    public static final String ADMIN_ORDER_DETAIL_URL = "http://zraya.com/AppRequest/GetAdminSearchOrderDetails.php";
    public static final String MARCHANT_ORDER_ONGING_URL = "http://zraya.com/AppRequest/GetMarchantOrdersList.php";
    public static final String MARCHANT_ORDER_COMPLETED_URL = "http://zraya.com/AppRequest/getMerchantCompleteOrderList.php";
    public static final String ORDER_UPDATE_STATUS_URL = "http://zraya.com/AppRequest/UpdateOrderStatus.php";
    public static final String ORDER_ASSIGNING_URL = "http://zraya.com/AppRequest/AssigningOrderMerchant.php";
    public static final String LOGIN_URL = "http://zraya.com/AppRequest/LoginFromApp.php";
    public static final String USER_REGISTER_URL = "http://zraya.com/AppRequest/UserRegister.php";
    public static final String MERCHANT_REGISTER_URL = "http://zraya.com/AppRequest/MerchantRegister.php";
    public static final String USER_LIST_URL = "http://zraya.com/AppRequest/UsersList.php";
    public static final String ADMIN_ORDERS_URL = "http://zraya.com/AppRequest/AdminOrdersList.php";
    public static final String FORGOT_PASSWORD_URL = "http://zraya.com/AppRequest/ForgotPassword.php";
    public static final String RESET_PASSWORD_URL = "http://zraya.com/AppRequest/ResetPassword.php";
    public static final String GET_PRODUCT_STATUS_COMMENT = "http://zraya.com/AppRequest/getProductStatusComment.php";
    public static final String RETRIEVE_ADMIN_FULL_DETAILS = "http://zraya.com/AppRequest/getAdminContentValues.php";
    public static final String MERCHANT_LIST_DETAILS_URL = "http://zraya.com/AppRequest/getWorkstationListDetails.php";
    public static final String LOGIN_URL_DUP = "http://zraya.com/getCustomisingOfficeTable.php";
    public static final String USER_ORDERS_LIST_URL = "http://zraya.com/DBoperation/AppParentOrderList.php";

    public static final String VIDEO_TYPE = "VIDEO";
    public static final String BUNDLE_ITEM_COLLECTION = "bundle_item_collection";
    public static final String BUNDLE_ITEM = "bundle_item";
    public static final String BUNDLE_ORDER_DATA = "bundle_order_data";
    public static final String BUNDLE_STATUS = "bundle_status";
    public static final String BUNDLE_ORDER_ID = "bundle_order_id";
    public static final String BUNDLE_IMAGE_ARRAY = "bundle_images";
    public static final String BUNDLE_ASSIGN_ORDER = "assign_order";


    public static final String ORDER_ID = "order_id";
    public static final String PRODUCT_ID = "product_id";
    //    Sending user Data Constants
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PWD = "user_pwd";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHONE = "user_phNo";
    public static final String USER_LOCATION = "user_location";
    public static final String USER_IS_ADMIN = "user_isAdmin";
    public static final String STATUS = "status";
    public static final String ORDER_CATEGORY = "order_category";
    public static final String ORDER_POSITOIN = "order_position";
    public static final String IS_ASSIGN = "is_assign";

    //    Merchant info Variables
    public static final String MERCHANT_ID = "marchant_id";
    public static final String MERCHANT_EMAIL = "marchant_username";
    public static final String MERCHANT_PWD = "marchant_pwd";
    public static final String MERCHANT_NAME = "marchant_name";
    public static final String MERCHANT_PHONE = "marchant_Phno";
    public static final String MERCHANT_OWNER_NAME = "marchant_ownerName";
    public static final String MERCHANT_LOCATION = "marchant_location";
    public static final String MERCHANT_ADDRESS = "marchant_address";
    public static final String MERCHANT_START_TIME = "marchant_starttime";
    public static final String MERCHANT_END_TIME = "marchant_endtime";
    public static final String MERCHANT_CONTACT_NUMBER = "marchant_contactPerson";

    public static final String REG_TYPE_USER = "User";
    public static final String REG_TYPE_MARCHANT = "Workstation";


    public static final String ORDER_PRODUCT_ID = "order_product_id";

    //    Order status data constants
    public static final String STATUS_ORDER_ID = "orderId";
    public static final String STATUS_STATUS = "status";
    public static final String STATUS_COMMENT = "comment";
    public static final String STATUS_DATE = "date";
    public static final String ORDERS_DATA = "orders_data";

    //    HTTP Response code
    public static final int NO_INTERNET = 0;
    public static final int NO_CONTENT = 204;
    public static final int BAD_REQUEST = 400;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int BAD_GATEWAY = 502;
    public static final int SERVICE_UNAVAILABLE = 503;
    public static final int GATEWAY_TIMEOUT = 504;

    // Reset Password Constants
    public static final String RESET_PASSWORD_TOKEN = "reset_pwd_token";
    public static final String RESET_PASSWORD = "reset_pwd";

    //    Display Pages
    public static final String LOGIN_PAGE = "Login Page";
    public static final int NO_INTERNET_OK = 1;

    //    Default Images for Display
    public static final String zrayaLogo = "http://zraya.com/img/logo_white.webp";
    public static final String zrayaLogo_black = "http://zraya.com/img/logo_black.webp";
    public static final String TOKEN_ID = "token_id";


    public static final String PATH_TO_SERVER_IMAGE_UPLOAD = "http://zraya.com/gcm/register.php";
    public static final String TOKEN_TO_SERVER = "server_token";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    //    number Constants days
    public static final int TEN_DAYS = 10;
    public static final int ZERO_DAYS = 0;
    public static final int ONE_DAYS = 1;
    public static final int TWO_DAYS = 2;
    public static final int THREE_DAYS = 3;
    public static final int FOUR_DAYS = 4;
    public static final int FIVE_DAYS = 5;
    public static final int SIX_DAYS = 6;
    public static final int SEVEN_DAYS = 7;
    public static final int EIGHT_DAYS = 8;
    public static final int NINE_DAYS = 9;


    public static final String DATE_FORMAT_NEWS_PUBLISH_INPUT = "EEE, dd MMM yyyy HH:mm:ss +SSSS";
    public static final String DATE_FORMAT_DISPLAY_OUTPUT = "EEE, dd MMM yyyy HH:mm";


    public static final String INPUT_DATE_FORMAT = "dd-MM-yyyy hh:mma";
    public static final String DATE_FORMAT = "dd";
    public static final String MONTH_FORMAT = "MMM";

    //    GCM Variables
    public static final String GCM_ORDER_TITLE = "gcm_order_title";
    public static final String GCM_ORDER_ID = "gcm_order_id";
    public static final String GCM_ORDER_NAME = "gcm_order_name";
    public static final String GCM_ORDER_STATUS = "gcm_order_status";

    public static final String ORDER_ASSIGNED = "1";
    public static final String ORDER_NOT_ASSIGNED = "0";


    public final static String[] status = {
            Constants.ITEM_STATUS_ORDERED,
            Constants.ITEM_STATUS_ASSIGNED,
            Constants.ITEM_STATUS_PURCHASED,
            Constants.ITEM_STATUS_CUTTING,
            Constants.ITEM_STATUS_ASSEMBLE,
            Constants.ITEM_STATUS_POLISH,
            Constants.ITEM_STATUS_QA,
            Constants.ITEM_STATUS_DELIVER,
            Constants.ITEM_STATUS_COMPLETED,
            Constants.ITEM_STATUS_CANCELLED
    };
}
