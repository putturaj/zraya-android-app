package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Raj on 19-Dec-16.
 */

public class MerchantDetailsPojo implements Serializable {
    @SerializedName("merchant_info")
    private MarchantInfoPojo mMarchantInfo;
    @SerializedName("user_id")
    private String mUserID;
    @SerializedName("order_count")
    private String mOrderCount;

    public MarchantInfoPojo getmMarchantInfo() {
        return mMarchantInfo;
    }

    public void setmMarchantInfo(MarchantInfoPojo mMarchantInfo) {
        this.mMarchantInfo = mMarchantInfo;
    }

    public String getmUserID() {
        return mUserID;
    }

    public void setmUserID(String mUserID) {
        this.mUserID = mUserID;
    }

    public String getmOrderCount() {
        return mOrderCount;
    }

    public void setmOrderCount(String mOrderCount) {
        this.mOrderCount = mOrderCount;
    }
}
