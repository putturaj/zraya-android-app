package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.StringUtil;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.ResetPasswordPresenter;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Raj on 10/6/2016.
 */
public class ResetPasswordActivity extends BaseDrawerActivity implements View.OnClickListener {
    EditText mPasswordNew, mPwdDuplicate;
    Button mSubmitReset;
    private String mResetToken, mResetUsermail, mResetUserID;
    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);
        initializeUIContent();
        Intent intent = getIntent();
        getIntentValues(intent);
        setOnclickListener();
    }

    private void getIntentValues(Intent intent) {
        String action = intent.getAction();
        Uri data = intent.getData();
        System.out.println("Data From URL : " + data);
        mResetToken = data.getQueryParameter("token");
        mResetUsermail = data.getQueryParameter("userEmail");
        mResetUserID = data.getQueryParameter("userID");
        System.out.println("token = " + mResetToken);
        System.out.println("userEmail = " + mResetUsermail);
        System.out.println("userID = " + mResetUserID);
    }

    private void initializeUIContent() {
        mToolbarTitle.setText("Reset Password");
        mPasswordNew = (EditText) findViewById(R.id.pwd_new);
        mPwdDuplicate = (EditText) findViewById(R.id.pwd_new_duplicate);
        mSubmitReset = (Button) findViewById(R.id.reset_pwd_submit);
        mSubmitReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String newPassword = mPasswordNew.getText().toString();
        String duplicatePwd = mPwdDuplicate.getText().toString();
        if (!(Utils.isEmpty(newPassword) && Utils.isEmpty(duplicatePwd))) {
            if (StringUtil.equals(newPassword, duplicatePwd)) {
                Map<String, String> resetValues = new LinkedHashMap<String, String>();
                resetValues.put(Constants.RESET_PASSWORD_TOKEN, mResetToken);
                resetValues.put(Constants.USER_EMAIL, mResetUsermail);
                resetValues.put(Constants.USER_ID, mResetUserID);
                resetValues.put(Constants.RESET_PASSWORD, newPassword);

                ResetPasswordPresenter resetPasswordPresenter = new ResetPasswordPresenter(this);
                resetPasswordPresenter.requestToServer(RequestActionType.RESET_PASSWORD, resetValues);
            } else {
                Toast.makeText(ResetPasswordActivity.this, "Password is Mismatch", Toast.LENGTH_SHORT).show();
            }
        } else if (Utils.isEmpty(newPassword)) {
            Toast.makeText(ResetPasswordActivity.this, "Password is Empty ", Toast.LENGTH_SHORT).show();
        } else if (Utils.isEmpty(duplicatePwd)) {
            Toast.makeText(ResetPasswordActivity.this, "Second Duplicate Password is Empty", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getIntentValues(intent);
    }

    private void setOnclickListener() {
        mToolBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void responseToResetActivity(String aResult) {
        if (aResult.equals(Constants.RESPONSE_TRUE)) {
            System.out.println("Forgot Password response");
            Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(ResetPasswordActivity.this, "Something Went Wrong try Sometime", Toast.LENGTH_SHORT).show();
        }
    }

}
