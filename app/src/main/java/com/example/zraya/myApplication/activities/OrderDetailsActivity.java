package com.example.zraya.myApplication.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.ImageViewPagerAdapter;
import com.example.zraya.myApplication.Utils.PageIndicator;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.CustomRequestPojo;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrderStatusPojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.ProductDisplayMainPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.GcmOrderDetailsPresenter;
import com.example.zraya.myApplication.presenters.GetProductStatusComment;
import com.example.zraya.myApplication.presenters.OrderDetailsPresenter;

import org.apache.poi.util.SystemOutLogger;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rajesh on 29-Apr-16.
 */
public class OrderDetailsActivity extends BaseDrawerActivity {
    Point p;
    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;
    public IJRModelData mIjrmodelData;
    public HorizontalScrollView mHorizontalScrollStatus;
    public OrdersData mOrderData;
    public String mLatestComment;
    public ProductDisplayMainPojo mProductDisplayPojo;
    public String mUserID;
    public TextView mItemName, mItemdesc, mItemLength, mItemWidth, mItemHeight, mItemLegthick, mItemTopmat, mItemBasemat, mItemTopcolor,
            mItemBasecolor, mItemAmount, mSuitable, mCareinstruction, mDrawerInclude, mPartition, mPowerOutletFull, mShape, mQuantity,
            mItemMeterial, mbeidingColor, mTypeLeg, mPowerBoxFour, mPowerBoxSix, mSinglePowerOutlet, mMetalDrawer, mKeyboardDrawer, mMonitorstand,
            ordered, assign, purchase, cutting, assemble, polish, qa, deliver, completed, cancle;
    public TextView mCustReqText, mWorkshopName;
    //    public ImageView mHeaderImage;

    public ArrayList<OrderInfo> mOrderItemInfo = new ArrayList<OrderInfo>();
    public OrderInfo mOrderInfo = null;
    public Button mStatusChange, retry, mAssign_order;
    public LinearLayout mOrderContent, mNoInternet, mLengthwidthLayout, mLegthickLayout, mHeightLayouts, mSuitableLayout, mCareLayout,
            mTabledrawerLayout, mPartitionLayouts, mStatusLayout, mPowercableLayout, mShapeLayouts, mQuantityLayouts,
            mTopmeterialLayouts, mBasemeterialLayout, mMeterialLayout, mColorLayout, mBeidingColorLayout, mLegTypeLayout, mOtherthingsLayout,
            mCustReqDetails;
    public RelativeLayout mProgressContent;

    public Button mAssignOrder;
    ConnectionDetector mConnectionDetector;
    UserSessionManager mSession;
    private ViewPager viewPagerImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        mProgressContent = (RelativeLayout) findViewById(R.id.progress_container);
        mOrderContent = (LinearLayout) findViewById(R.id.content_container);
        mNoInternet = (LinearLayout) findViewById(R.id.no_internet);
        mProgressContent.setVisibility(View.VISIBLE);
        mOrderContent.setVisibility(View.GONE);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);

        mConnectionDetector = new ConnectionDetector(this);
        mSession = new UserSessionManager(getApplicationContext());
        initialiseUI();
        setOnclickListener();
        getItemInfo(getIntent());
    }

    // Get the x and y position after the button is draw on screen
    // (It's important to note that we can't get the position in the onCreate(),
    // because at that stage most probably the view isn't drawn yet, so it will return (0, 0))
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

    /**
     * Created by Rajesh on 16-Aug-16.
     * send product ID and user ID for fetching the details of particular product details
     * gcmOrder_id =  is coming from Notification
     */

    private void getItemInfo(Intent aIntent) {
        OrderDetailsPresenter orderDetailsPresenter = new OrderDetailsPresenter(this);
        Intent intent = aIntent;
        String gcmOrder_id = (String) intent.getSerializableExtra(Constants.GCM_ORDER_ID);

        if (mConnectionDetector.isConnectingToInternet()) {
            //  checking if it is from notification or not
            if (Utils.isEmpty(gcmOrder_id)) {
                mOrderData = (OrdersData) intent.getSerializableExtra(Constants.BUNDLE_ORDER_DATA);
                mIjrmodelData = (IJRModelData) mOrderData.getProductObject();
                mUserID = (String) intent.getSerializableExtra(Constants.USER_ID);
                mOrderInfo = mOrderData.getOrderObj();
                mProductDisplayPojo = mOrderData.getmProductDisplayMainPojo();
                try {
                    loadInfoXML();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                GcmOrderDetailsPresenter gcmOrderDetailsPresenter = new GcmOrderDetailsPresenter(this);
                gcmOrderDetailsPresenter.requestToServer(RequestActionType.ORDER_DETAILS, gcmOrder_id);
            }

        } else {
            Toast.makeText(OrderDetailsActivity.this, "Something is Wrong. Try Again", Toast.LENGTH_SHORT).show();
            mNoInternet.setVisibility(View.VISIBLE);
            mProgressContent.setVisibility(View.GONE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
        }

    }

    private void initialiseUI() {
        mHorizontalScrollStatus = (HorizontalScrollView) findViewById(R.id.horizontal_scroll_id);
        mStatusLayout = (LinearLayout) findViewById(R.id.status_display);
        mLengthwidthLayout = (LinearLayout) findViewById(R.id.lengthwidth_layout);
        mLegthickLayout = (LinearLayout) findViewById(R.id.legthick_layout);
        mHeightLayouts = (LinearLayout) findViewById(R.id.height_layouts);
        mSuitableLayout = (LinearLayout) findViewById(R.id.suitable_layout);
        mCareLayout = (LinearLayout) findViewById(R.id.care_layout);
        mTabledrawerLayout = (LinearLayout) findViewById(R.id.tabledrawer_layout);
        mPartitionLayouts = (LinearLayout) findViewById(R.id.partition_layout);
        mPowercableLayout = (LinearLayout) findViewById(R.id.powercables_layout);
        mShapeLayouts = (LinearLayout) findViewById(R.id.shape_layouts);
        mQuantityLayouts = (LinearLayout) findViewById(R.id.quantity_layouts);
        mTopmeterialLayouts = (LinearLayout) findViewById(R.id.topmeterial_layouts);
        mBasemeterialLayout = (LinearLayout) findViewById(R.id.basemeterial_layouts);
        mMeterialLayout = (LinearLayout) findViewById(R.id.meterial_layout);
        mColorLayout = (LinearLayout) findViewById(R.id.color_layout);
        mBeidingColorLayout = (LinearLayout) findViewById(R.id.beiding_color_layout);
        mOtherthingsLayout = (LinearLayout) findViewById(R.id.other_meterials_layouts);
        mLegTypeLayout = (LinearLayout) findViewById(R.id.type_leg_layout);
        mCustReqDetails = (LinearLayout) findViewById(R.id.custumReq_layout_id);
        mAssignOrder = (Button) findViewById(R.id.assign_order);

        viewPagerImage = (ViewPager) findViewById(R.id.view_pager_image);
        retry = (Button) findViewById(R.id.retry);
        mStatusChange = (Button) findViewById(R.id.status_change);
        mAssign_order = (Button) findViewById(R.id.assign_order);

// mHeaderImage = (ImageView) findViewById(R.id.htab_header);
        mItemName = (TextView) findViewById(R.id.item_name);
        mItemdesc = (TextView) findViewById(R.id.item_desc);
        mItemWidth = (TextView) findViewById(R.id.item_width);
        mItemTopmat = (TextView) findViewById(R.id.item_top_mat);
        mItemBasemat = (TextView) findViewById(R.id.item_base_mat);
        mItemTopcolor = (TextView) findViewById(R.id.item_top_color);
        mItemBasecolor = (TextView) findViewById(R.id.item_base_color);
        mItemLength = (TextView) findViewById(R.id.item_length);
        mItemLegthick = (TextView) findViewById(R.id.item_leg_thick);
        mItemHeight = (TextView) findViewById(R.id.item_height);
        mItemAmount = (TextView) findViewById(R.id.item_amount);
        mSuitable = (TextView) findViewById(R.id.suitable);
        mCareinstruction = (TextView) findViewById(R.id.careinstruction);
        mDrawerInclude = (TextView) findViewById(R.id.drawerinclude);
        mPartition = (TextView) findViewById(R.id.partition);
        mPowerOutletFull = (TextView) findViewById(R.id.poweroutletfull);
        mShape = (TextView) findViewById(R.id.item_shape);
        mQuantity = (TextView) findViewById(R.id.item_quantity);
        mItemMeterial = (TextView) findViewById(R.id.item_mat);
        mbeidingColor = (TextView) findViewById(R.id.beiding_color);
        mTypeLeg = (TextView) findViewById(R.id.leg_type);
        mPowerBoxFour = (TextView) findViewById(R.id.powerboxfour);
        mPowerBoxSix = (TextView) findViewById(R.id.powerboxsix);
        mSinglePowerOutlet = (TextView) findViewById(R.id.singlepoweroutlet);
        mMetalDrawer = (TextView) findViewById(R.id.metaldrawer);
        mKeyboardDrawer = (TextView) findViewById(R.id.keyboarddrawer);
        mMonitorstand = (TextView) findViewById(R.id.monitorstand);
        mCustReqText = (TextView) findViewById(R.id.custReqDetails);
        mWorkshopName = (TextView) findViewById(R.id.workshop_name_id);


        ordered = (TextView) findViewById(R.id.ordered);
        assign = (TextView) findViewById(R.id.assigned);
        purchase = (TextView) findViewById(R.id.purchase);
        cutting = (TextView) findViewById(R.id.cutting);
        assemble = (TextView) findViewById(R.id.assemble);
        polish = (TextView) findViewById(R.id.polish);
        qa = (TextView) findViewById(R.id.qa);
        deliver = (TextView) findViewById(R.id.deliver);
        completed = (TextView) findViewById(R.id.completed);
        cancle = (TextView) findViewById(R.id.cancelled);


    }

    private void loadInfoXML() throws UnsupportedEncodingException {
        if (mConnectionDetector.isConnectingToInternet()) {
            mToolbarTitle.setText(mProductDisplayPojo.getName());
            mProgressContent.setVisibility(View.GONE);
            mOrderContent.setVisibility(View.VISIBLE);
            checkingUserType();
            checkVisibilities();
            if (!Utils.isEmpty(mProductDisplayPojo.getImageUrls())) {
                String imageUrls = mProductDisplayPojo.getImageUrls();
                String[] productSlideImageArray = imageUrls.split(",");
                ImageViewPagerAdapter viewPagerAdapter = new ImageViewPagerAdapter(this, productSlideImageArray, viewPagerImage);
                viewPagerImage.setAdapter(viewPagerAdapter);

                PageIndicator pageIndicator = (PageIndicator) findViewById(R.id.page_indicator);
                pageIndicator.setViewPager(viewPagerImage);
            } else if (!Utils.isEmpty(mProductDisplayPojo.getImgurl())) {
                String imageUrls = mProductDisplayPojo.getImgurl();
                String[] productSlideImageArray = new String[]{imageUrls};
                ImageViewPagerAdapter viewPagerAdapter = new ImageViewPagerAdapter(this, productSlideImageArray, viewPagerImage);
                viewPagerImage.setAdapter(viewPagerAdapter);

                PageIndicator pageIndicator = (PageIndicator) findViewById(R.id.page_indicator);
                pageIndicator.setViewPager(viewPagerImage);
            } else {
                String[] productSlideImageArray = {Constants.zrayaLogo};
                ImageViewPagerAdapter viewPagerAdapter = new ImageViewPagerAdapter(this, productSlideImageArray, viewPagerImage);
                viewPagerImage.setAdapter(viewPagerAdapter);

                PageIndicator pageIndicator = (PageIndicator) findViewById(R.id.page_indicator);
                pageIndicator.setViewPager(viewPagerImage);
            }

            if (null != mOrderInfo.getMarchantInfo()) {
                mWorkshopName.setText("Assigned to " + mOrderInfo.getMarchantInfo()
                        .getMarchant_name() + " Workshop");
            } else {
                mWorkshopName.setText("Not yet Assigned");
            }


            mItemName.setText(mProductDisplayPojo.getName());

            if (!(Utils.isEmpty(mProductDisplayPojo.getDiscription()))) {
                mItemdesc.setVisibility(View.VISIBLE);
                mItemdesc.setText(mProductDisplayPojo.getDiscription());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getShape())) {
                mShape.setVisibility(View.VISIBLE);
                mShape.setText(mProductDisplayPojo.getShape());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getNumberproduct())) {
                mQuantityLayouts.setVisibility(View.VISIBLE);
                mQuantity.setText(mProductDisplayPojo.getNumberproduct());
            }
            if (!(Utils.isEmpty(mProductDisplayPojo.getWidth()) && Utils.isEmpty(mProductDisplayPojo.getLength()))) {
                mLengthwidthLayout.setVisibility(View.VISIBLE);
                mItemWidth.setText(mProductDisplayPojo.getWidth());
                mItemLength.setText(mProductDisplayPojo.getLength());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getHeight())) {
                mHeightLayouts.setVisibility(View.VISIBLE);
                mItemHeight.setText(mProductDisplayPojo.getHeight());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getLegthick())) {
                mLegthickLayout.setVisibility(View.VISIBLE);
                mItemLegthick.setText(mProductDisplayPojo.getLegthick());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getHeight())) {
                mHeightLayouts.setVisibility(View.VISIBLE);
                mItemHeight.setText(mProductDisplayPojo.getHeight());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getTopmaterial())) {
                mTopmeterialLayouts.setVisibility(View.VISIBLE);
                mItemTopmat.setText(mProductDisplayPojo.getTopmaterial());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getBasemeterial())) {
                mBasemeterialLayout.setVisibility(View.VISIBLE);
                mItemBasemat.setText(mProductDisplayPojo.getBasemeterial());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getMeterial())) {
                mMeterialLayout.setVisibility(View.VISIBLE);
                mItemMeterial.setText(mProductDisplayPojo.getMeterial());
            }

            if (!Utils.isEmpty(mProductDisplayPojo.getBasecolor()) && !Utils.isEmpty(mProductDisplayPojo.getTopcolor())) {
                mColorLayout.setVisibility(View.VISIBLE);
                mItemBasecolor.setText(mProductDisplayPojo.getBasecolor());
                mItemTopcolor.setText(mProductDisplayPojo.getTopcolor());
            }


            if (!Utils.isEmpty(mProductDisplayPojo.getBeidingcolor())) {
                mBeidingColorLayout.setVisibility(View.VISIBLE);
                mbeidingColor.setText(mProductDisplayPojo.getBeidingcolor());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getTypeoflegs())) {
                mLegTypeLayout.setVisibility(View.VISIBLE);
                mTypeLeg.setText(mProductDisplayPojo.getTypeoflegs());
            }

            if (!Utils.isEmpty(mProductDisplayPojo.getAmount())) {
                mItemAmount.setText(mProductDisplayPojo.getAmount());
            } else {
                mItemAmount.setText("No Product price");
            }

            if (!Utils.isEmpty(mProductDisplayPojo.getSuitable())) {
                mSuitableLayout.setVisibility(View.VISIBLE);
                mSuitable.setText(mProductDisplayPojo.getSuitable());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getCareinstuction())) {
                mCareLayout.setVisibility(View.VISIBLE);
                mCareinstruction.setText(mProductDisplayPojo.getCareinstuction());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getDrawerinclude())) {
                mTabledrawerLayout.setVisibility(View.VISIBLE);
                mDrawerInclude.setText(mProductDisplayPojo.getDrawerinclude());
            }
            if (!(Utils.isEmpty(mProductDisplayPojo.getPoweroutletfull()) &&
                    !(Utils.isEmpty(mProductDisplayPojo.getEnclosedpowerboxfour())) &&
                    !(Utils.isEmpty(mProductDisplayPojo.getEnclosedpowerboxsix())) &&
                    !(Utils.isEmpty(mProductDisplayPojo.getSinglepoweroutlet())))) {
                mPowercableLayout.setVisibility(View.VISIBLE);
                mPowerBoxFour.setText(mProductDisplayPojo.getEnclosedpowerboxfour());
                mPowerBoxSix.setText(mProductDisplayPojo.getEnclosedpowerboxsix());
                mSinglePowerOutlet.setText(mProductDisplayPojo.getSinglepoweroutlet());
                mPowerOutletFull.setText(mProductDisplayPojo.getPoweroutletfull());
            }
            if (!(Utils.isEmpty(mProductDisplayPojo.getMetaldrawer())) &&
                    !(Utils.isEmpty(mProductDisplayPojo.getKeyboarddrawer())) &&
                    !(Utils.isEmpty(mProductDisplayPojo.getMonitorstand()))) {
                mOtherthingsLayout.setVisibility(View.VISIBLE);
                mMetalDrawer.setText(mProductDisplayPojo.getMetaldrawer());
                mKeyboardDrawer.setText(mProductDisplayPojo.getKeyboarddrawer());
                mMonitorstand.setText(mProductDisplayPojo.getMonitorstand());
            }
            if (!Utils.isEmpty(mProductDisplayPojo.getPartition())) {
                mPartitionLayouts.setVisibility(View.VISIBLE);
                mPartition.setText(mProductDisplayPojo.getPartition());
            }

//            custom Request details Appending to Text View
            if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_ORDER)) {
                CustomRequestPojo customRequestPojo = null;
                mPowercableLayout.setVisibility(View.GONE);
                customRequestPojo = (CustomRequestPojo) mOrderData.getProductObject();
                StringBuffer custReqDetails = new StringBuffer();
                custReqDetails.append("Date Of Delivery Request : " + customRequestPojo.getDod());
                custReqDetails.append("\nRequirements : " + customRequestPojo.getRequirement());
                mCustReqText.setText(custReqDetails.toString());
            }


            assignStatusColor();
            getStatusComment(mOrderData);
        } else {
            Toast.makeText(OrderDetailsActivity.this, "Something is Wrong. Try Again", Toast.LENGTH_SHORT).show();
            mNoInternet.setVisibility(View.VISIBLE);
            mProgressContent.setVisibility(View.GONE);
            retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
        }
    }

    private void assignStatusColor() {
        if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_ORDERED)) {
            ordered.setBackgroundResource(R.drawable.button_login);
            ordered.setTextColor(getResources().getColor(R.color.color_white));
            ordered.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(ordered);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_ASSIGNED)) {
            assign.setBackgroundResource(R.drawable.button_login);
            assign.setTextColor(getResources().getColor(R.color.color_white));
            assign.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(assign);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_ORDERED)) {
            ordered.setBackgroundResource(R.drawable.button_login);
            ordered.setTextColor(getResources().getColor(R.color.color_white));
            ordered.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(ordered);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_CUTTING)) {
            cutting.setBackgroundResource(R.drawable.button_login);
            cutting.setTextColor(getResources().getColor(R.color.color_white));
            cutting.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(cutting);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_PURCHASED)) {
            purchase.setBackgroundResource(R.drawable.button_login);
            purchase.setTextColor(getResources().getColor(R.color.color_white));
            purchase.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(purchase);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_ASSEMBLE)) {
            assemble.setBackgroundResource(R.drawable.button_login);
            assemble.setTextColor(getResources().getColor(R.color.color_white));
            assemble.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(assemble);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_POLISH)) {
            polish.setBackgroundResource(R.drawable.button_login);
            polish.setTextColor(getResources().getColor(R.color.color_white));
            polish.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(polish);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_QA)) {
            qa.setBackgroundResource(R.drawable.button_login);
            qa.setTextColor(getResources().getColor(R.color.color_white));
            qa.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(qa);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_DELIVER)) {
            deliver.setBackgroundResource(R.drawable.button_login);
            deliver.setTextColor(getResources().getColor(R.color.color_white));
            deliver.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(deliver);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_COMPLETED)) {
            completed.setBackgroundResource(R.drawable.button_login);
            completed.setTextColor(getResources().getColor(R.color.color_white));
            completed.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(completed);
        } else if (mOrderInfo.getProductStatus().equals(Constants.ITEM_STATUS_CANCELLED)) {
            cancle.setBackgroundResource(R.drawable.button_login);
            cancle.setTextColor(getResources().getColor(R.color.color_white));
            cancle.setTypeface(null, Typeface.BOLD);
            setStatusClickListener(cancle);
        }
    }

    @Override
    protected void onNewIntent(Intent aIntent) {
        super.onNewIntent(aIntent);
        getItemInfo(aIntent);
    }

    private void setOnclickListener() {
        mStatusChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderDetailsActivity.this, ProductStatusChangeActivity.class);
                intent.putExtra(Constants.BUNDLE_ORDER_ID, mOrderInfo.getOrderId());
                intent.putExtra(Constants.BUNDLE_STATUS, mOrderInfo);
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                startActivity(intent);
                finish();
            }
        });

        mToolBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mAssign_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderDetailsActivity.this, MerchantsFullListActivity.class);
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                intent.putExtra(Constants.BUNDLE_ASSIGN_ORDER, "orderAssign");
                startActivity(intent);
                finish();
            }
        });
    }

    private void checkingUserType() {
        HashMap<String, String> user = mSession.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);
        if (Constants.ADMIN.equals(userType)) {
            mStatusChange.setVisibility(View.VISIBLE);
        } else if (Constants.MARCHANT.equals(userType)) {
            mStatusChange.setVisibility(View.VISIBLE);
        } else if (Constants.USER.equals(userType)) {
            mStatusChange.setVisibility(View.GONE);
        }
    }

    private void checkVisibilities() {
        HashMap<String, String> user = mSession.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);
        if (Constants.ADMIN.equals(userType)) {
            if (mOrderInfo.getIsOrderAssigned().equals(Constants.ORDER_NOT_ASSIGNED)) {
                mStatusLayout.setVisibility(View.GONE);
                mAssignOrder.setVisibility(View.VISIBLE);
            }
        } else {
        }

        if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_ORDER)) {
            mCustReqDetails.setVisibility(View.VISIBLE);

        }
    }


    public void responseFromGCMOrderResponse(ArrayList<OrdersData> aOrderData) {
        OrdersData ordersData = null;
        if (aOrderData.size() > 0) {
            for (int i = 0; i < aOrderData.size(); i++) {
                ordersData = aOrderData.get(i);
                this.mOrderData = aOrderData.get(i);
                HashMap<String, String> user = mSession.getUserDetails();
                mIjrmodelData = (IJRModelData) mOrderData.getProductObject();
                mProductDisplayPojo = mOrderData.getmProductDisplayMainPojo();
                mUserID = user.get(UserSessionManager.USER_ID);
                mOrderInfo = this.mOrderData.getOrderObj();
            }
        }
        OrderDetailsActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                checkingUserType();
                try {
                    loadInfoXML();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getStatusComment(OrdersData aOrdersData) {
        OrderInfo orderInfo = aOrdersData.getOrderObj();
        GetProductStatusComment getProductStatusComment = new GetProductStatusComment(this);
        getProductStatusComment.requestToServer(RequestActionType.ORDER_DETAILS, orderInfo);
    }

    public void responseGetProductStatusComment(ArrayList<OrderStatusPojo> aStatusPojo) {
        OrderStatusPojo statusPojo = null;
        if (aStatusPojo.size() > 0) {
            for (int i = 0; i < aStatusPojo.size(); i++) {
                statusPojo = aStatusPojo.get(i);
                mLatestComment = statusPojo.getStatusComment();
                System.out.println("Last Comment Setted : " + mLatestComment);
            }
        } else {
            mLatestComment = Constants.EMPTY_STRING;
            System.out.println("Last Comment Setted : " + mLatestComment);
        }
    }

    private void setStatusClickListener(TextView mStatusButton) {
        mStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(OrderDetailsActivity.this, p);
            }
        });
    }

    // The method that displays the popup.
    private void showPopup(final Activity context, Point p) {
        TextView productName, comment;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_comment_display);
        productName = (TextView) dialog.findViewById(R.id.productName);
        comment = (TextView) dialog.findViewById(R.id.comment_display);
        Button dismissComment = (Button) dialog.findViewById(R.id.dismiss_comment);
        productName.setText(mProductDisplayPojo.getName());
        if (!Utils.isEmpty(mLatestComment)) {
            comment.setText(mLatestComment);
        } else {
            comment.setText("No Comments");
        }
        dialog.show();
        dismissComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialog.dismiss();
                } catch (Exception e) {
                    System.out.println("Comment Error" + e.toString());
                }
            }
        });
    }

}



