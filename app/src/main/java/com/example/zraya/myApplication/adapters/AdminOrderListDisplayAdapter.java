package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.ViewHolders.CafetariaViewHolder;
import com.example.zraya.myApplication.ViewHolders.CustomRequestViewHolder;
import com.example.zraya.myApplication.ViewHolders.DirectordeskViewHolder;
import com.example.zraya.myApplication.ViewHolders.OfficeChairViewholder;
import com.example.zraya.myApplication.ViewHolders.OfficeTableViewholder;
import com.example.zraya.myApplication.ViewHolders.OrderListViewholder;
import com.example.zraya.myApplication.ViewHolders.PedestalViewholder;
import com.example.zraya.myApplication.ViewHolders.SofaViewholder;
import com.example.zraya.myApplication.ViewHolders.WhiteboardViewholder;
import com.example.zraya.myApplication.ViewHolders.WorkstationViewholder;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrdersData;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class AdminOrderListDisplayAdapter extends ArrayAdapter<OrdersData> {

    private Context mContext;
    /***********
     * Declare Used Variables
     *********/

    private ArrayList data;
    OrdersData mOrderData = null;
    private static LayoutInflater mInflater = null;
    public Resources res;
    public String userID;
    OfficeTablePojo mOfficeTablepojo = null;

    /*************
     * CustomAdapter Constructor
     *****************/
    public AdminOrderListDisplayAdapter(Context aContext, ArrayList<OrdersData> mAdminOrderDataPojo, String mUserId) {
        super(aContext, 0, mAdminOrderDataPojo);
        data = mAdminOrderDataPojo;
        userID = mUserId;
        mInflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = aContext;
    }

    public void setOrderData(ArrayList<OrdersData> mAdminOrderDataPojo){
        data = mAdminOrderDataPojo;
        notifyDataSetChanged();
    }

    /******
     * Depends upon data size called for each row , Create each ListView row
     *****/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        mOfficeTablepojo = null;
        mOrderData = (OrdersData) data.get(position);
        if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_OFFICE_TABLE) ||
                mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_OFFICE_TABLE)) {
            OfficeTableViewholder officeTableHolder;
            itemView = mInflater.inflate(R.layout.order_list_content, null);
            officeTableHolder = new OfficeTableViewholder(itemView, mContext);
            officeTableHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_WORKSTATION)) {
            WorkstationViewholder workStationHolder;
            itemView = mInflater.inflate(R.layout.workstation_order_list_details, null);
            workStationHolder = new WorkstationViewholder(itemView, mContext);
            workStationHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_SOFAS) ||
                mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CUSTOM_SOFA)) {
            SofaViewholder sofaViewholder;
            itemView = mInflater.inflate(R.layout.activity_sofa, null);
            sofaViewholder = new SofaViewholder(itemView, mContext);
            sofaViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_OFFICE_CHAIR)) {
            OfficeChairViewholder officechairViewholder;
            itemView = mInflater.inflate(R.layout.activity_officechair_order, null);
            officechairViewholder = new OfficeChairViewholder(itemView, mContext);
            officechairViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_WHITEBOARD)) {
            WhiteboardViewholder whiteboardViewholder;
            itemView = mInflater.inflate(R.layout.activity_whiteboard_order, null);
            whiteboardViewholder = new WhiteboardViewholder(itemView, mContext);
            whiteboardViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_PEDESTAL)) {
            PedestalViewholder pedestalViewholder;
            itemView = mInflater.inflate(R.layout.activity_pedestal_order, null);
            pedestalViewholder = new PedestalViewholder(itemView, mContext);
            pedestalViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_ORDER)) {
            CustomRequestViewHolder customrequestViewholder;
            itemView = mInflater.inflate(R.layout.activity_custom_upload_order, null);
            customrequestViewholder = new CustomRequestViewHolder(itemView, mContext);
            customrequestViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.CAFETARIA_TABLE)) {
            CafetariaViewHolder cafetariaViewHolder;
            itemView = mInflater.inflate(R.layout.activity_cafetaria_table, null);
            cafetariaViewHolder = new CafetariaViewHolder(itemView, mContext);
            cafetariaViewHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.COFFEE_TABLE)) {
            CafetariaViewHolder cafetariaViewHolder;
            itemView = mInflater.inflate(R.layout.activity_cafetaria_table, null);
            cafetariaViewHolder = new CafetariaViewHolder(itemView, mContext);
            cafetariaViewHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (mOrderData.getOrderObj().getItemCategoryID().equals(Constants.DIRECTOR_DESK)) {
            DirectordeskViewHolder directordeskViewHolder;
            itemView = mInflater.inflate(R.layout.activity_director_desk, null);
            directordeskViewHolder = new DirectordeskViewHolder(itemView, mContext);
            directordeskViewHolder.updateUI(position, (IJRModelData) data.get(position));
        }
        return itemView;
    }



}
