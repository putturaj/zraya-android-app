package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rajesh on 18-May-16.
 */
public class OrderStatusPojo implements Serializable {
    @SerializedName("statusid")
    private String statusID;
    @SerializedName("orderId")
    private String statusOrderID;
    @SerializedName("status")
    private String statusStatus;
    @SerializedName("comment")
    private String statusComment;
    @SerializedName("date")
    private String statusDate;

    public String getStatusID() {
        return statusID;
    }

    public void setStatusID(String statusID) {
        this.statusID = statusID;
    }

    public String getStatusOrderID() {
        return statusOrderID;
    }

    public void setStatusOrderID(String statusOrderID) {
        this.statusOrderID = statusOrderID;
    }

    public String getStatusStatus() {
        return statusStatus;
    }

    public void setStatusStatus(String statusStatus) {
        this.statusStatus = statusStatus;
    }

    public String getStatusComment() {
        return statusComment;
    }

    public void setStatusComment(String statusComment) {
        this.statusComment = statusComment;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }


}
