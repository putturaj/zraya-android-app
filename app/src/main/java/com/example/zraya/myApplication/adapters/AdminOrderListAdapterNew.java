package com.example.zraya.myApplication.adapters;


import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.ZrayaProductsType;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.example.zraya.myApplication.viewfactory.ZrayaProductsViewHolderFactory;

import java.util.ArrayList;

public class AdminOrderListAdapterNew extends RecyclerView.Adapter {
    private Context mContext;
    /***********
     * Declare Used Variables
     *********/

    private ArrayList<OrdersData> data;
    OrdersData mOrderData = null;
    private static LayoutInflater mInflater = null;
    public Resources res;
    public String userID;
    private OnProductItemClickListener mListener;
    OfficeTablePojo mOfficeTablepojo = null;

    public AdminOrderListAdapterNew(Context aContext, ArrayList<OrdersData> mAdminOrderDataPojo,
                                    String mUserId, OnProductItemClickListener listener) {
        data = mAdminOrderDataPojo;
        userID = mUserId;
        mListener = listener;
        mInflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = aContext;
    }

    public void setOrderData(ArrayList<OrdersData> mAdminOrderDataPojo){
        data = mAdminOrderDataPojo;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ZrayaProductsType cardType = ZrayaProductsType.fromIndex(viewType);
        return ZrayaProductsViewHolderFactory.getViewHolder(parent, mListener, cardType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IJRModelData topicObj = (IJRModelData) data.get(position);
        UpdatableViewHolder updatableViewHolder = (UpdatableViewHolder) holder;
        updatableViewHolder.updateUI(position, topicObj);
    }

    @Override
    public int getItemViewType(int position) {
        OrdersData item = data.get(position);
        ZrayaProductsType itemType = ZrayaProductsType.fromIndex(Integer.parseInt(item
                .getOrderObj().getItemCategoryID()));
       return itemType.getIndex();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
