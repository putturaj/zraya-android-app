package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Raj on 7/10/2016.
 */
public class AdminOrderDetailsPojo implements Serializable {
    @SerializedName("user_detail")
    private UserInfoPojo aUserInfoPojo;
    @SerializedName("product_detail")
    private OfficeTablePojo aOfficeTablepojo;

    public UserInfoPojo getaUserInfoPojo() {
        return aUserInfoPojo;
    }

    public void setaUserInfoPojo(UserInfoPojo aUserInfoPojo) {
        this.aUserInfoPojo = aUserInfoPojo;
    }

    public OfficeTablePojo getaOfficeTablepojo() {
        return aOfficeTablepojo;
    }

    public void setaOfficeTablepojo(OfficeTablePojo aOfficeTablepojo) {
        this.aOfficeTablepojo = aOfficeTablepojo;
    }
}
