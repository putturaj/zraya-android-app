package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.ViewHolders.CafetariaViewHolder;
import com.example.zraya.myApplication.ViewHolders.CustomRequestViewHolder;
import com.example.zraya.myApplication.ViewHolders.DirectordeskViewHolder;
import com.example.zraya.myApplication.ViewHolders.OfficeChairViewholder;
import com.example.zraya.myApplication.ViewHolders.OfficeTableViewholder;
import com.example.zraya.myApplication.ViewHolders.PedestalViewholder;
import com.example.zraya.myApplication.ViewHolders.SofaViewholder;
import com.example.zraya.myApplication.ViewHolders.UserOrderListsViewHolder;
import com.example.zraya.myApplication.ViewHolders.WhiteboardViewholder;
import com.example.zraya.myApplication.ViewHolders.WorkstationViewholder;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.ParentOrdersData;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class UserOrdersListDisplayAdapter extends ArrayAdapter<ParentOrdersData> {

    private Context mContext;
    /***********
     * Declare Used Variables
     *********/

    private ArrayList data;
    ParentOrdersData mOrderData = null;
    private static LayoutInflater mInflater = null;
    public Resources res;
    public String userID;
    OfficeTablePojo mOfficeTablepojo = null;

    /*************
     * CustomAdapter Constructor
     *****************/
    public UserOrdersListDisplayAdapter(Context aContext, ArrayList<ParentOrdersData> mAdminOrderDataPojo, String mUserId) {
        super(aContext, 0, mAdminOrderDataPojo);
        data = mAdminOrderDataPojo;
        userID = mUserId;
        mInflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = aContext;
    }

    public void setOrderData(ArrayList<OrdersData> mAdminOrderDataPojo) {
        data = mAdminOrderDataPojo;
        notifyDataSetChanged();
    }

    /******
     * Depends upon data size called for each row , Create each ListView row
     *****/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        mOfficeTablepojo = null;
        mOrderData = (ParentOrdersData) data.get(position);
        UserOrderListsViewHolder userOrderListsHolder;
        itemView = mInflater.inflate(R.layout.activity_user_orders_lists, null);
        userOrderListsHolder = new UserOrderListsViewHolder(itemView, mContext);
        userOrderListsHolder.updateUI(position, mOrderData);
        return itemView;
    }


}
