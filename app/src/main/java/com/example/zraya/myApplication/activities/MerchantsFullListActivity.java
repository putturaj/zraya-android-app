package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.adapters.MerchantListTabAdapter;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.HashMap;

/**
 * Created by Raj on 16-Dec-16.
 */

public class MerchantsFullListActivity extends BaseDrawerActivity {
    private ViewPager mViewPager;
    UserSessionManager mSession;
    MerchantListTabAdapter mWorkstationListTabAdapter;
    TextView mToolbarTitle;
    LinearLayout mToolbarBackTitleLayout;
    public OrdersData mOrderData;
    public String mAssignOrder;

    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workstation_list);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarBackTitleLayout = (LinearLayout) findViewById(R.id.toolbar_back_title_layout);
        mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        mSession = new UserSessionManager(getApplicationContext());
        progressErrorContainer = (RelativeLayout) findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        errorContainer = (RelativeLayout) findViewById(R.id.rv_error_container);
        setupViewPager(mViewPager);
        setOnclickListener();
    }

    private void setupViewPager(ViewPager aViewPager) {
        Intent intent = getIntent();
        mToolbarTitle.setText("Merchanats List");
        HashMap<String, String> user = mSession.getUserDetails();
        String userEmail = user.get(UserSessionManager.USER_EMAIL);
        String userID = user.get(UserSessionManager.USER_ID);

        mWorkstationListTabAdapter = new MerchantListTabAdapter(getFragmentManager());

        mOrderData = (OrdersData) intent.getSerializableExtra(Constants.BUNDLE_ORDER_DATA);
        mAssignOrder = (String) intent.getSerializableExtra(Constants.BUNDLE_ASSIGN_ORDER);

        UserInfoPojo userInfo = new UserInfoPojo();
        mWorkstationListTabAdapter.setUserEmail(userEmail);
        mWorkstationListTabAdapter.setUserID(userID);
        mWorkstationListTabAdapter.setOrderData(mOrderData);
        mWorkstationListTabAdapter.setAssignOrder(mAssignOrder);
        aViewPager.setAdapter(mWorkstationListTabAdapter);

    }

    private void setOnclickListener() {
        mToolbarBackTitleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}