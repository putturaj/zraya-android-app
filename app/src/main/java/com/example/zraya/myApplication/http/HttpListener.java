package com.example.zraya.myApplication.http;

/**
 * Interface for callback from HTTPEngine
 */

public interface HttpListener {
    void handleHttpResponse(HttpRequest httpRequest);

    void handleHttpException(Exception e, HttpRequest httpRequest);
}
