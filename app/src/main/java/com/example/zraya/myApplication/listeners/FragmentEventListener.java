package com.example.zraya.myApplication.listeners;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public interface FragmentEventListener {

    void updateFragment(boolean updateAllFragment);

    boolean isFragmentVisible();

}
