package com.example.zraya.myApplication.beans;

/**
 * Created by Rajesh on 26-Apr-16.
 */

/**
 * Setter and getter of Order IDs.
 *
 * @param <K>
 */
public interface Entity<K> {
    K getOrderId();

    void setOrderId(K id);
}
