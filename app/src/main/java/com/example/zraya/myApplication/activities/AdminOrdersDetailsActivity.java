package com.example.zraya.myApplication.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.beans.AdminOrderDetailsPojo;
import com.example.zraya.myApplication.beans.AdminOrderListPojo;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.AdminOrderDetailsPresenter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rajesh on 29-Apr-16.
 */
public class AdminOrdersDetailsActivity extends Activity {
    public AdminOrderListPojo mAdminOrderListPojo;
    public String mUserID;
    public TextView mItemName, mItemdesc, mItemTopmat, mItemBasemat, mItemAmount, mItemTopcolor, mItemBasecolor, mUserName, mUserPh, mUserEmail, mUserOrdersPending,
            mItemLength, mItemWidth, mItemHeight, mItemLegthick, ordered, purchase, cutting, assemble, polish, qa, deliver, completed;
    public ArrayList<OrderInfo> mOrderItemInfo = new ArrayList<OrderInfo>();
    public AdminOrderDetailsPojo mAdminOrderDetails = null;
    public UserInfoPojo mUserInfoPojo;
    public OfficeTablePojo mOfficeTableInfoPojo;
    public Button mStatusChange;
    public LinearLayout mOrderContent;
    public RelativeLayout mProgressContent;
    UserSessionManager mSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_display_order_details);
        mProgressContent = (RelativeLayout) findViewById(R.id.progress_container);
        mOrderContent = (LinearLayout) findViewById(R.id.content_container);
        mProgressContent.setVisibility(View.VISIBLE);
        mOrderContent.setVisibility(View.GONE);
        mSession = new UserSessionManager(getApplicationContext());
        setInfoXML();
        checkUserType();
        setOnclickListener();
        getItemInfo();
//        setInfoXML();
//        setOnclickListener();
    }

    private void checkUserType() {
        if (!mSession.checkLogin()) {
            HashMap<String, String> user = mSession.getUserDetails();
            String userType = user.get(UserSessionManager.USER_IS_ADMIN);
            if (userType.equals(Constants.USER)) {
                mStatusChange.setVisibility(View.GONE);
            } else if (userType.equals(Constants.ADMIN)) {
                mStatusChange.setVisibility(View.VISIBLE);
            } else if (userType.equals(Constants.MARCHANT)) {
                mStatusChange.setVisibility(View.VISIBLE);
            }
        }
    }


    private void getItemInfo() {
        AdminOrderDetailsPresenter adminOrderDetailsPresenter = new AdminOrderDetailsPresenter(this);
        Intent intent = getIntent();
        mAdminOrderListPojo = (AdminOrderListPojo) intent.getSerializableExtra(Constants.BUNDLE_ITEM_COLLECTION);
        mUserID = (String) intent.getSerializableExtra(Constants.USER_ID);
        adminOrderDetailsPresenter.requestToServer(RequestActionType.ORDER_DETAILS, "" + mAdminOrderListPojo.getProductID(), mUserID);
    }


    public void responseFromServer(ArrayList<AdminOrderDetailsPojo> mAdminorderList) {
        for (int i = 0; i < mAdminorderList.size(); i++) {
            mAdminOrderDetails = mAdminorderList.get(i);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadInfoXML();
            }
        });
    }

    private void setInfoXML() {
        mStatusChange = (Button) findViewById(R.id.status_change);
        mItemName = (TextView) findViewById(R.id.item_name);
        mItemdesc = (TextView) findViewById(R.id.item_desc);
        mItemWidth = (TextView) findViewById(R.id.item_width);
        mItemTopmat = (TextView) findViewById(R.id.item_top_mat);
        mItemBasemat = (TextView) findViewById(R.id.item_base_mat);
        mItemTopcolor = (TextView) findViewById(R.id.item_top_color);
        mItemBasecolor = (TextView) findViewById(R.id.item_base_color);
        mItemLength = (TextView) findViewById(R.id.item_length);
        mItemLegthick = (TextView) findViewById(R.id.item_leg_thick);
        mItemHeight = (TextView) findViewById(R.id.item_height);
        mItemAmount = (TextView) findViewById(R.id.item_amount);

        mUserName = (TextView) findViewById(R.id.user_name);
        mUserPh = (TextView) findViewById(R.id.user_phNum);
        mUserEmail = (TextView) findViewById(R.id.user_email);
        mUserOrdersPending = (TextView) findViewById(R.id.user_no_orders);

        ordered = (TextView) findViewById(R.id.ordered);
        purchase = (TextView) findViewById(R.id.purchase);
        cutting = (TextView) findViewById(R.id.cutting);
        assemble = (TextView) findViewById(R.id.assemble);
        polish = (TextView) findViewById(R.id.polish);
        deliver = (TextView) findViewById(R.id.deliver);
        completed = (TextView) findViewById(R.id.completed);
    }

    private void loadInfoXML() {
        mUserInfoPojo = mAdminOrderDetails.getaUserInfoPojo();
        mOfficeTableInfoPojo = mAdminOrderDetails.getaOfficeTablepojo();

        mProgressContent.setVisibility(View.GONE);
        mOrderContent.setVisibility(View.VISIBLE);

        HashMap<String, String> user = mSession.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);
        if (Constants.ADMIN.equals(userType)) {
            mStatusChange.setVisibility(View.VISIBLE);
        }

        mItemName.setText(mOfficeTableInfoPojo.getTableName());
        mItemdesc.setText(mOfficeTableInfoPojo.getTableDesc());
        mItemWidth.setText(mOfficeTableInfoPojo.getTableWidth());
        mItemLength.setText(mOfficeTableInfoPojo.getTableLength());
        mItemLegthick.setText(mOfficeTableInfoPojo.getTableLegThick());
        mItemHeight.setText(mOfficeTableInfoPojo.getTableHeight());
        mItemTopmat.setText(mOfficeTableInfoPojo.getTableTopMat());
        mItemBasemat.setText(mOfficeTableInfoPojo.getTableBaseMat());
        mItemBasecolor.setText(mOfficeTableInfoPojo.getTableBaseColor());
        mItemTopcolor.setText(mOfficeTableInfoPojo.getTableTopColor());
        mItemAmount.setText(mOfficeTableInfoPojo.getTableAmount());
        mUserName.setText(mUserInfoPojo.getUser_name());
        mUserEmail.setText(mUserInfoPojo.getUser_email());
        mUserPh.setText(mUserInfoPojo.getUser_phNo());
    }

    private void setOnclickListener() {
        mStatusChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminOrdersDetailsActivity.this, ProductStatusChangeActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mOfficeTableInfoPojo);
                intent.putExtra(Constants.BUNDLE_ORDER_ID, mAdminOrderListPojo.getOrderId());
                intent.putExtra(Constants.BUNDLE_STATUS, mAdminOrderListPojo.getOrderStatus());
                startActivity(intent);
                finish();
            }
        });

    }

}



