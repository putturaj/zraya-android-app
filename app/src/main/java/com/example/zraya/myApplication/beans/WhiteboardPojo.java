package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class WhiteboardPojo extends IJRModelData {
    @SerializedName("amount")
    private String amount;

    @SerializedName("id")
    private String id;

    @SerializedName("height")
    private String height;

    @SerializedName("productid")
    private String productid;

    @SerializedName("imgurls")
    private String imgurls;

    @SerializedName("meterial")
    private String meterial;

    @SerializedName("width")
    private String width;

    @SerializedName("name")
    private String name;

    @SerializedName("thickness")
    private String thickness;

    @SerializedName("bgcolor")
    private String bgcolor;

    @SerializedName("cornertype")
    private String cornertype;

//    @SerializedName("imageUrls")
//    private String imageUrls;
//
//    public String getImageUrls() {
//        return imageUrls;
//    }
//
//    public void setImageUrls(String imageUrls) {
//        this.imageUrls = imageUrls;
//    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getImgurls() {
        return imgurls;
    }

    public void setImgurls(String imgurls) {
        this.imgurls = imgurls;
    }

    public String getMeterial() {
        return meterial;
    }

    public void setMeterial(String meterial) {
        this.meterial = meterial;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThickness() {
        return thickness;
    }

    public void setThickness(String thickness) {
        this.thickness = thickness;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getCornertype() {
        return cornertype;
    }

    public void setCornertype(String cornertype) {
        this.cornertype = cornertype;
    }

    @Override
    public String toString() {
        return "ClassPojo [amount = " + amount + ", id = " + id + ", height = " + height + ", productid = " + productid + ", imgurls = " + imgurls + ", meterial = " + meterial + ", width = " + width + ", name = " + name + ", thickness = " + thickness + ", bgcolor = " + bgcolor + ", cornertype = " + cornertype + "]";
    }
}