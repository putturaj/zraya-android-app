package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeChairPojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by puttu on 24-Mar-17.
 */

public class OfficeChairViewholder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private View parentView;
    private OnProductItemClickListener onProductItemClickListener;

    private View container;
    Context mContext;
    ImageView mImageView;
    UserSessionManager mSession;
    String mIsMerchant;
    OfficeChairPojo mOfficeChairPojo;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;


    TextView mName, mHeight, mWidth,
            mAssinedWorshop, mQuantity, mDeliveryDate, mOrderStatus;


    public OfficeChairViewholder(View itemView, Context aContext) {

        super(itemView);
        container = itemView;
        mContext = aContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mImageView = (ImageView) container.findViewById(R.id.prodImg);
        mName = (TextView) container.findViewById(R.id.name);
        mHeight = (TextView) container.findViewById(R.id.officechair_height);
        mWidth = (TextView) container.findViewById(R.id.officechair_width);
        mDeliveryDate = (TextView) container.findViewById(R.id.delivery_date);
        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
        mOrderStatus = (TextView) itemView.findViewById(R.id.order_status);
    }

    public OfficeChairViewholder(View productView, OnProductItemClickListener viewOnBaseItemClickListener) {
        super(productView);
        this.onProductItemClickListener = viewOnBaseItemClickListener;
        this.parentView = productView;
        this.mContext = productView.getContext();

        mImageView = (ImageView) productView.findViewById(R.id.prodImg);
        mName = (TextView) productView.findViewById(R.id.name);
        mHeight = (TextView) productView.findViewById(R.id.officechair_height);
        mWidth = (TextView) productView.findViewById(R.id.officechair_width);
        mDeliveryDate = (TextView) productView.findViewById(R.id.delivery_date);
        mAssinedWorshop = (TextView) productView.findViewById(R.id.assined_marchant);
        mQuantity = (TextView) productView.findViewById(R.id.quantity);
        mOrderStatus = (TextView) productView.findViewById(R.id.order_status);
    }
    @Override
    public void updateUI(int position, IJRModelData ijrModelData) {
        mOrderData = (OrdersData) ijrModelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mOfficeChairPojo = (OfficeChairPojo) mOrderData.getProductObject();

        Date date = Utils.getDate(orderInfo.getDate());
        mName.setText(mOfficeChairPojo.getName());
        mHeight.setText(mOfficeChairPojo.getHeight());
        mWidth.setText(mOfficeChairPojo.getWidth());


        if (null != mOfficeChairPojo.getImageURL()) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mOfficeChairPojo.getImageURL())).into(mImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.DEFAULT_OFFICE_CHAIR_IMG_URL).into(mImageView);
        }
        mDeliveryDate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants
                .SEVEN_DAYS)));


        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mOrderStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
        }

        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText('1');
        }

        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mOfficeChairPojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });

    }
}
