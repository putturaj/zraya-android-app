package com.example.zraya.myApplication.Utils;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.zraya.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;


public class ImageFullPageViewPagerAdapter extends PagerAdapter {

    private String[] imageUrls = null;
    private Context mcontext;
    private LayoutInflater mLayoutInflater;
    ViewPager mpager;


    public ImageFullPageViewPagerAdapter(Context context, String[] storeFrontItem, ViewPager mPager) {
        mLayoutInflater = LayoutInflater.from(context);
        mcontext = context;
        imageUrls = storeFrontItem;
        mpager = mPager;
        for (int i = 0; i < imageUrls.length; i++) {
            System.out.println("image " + i + " : " + imageUrls[i]);
        }
    }

    @Override
    public int getCount() {
        int length = imageUrls == null ? 0 : imageUrls.length;
        return length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View view = mLayoutInflater.inflate(R.layout.popup_full_image_display, container, false);
        final ImageView imageView = (ImageView) view.findViewById(R.id.full_page_image);
        final ProgressBar spinner = (ProgressBar) view.findViewById(R.id.loading);
//        spinner.setVisibility(View.GONE);

        int index = mpager.getCurrentItem();

        //  Log.e("position-----",index+"");

        String frontItem = imageUrls[position];
        String fullImageUrl = frontItem;
        if (!TextUtils.isEmpty(fullImageUrl)) {
//            if (URLUtil.isValidUrl(fullImageUrl)) {
            try {
                Picasso.with(mcontext)
                        .load(Utils.getUrlEncode(fullImageUrl))
                        .error(R.drawable.icon_error)
                        .resize(250, 200)
                        //                        .into(imageView);
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                spinner.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                // TODO Auto-generated method stub
                                spinner.setVisibility(View.GONE);
                            }
                        });
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
//            }
        }

        (container).addView(view);
        view.setTag(frontItem);
        return view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        if (view instanceof RelativeLayout) {
            ((RelativeLayout) view).removeAllViews();
        }
        ((ViewPager) container).removeView((View) object);
    }
}


