package com.example.zraya.myApplication.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.adapters.UserOrdersListDisplayAdapter;
import com.example.zraya.myApplication.beans.ParentOrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.listeners.OnDrawerItemClickListener;
import com.example.zraya.myApplication.presenters.UserOrdersListPresenter;

import java.util.ArrayList;

public class UserOrdersListFragment extends Fragment implements View.OnClickListener, OnDrawerItemClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String USER_EMAIL = "abc@gmail.com";
    public static final String USER_ID = "USER_ID";
    private String mPageName;
    private String mUserEmail;
    private String mUserID;

    private RecyclerView mRecyclerView;
    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayouts;
    private ImageView error_image;
    private TextView error_description, error_action_refresh;
    public ListView mListView;
    UserOrdersListPresenter mUserOrdersListPresenter;
    public ArrayList<ParentOrdersData> mParentOrderArr = new ArrayList<ParentOrdersData>();
    UserOrdersListDisplayAdapter mUserOrdersListDisplayAdapter;

    public static UserOrdersListFragment newInstance(String aPageTitle, String aUserEmail, String aUserID) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, aPageTitle);
        args.putString(USER_EMAIL, aUserEmail);
        args.putString(USER_ID, aUserID);
        UserOrdersListFragment fragment = new UserOrdersListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPageName = getArguments().getString(ARG_PAGE);
            mUserEmail = getArguments().getString(USER_EMAIL);
            mUserID = getArguments().getString(USER_ID);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        progressErrorContainer = (RelativeLayout) view.findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayouts = (RelativeLayout) view.findViewById(R.id.rv_error_layout);
        error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        error_description = (TextView) view.findViewById(R.id.tv_error_description);
        error_action_refresh = (TextView) view.findViewById(R.id.tv_error_action_text);
        error_action_refresh.setOnClickListener(this);
        progressErrorContainer.setVisibility(View.VISIBLE);
        mUserOrdersListPresenter = new UserOrdersListPresenter(this);
        if (mPageName.equals(Constants.TAB_USERS_ORDERS)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mUserOrdersListPresenter.requestToServer(RequestActionType.USER_LIST, mUserEmail, mUserID);
        }
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(View view, String ordersData) {

    }

    public void responseUserOrdersFrag(final ArrayList<ParentOrdersData> aParentUsersOrders) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (aParentUsersOrders != null && aParentUsersOrders.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.GONE);
                    mParentOrderArr = aParentUsersOrders;
                    mUserOrdersListDisplayAdapter = new UserOrdersListDisplayAdapter(getActivity(),
                            mParentOrderArr,
                            mUserID);
                    mListView.setAdapter(mUserOrdersListDisplayAdapter);
                } else {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.VISIBLE);
                    if (mPageName.equals(Constants.TAB_USERS)) {
                        error_description.setText("No user Found");
                    } else if (mPageName.equals(Constants.TAB_ORDERS)) {
                        error_description.setText("No orders ");
                    }
                }
            }
        });
    }
}
