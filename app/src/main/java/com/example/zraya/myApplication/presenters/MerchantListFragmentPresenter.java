package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.beans.MerchantDetailsPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.fragments.MerchantListFragment;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class MerchantListFragmentPresenter implements HttpListener {

    MerchantListFragment frag;
    RequestActionType mRequestActionType;
    ArrayList<MerchantDetailsPojo> baseItems;
    PostObjects mPostObjects = new PostObjects();

    public MerchantListFragmentPresenter(MerchantListFragment aFrag) {
        this.frag = aFrag;
    }

    public void requestToServer(RequestActionType aRequestActionType, String aUserID) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;

        switch (aRequestActionType) {
            case WORKSTATION_LIST:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.MERCHANT_LIST_DETAILS_URL;
                break;
            default:
                return;
        }
        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content, "UTF-8");
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();
                ArrayList<MerchantDetailsPojo> merchantDetailsPojos  = (ArrayList<MerchantDetailsPojo>) gson.fromJson(str,
                        new TypeToken<ArrayList<MerchantDetailsPojo>>() {
                        }.getType());
                baseItems = merchantDetailsPojos;
            } catch (Exception e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
        frag.responseWorstationListPresenter(baseItems);
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
