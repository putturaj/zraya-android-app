package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.PedestalPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by puttu on 24-Mar-17.
 */

public class PedestalViewholder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private View parentView;
    private OnProductItemClickListener onProductItemClickListener;

    private View container;
    UserSessionManager mSession;
    String mIsMerchant;

    ImageView mImageView;
    TextView mName, mQuantity, mWidth, mheight,
            mDeliveryDate, mAssinedWorshop, mStatus;

    PedestalPojo mPedestalPojo = null;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public PedestalViewholder(View itemView, Context mContext) {
        super(itemView);
        container = itemView;
        this.mContext = mContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mImageView = (ImageView) container.findViewById(R.id.prodImg);
        mName = (TextView) container.findViewById(R.id.name);
        mWidth = (TextView) container.findViewById(R.id.width);
        mheight = (TextView) container.findViewById(R.id.height);

        mDeliveryDate = (TextView) container.findViewById(R.id.delivery_date);
        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
        mStatus = (TextView) itemView.findViewById(R.id.order_status);
    }

    public PedestalViewholder(View productView, OnProductItemClickListener viewOnBaseItemClickListener) {
        super(productView);
        this.onProductItemClickListener = viewOnBaseItemClickListener;
        this.parentView = productView;
        this.mContext = productView.getContext();

        mImageView = (ImageView) productView.findViewById(R.id.prodImg);
        mName = (TextView) productView.findViewById(R.id.name);
        mWidth = (TextView) productView.findViewById(R.id.width);
        mheight = (TextView) productView.findViewById(R.id.height);
        mDeliveryDate = (TextView) productView.findViewById(R.id.delivery_date);
        mAssinedWorshop = (TextView) productView.findViewById(R.id.assined_marchant);
        mQuantity = (TextView) productView.findViewById(R.id.quantity);
        mStatus = (TextView) productView.findViewById(R.id.order_status);
    }

    @Override
    public void updateUI(int position, IJRModelData ijrModelData) {
        StringBuffer colorField = new StringBuffer();
        mOrderData = (OrdersData) ijrModelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mPedestalPojo = (PedestalPojo) mOrderData.getProductObject();
        colorField.append(mPedestalPojo.getOutercolor() + " outer color, " + mPedestalPojo.getDrawercolor() + " drawer color");
        Date date = Utils.getDate(orderInfo.getDate());
        mName.setText(mPedestalPojo.getName());
        mWidth.setText(mPedestalPojo.getWidth());
        mheight.setText(mPedestalPojo.getHeight());

        if (null != mPedestalPojo.getImgurls()) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mPedestalPojo.getImgurls())).into(mImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.DEFAULT_PEDESTAL_IMG_URL).into(mImageView);
        }
        mDeliveryDate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants
                .SEVEN_DAYS)));


        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
            mStatus.setText("Unassigned");
        }

        //Setting Quantity
        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText('1');
        }

        parentView.setClickable(true);
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mPedestalPojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }
}
