package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class MetalFileStoragePojo extends IJRModelData {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("width")
    private String width;

    @SerializedName("height")
    private String height;

    @SerializedName("drawers_counter")
    private String drawers_counter;

    @SerializedName("outercolor")
    private String outercolor;

    @SerializedName("innercolor")
    private String innercolor;

    @SerializedName("wheelsinclude")
    private String wheelsinclude;

    @SerializedName("lockerInclude")
    private String lockerInclude;

    @SerializedName("amount")
    private String amount;

    @SerializedName("imgurl")
    private String imgurl;

    @SerializedName("productid")
    private String productid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getDrawers_counter() {
        return drawers_counter;
    }

    public void setDrawers_counter(String drawers_counter) {
        this.drawers_counter = drawers_counter;
    }

    public String getOutercolor() {
        return outercolor;
    }

    public void setOutercolor(String outercolor) {
        this.outercolor = outercolor;
    }

    public String getInnercolor() {
        return innercolor;
    }

    public void setInnercolor(String innercolor) {
        this.innercolor = innercolor;
    }

    public String getWheelsinclude() {
        return wheelsinclude;
    }

    public void setWheelsinclude(String wheelsinclude) {
        this.wheelsinclude = wheelsinclude;
    }

    public String getLockerInclude() {
        return lockerInclude;
    }

    public void setLockerInclude(String lockerInclude) {
        this.lockerInclude = lockerInclude;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
}