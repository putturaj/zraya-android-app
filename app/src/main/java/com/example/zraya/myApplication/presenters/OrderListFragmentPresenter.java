package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.BaseItem;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.dao.DBItemSQLDao;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.fragments.OrderListFragment;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class OrderListFragmentPresenter implements HttpListener {

    OrderListFragment frag;
    RequestActionType mRequestActionType;
    ArrayList<OrdersData> baseItems;
    PostObjects mPostObjects = new PostObjects();

    public OrderListFragmentPresenter(OrderListFragment aFrag) {
        this.frag = aFrag;
    }

    public void loadPlaces() {
        DBItemSQLDao database = new DBItemSQLDao(Utils.getApplication());
        database.open();
        ArrayList<BaseItem> baseItems = null;
//        baseItems = database.getAllBaseItems();

        for (int i = 0; i < baseItems.size(); i++) {
            if (frag.pageTabName.equals(Constants.TAB_LOCATION)) {
//                ((BaseItem) baseItems.get(i)).setCardType(NewsCardType.NORMAL);
            } else {
//                ((BaseItem) baseItems.get(i)).setCardType(NewsCardType.WITH_CAT_LOCATION);
            }
        }
    }

    public void requestToServer(RequestActionType aRequestActionType, String aUserID) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createUserIdPostBody(aUserID).toString();

        switch (aRequestActionType) {
            case ORDER_ONGOING:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.ORDER_LIST_ONGOING_URL;
                break;
            case ORDER_COMPLETED:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.ORDER_LIST_COMPLETED_URL;
                break;
            case ORDER_CANCELLED:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.ORDER_LIST_CANCELLED_URL;
                break;
            default:
                return;
        }


        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content, "UTF-8");
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();
                ArrayList<OrdersData> orderItems = (ArrayList<OrdersData>) gson.fromJson(str,
                        new TypeToken<ArrayList<OrdersData>>() {
                        }.getType());
                baseItems = orderItems;
            } catch (Exception e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
        frag.responseServer(baseItems);
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
