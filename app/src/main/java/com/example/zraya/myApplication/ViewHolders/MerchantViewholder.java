package com.example.zraya.myApplication.ViewHolders;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.activities.MerchantOrdersActivity;
import com.example.zraya.myApplication.beans.MerchantDetailsPojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.AssignOrderMerchantPresenter;

import java.util.HashMap;

/**
 * Created by Raj on 9/21/2016.
 */
public class MerchantViewholder {
    public TextView mMerchantName;
    public TextView mMerchantPh;
    public TextView mMerchantOwnerName;
    public TextView mMerchantLoc;
    public TextView mMerchantStartTime;
    public TextView mMerchantEndTime;
    public TextView mMerchantOrderCount;
    public Button mAssignMerchantButton;
    private View container;
    UserSessionManager mSession;
    String mIsMerchant;

    MerchantDetailsPojo mMerchantDetailsPojo = null;
    OrdersData mOrderData = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public MerchantViewholder(View itemView, Context aContext) {
        container = itemView;
        mContext = aContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mMerchantName = (TextView) itemView.findViewById(R.id.merchant_name);
        mMerchantPh = (TextView) itemView.findViewById(R.id.merchant_ph);
        mMerchantOwnerName = (TextView) itemView.findViewById(R.id.merchant_owner_name);
        mMerchantLoc = (TextView) itemView.findViewById(R.id.merchant_loc);
        mMerchantStartTime = (TextView) itemView.findViewById(R.id.start_time);
        mMerchantEndTime = (TextView) itemView.findViewById(R.id.end_time);
        mMerchantOrderCount = (TextView) itemView.findViewById(R.id.merchant_order_count);
        mAssignMerchantButton = (Button) itemView.findViewById(R.id.assign_to_merchant);

    }

    public void updateUI(int position, MerchantDetailsPojo aMerchantDetial, OrdersData mOrdersData) {
        mMerchantDetailsPojo = aMerchantDetial;
        mOrderData = mOrdersData;
        visibilityCheckOut();

        mMerchantName.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_name());
        mMerchantPh.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_Phno());
        mMerchantOwnerName.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_ownerName());
        mMerchantLoc.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_location());
        mMerchantStartTime.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_starttime());
        mMerchantEndTime.setText(mMerchantDetailsPojo.getmMarchantInfo().getMarchant_endtime());
        mMerchantOrderCount.setText(mMerchantDetailsPojo.getmOrderCount());
        mMerchantPh.setClickable(true);

        mMerchantPh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mMerchantDetailsPojo.getmMarchantInfo().getMarchant_Phno() + ""));
                sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mContext.startActivity(sIntent);
            }
        });

        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MerchantOrdersActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mMerchantDetailsPojo);
                mContext.startActivity(intent);
            }
        });
        mAssignMerchantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AssignOrderMerchantPresenter assignOrderMerchantPresenter = new AssignOrderMerchantPresenter(mContext);
                assignOrderMerchantPresenter.requestToServer(RequestActionType.ORDER_STATUS_UPDATE, mOrderData, mMerchantDetailsPojo.getmMarchantInfo().getMarchant_id());
            }
        });
    }

    private void visibilityCheckOut() {
        if (mOrderData != null) {
            if (mOrderData.getOrderObj().getIsOrderAssigned().equals(Constants.ORDER_NOT_ASSIGNED)) {
                mAssignMerchantButton.setVisibility(View.VISIBLE);
            }
        }
    }

}
