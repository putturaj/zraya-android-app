package com.example.zraya.myApplication.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rajesh on 23-Apr-16.
 */
public class DBSQLiteHelper extends SQLiteOpenHelper {

    public static final String ORDER_TABLE = "ORDERS";
    public static final String OFFICE_TABLE = "OFFICETABLE";
    public static final String ORDER_STATUS = "ORDERSTATUS";

    public static String SQLITE_DB_NAME = "raj_orders";
    public static int SQLITE_DB_VERSION = 1;
    private static volatile DBSQLiteHelper instance;


    public DBSQLiteHelper(Context context) {
        super(context, SQLITE_DB_NAME, null, SQLITE_DB_VERSION);
    }

    public static DBSQLiteHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DBSQLiteHelper.class) {
                if (instance == null) {
                    instance = new DBSQLiteHelper(context);
//                    instance.getWritableDatabase();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(getofficeTableSQL());
            db.execSQL(getOrderTableSQL());
            db.execSQL(getOrderStatusTableSQL());
        } catch (Exception e) {
            System.out.println("DBSQLiteHelper : onCreate : Exception : " + e.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + OFFICE_TABLE);
        onCreate(db);
    }

    private String getOrderTableSQL() {
        return "CREATE TABLE IF NOT EXISTS "
                + ORDER_TABLE + "(" + DBSQLiteConstants.ORDER_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.ORDER_PRODUCT_ID
                + " text," + DBSQLiteConstants.ORDER_USER_ID
                + " text," + DBSQLiteConstants.ORDER_CATEGORY_ID
                + " text," + DBSQLiteConstants.ORDER_STATUS
                + " text)";
    }

    private String getofficeTableSQL() {
        return "CREATE TABLE IF NOT EXISTS "
                + OFFICE_TABLE + "(" + DBSQLiteConstants.OFFICE_TABLE_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.OFFICE_TABLE_NAME
                + " text," + DBSQLiteConstants.ORDER_USER_ID
                + " text," + DBSQLiteConstants.OFFICE_TABLE_LENGTH
                + " text," + DBSQLiteConstants.OFFICE_TABLE_WIDTH
                + " text," + DBSQLiteConstants.OFFICE_TABLE_HEIGHT
                + " text," + DBSQLiteConstants.OFFICE_TABLE_LEG_THICK
                + " text," + DBSQLiteConstants.OFFICE_TABLE_DESCRIPTION
                + " text," + DBSQLiteConstants.OFFICE_TABLE_TYPE
                + " text," + DBSQLiteConstants.OFFICE_TABLE_TOP_MATERIAL
                + " text," + DBSQLiteConstants.OFFICE_TABLE_BASE_MATERIAL
                + " text," + DBSQLiteConstants.OFFICE_TABLE_TOP_COLOR
                + " text," + DBSQLiteConstants.OFFICE_TABLE_BASE_COLOR
                + " text," + DBSQLiteConstants.OFFICE_TABLE_IMG_URL
                + " text," + DBSQLiteConstants.OFFICE_TABLE_AMOUNT
                + " text," + DBSQLiteConstants.OFFICE_TABLE_PRODUCT_ID
                + " text)";
    }

    private String getOrderStatusTableSQL() {
        return "CREATE TABLE IF NOT EXISTS "
                + ORDER_STATUS + "(" + DBSQLiteConstants.STATUS_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.STATUS_ORDER_ID
                + " text," + DBSQLiteConstants.STATUS_STATUS
                + " text," + DBSQLiteConstants.STATUS_COMMENT
                + " text," + DBSQLiteConstants.STATUS_DATE
                + " text)";
    }
}
