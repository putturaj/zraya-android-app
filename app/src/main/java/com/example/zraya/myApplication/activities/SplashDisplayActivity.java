package com.example.zraya.myApplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.AlertDialogManager;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.TokenPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.SplashActivityPresenter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rajesh on 30-Jun-16.
 */
public class SplashDisplayActivity extends AppCompatActivity {
    SplashActivityPresenter mSplashPresenter;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    UserSessionManager session;
    LoginActivity mMainActivityVar;
    ConnectionDetector mConnectionCheck;
    AlertDialogManager mAlertDialog;
    Context mContext = this;

    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        mConnectionCheck = new ConnectionDetector(this);
        mAlertDialog = new AlertDialogManager();
        checkPlayServices();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        session = new UserSessionManager(getApplicationContext());
        Utils.setApplicationContext(getApplicationContext());
        mSplashPresenter = new SplashActivityPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mConnectionCheck.isConnectingToInternet()) {
            System.out.println("TOKEN STORED IS:::" + session.getToken());
            if (session.checkLogin()) {
                callMainActivity();
            } else {
                callOrderListActivity();
            }
        } else {
            mAlertDialog.showAlertDialogFinishActivity(SplashDisplayActivity.this, "No Internet", "Please, ON cellular Data Or Wifi !! ", false);
        }
    }

    private void callMainActivity() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashDisplayActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }

    private void callOrderListActivity() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashActivityPresenter splashActivityPresenter = new SplashActivityPresenter(mContext);
                UserInfoPojo aUserInfoPojo = new UserInfoPojo();
                TokenPojo tokenPojo = new TokenPojo();
                HashMap<String, String> user = session.getUserDetails();
                String email = user.get(UserSessionManager.USER_EMAIL);
                String pwd = user.get(UserSessionManager.USER_PWD);
                tokenPojo.setToken(session.getToken());
                aUserInfoPojo.setUser_email(email);
                aUserInfoPojo.setUser_pwd(pwd);
                aUserInfoPojo.setTokenpojo(tokenPojo);
                splashActivityPresenter.requestToServer(RequestActionType.LOGIN, aUserInfoPojo);
            }
        }, Constants.SPLASH_TIME);
    }


    public void responseSplashPresenter(ArrayList<UserInfoPojo> mUserinfo) {
        UserInfoPojo userPojo = null;
        for (int i = 0; i < mUserinfo.size(); i++) {
            userPojo = mUserinfo.get(i);
            session.createUserLoginSession(userPojo);
        }
        checkingUserType(userPojo);
    }

    private void checkingUserType(UserInfoPojo mUserPojo) {
        Intent intent = null;
        if (mUserPojo.getUser_isAdmin().equals(Constants.USER)) {
            intent = new Intent(SplashDisplayActivity.this, OrderListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, mUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, mUserPojo.getUser_id());
        } else if (mUserPojo.getUser_isAdmin().equals(Constants.ADMIN)) {
            intent = new Intent(SplashDisplayActivity.this, AdminOrderListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, mUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, mUserPojo.getUser_id());
        } else if (mUserPojo.getUser_isAdmin().equals(Constants.MARCHANT)) {
            intent = new Intent(SplashDisplayActivity.this, MarchantListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, mUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, mUserPojo.getUser_id());
        }
        startActivity(intent);
        finish();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}
