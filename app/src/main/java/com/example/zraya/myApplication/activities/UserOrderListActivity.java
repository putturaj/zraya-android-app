package com.example.zraya.myApplication.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.fragments.UserOrdersListFragment;
import com.example.zraya.myApplication.listeners.OnDrawerItemClickListener;

import java.util.HashMap;

public class UserOrderListActivity extends BaseDrawerActivity {
    DrawerLayout drawer;
    UserSessionManager session;
    public HashMap<String, String> user;
    UserOrdersListFragment mUserOrdersListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_orders);
        showNavigationDrawer(true);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        mUserOrdersListFragment = UserOrdersListFragment.newInstance(Constants.TAB_USERS_ORDERS, user.get(UserSessionManager.USER_EMAIL), user.get(UserSessionManager.USER_ID));
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, mUserOrdersListFragment,
                Constants.TAB_ORDERS).commit();
        setTitle("User Order Lists");
    }

}
