package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Raj on 9/21/2016.
 */
public class OfficeTableViewholder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private View parentView;
    private OnProductItemClickListener onProductItemClickListener;


    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public ImageView TableImage;
    public TextView mQuantity, mDeliveryDate, mDeliverMonth, mOrderStatus, mAssinedWorshop,
            TableWidth, TableName, TableLength;

    OfficeTablePojo mOfficeTablepojo = null;
//    private View container;
    public UserSessionManager mSession;
    public String mIsMerchant;

    OrdersData mOrdersData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public OfficeTableViewholder(View itemView, Context aContext) {
        super(itemView);
        mContext = aContext;

        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        TableName = (TextView) itemView.findViewById(R.id.office_table_name);
        TableLength = (TextView) itemView.findViewById(R.id.table_length);
        TableWidth = (TextView) itemView.findViewById(R.id.table_width);
        TableImage = (ImageView) itemView.findViewById(R.id.prodImg);
        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
        mOrderStatus = (TextView) itemView.findViewById(R.id.order_status);
        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
        mDeliveryDate = (TextView) itemView.findViewById(R.id.delivery_date);
        mDeliverMonth = (TextView) itemView.findViewById(R.id.delivery_month);

    }

    public OfficeTableViewholder(View productView, OnProductItemClickListener
            viewOnBaseItemClickListener) {
        super(productView);
        this.onProductItemClickListener = viewOnBaseItemClickListener;
        this.parentView = productView;
        this.mContext = productView.getContext();

        TableName = (TextView) productView.findViewById(R.id.office_table_name);
        TableLength = (TextView) productView.findViewById(R.id.table_length);
        TableWidth = (TextView) productView.findViewById(R.id.table_width);
        TableImage = (ImageView) productView.findViewById(R.id.prodImg);
        mAssinedWorshop = (TextView) productView.findViewById(R.id.assined_marchant);
        mOrderStatus = (TextView) productView.findViewById(R.id.order_status);
        mQuantity = (TextView) productView.findViewById(R.id.quantity);
        mDeliveryDate = (TextView) productView.findViewById(R.id.delivery_date);
        mDeliverMonth = (TextView) productView.findViewById(R.id.delivery_month);
    }

    @Override
    public void updateUI(int position, IJRModelData modelData) {
        StringBuffer color = new StringBuffer();
        mOrdersData = (OrdersData) modelData;
        orderInfo = mOrdersData.getOrderObj();
        userInfoPojo = mOrdersData.getUserObj();
        mOfficeTablepojo = (OfficeTablePojo) mOrdersData.getProductObject();

        Date date = Utils.getDate(orderInfo.getDate());
        String deliverDate = Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants.SEVEN_DAYS));

        TableName.setText(mOfficeTablepojo.getTableName());
        TableLength.setText(mOfficeTablepojo.getTableLength());
        TableWidth.setText(mOfficeTablepojo.getTableWidth());

//        TableOrderedDate.setText(Utils.getDateFormat(date));


        mDeliveryDate.setText(Utils.getDateFormatBySeperate(deliverDate, Constants
                .INPUT_DATE_FORMAT, Constants
                .DATE_FORMAT));
        mDeliverMonth.setText(Utils.getDateFormatBySeperate(deliverDate, Constants.INPUT_DATE_FORMAT,
                Constants.MONTH_FORMAT));


        if (null != mOfficeTablepojo.getTableImgUrl()) {
            try {
//                Picasso.with(mContext).load(Utils.getUrlEncode(mOfficeTablepojo.getTableImgUrl())).into(TableImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//            Picasso.with(mContext).load(Constants.DEFAULT_OFFICE_TABLE_IMG_URL).into(TableImage);
        }

        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mOrderStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
            mOrderStatus.setText("Unassigned");
        }


        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText(orderInfo.getQuantity());
        }


//        TableDeliverydate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants.TEN_DAYS)));

        parentView.setClickable(true);
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mOfficeTablepojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrdersData);
                mContext.startActivity(intent);
            }
        });
    }
}
