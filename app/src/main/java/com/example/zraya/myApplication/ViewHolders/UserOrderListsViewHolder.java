package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.ParentOrderInfo;
import com.example.zraya.myApplication.beans.ParentOrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.HashMap;

/**
 * Created by Raj on 9/21/2016.
 */
public class UserOrderListsViewHolder {

    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public TextView mOrderID;
    public TextView mOrderedDate;
    public TextView mOrderUserName;
    public TextView mTotalOrderAmount;

    //    OfficeTablePojo mOfficeTablepojo = null;
    private View container;
    public UserSessionManager mSession;
    public String mIsMerchant;

    ParentOrdersData mOrdersData = null;
    ParentOrderInfo parentOrderinfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public UserOrderListsViewHolder(View itemView, Context aContext) {
        container = itemView;
        mContext = aContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mOrderID = (TextView) itemView.findViewById(R.id.order_id);
        mOrderedDate = (TextView) itemView.findViewById(R.id.ordered_date);
        mOrderUserName = (TextView) itemView.findViewById(R.id.order_user_name);
        mTotalOrderAmount = (TextView) itemView.findViewById(R.id.total_order_amount);
    }

    public void updateUI(int position, ParentOrdersData parentOrdersData) {
        StringBuffer color = new StringBuffer();
        parentOrderinfo = parentOrdersData.getOrderObj();
        userInfoPojo = parentOrdersData.getUserObj();

        mOrderID.setText(parentOrderinfo.getParentOrderID());
        mOrderedDate.setText("10-march-2019");
        mOrderUserName.setText(userInfoPojo.getUser_name());
        mTotalOrderAmount.setText("2000");


        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, parentOrderinfo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                mContext.startActivity(intent);
            }
        });
    }
}
