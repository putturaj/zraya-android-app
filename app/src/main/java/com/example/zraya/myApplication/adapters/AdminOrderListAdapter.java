package com.example.zraya.myApplication.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.zraya.myApplication.beans.BaseItem;
import com.example.zraya.myApplication.beans.BaseItemContainer;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.fragments.AdminOrderListFragment;
import com.example.zraya.myApplication.fragments.OrderListFragment;
import com.example.zraya.myApplication.listeners.OnItemClickListener;
import com.example.zraya.myApplication.listeners.OrderUpdatebleViewHolder;

import java.util.ArrayList;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public class AdminOrderListAdapter extends RecyclerView.Adapter {
    BaseItemContainer placeCollection;
    ArrayList<BaseItem> newsItems;
    Context context;
    OnItemClickListener clickListener;

    public AdminOrderListAdapter(Context context, BaseItemContainer placeCollection,
                                 OnItemClickListener clickListener) {
        this.context = context;
        this.placeCollection = placeCollection;
        this.newsItems = placeCollection.getBaseItems();
        this.clickListener = clickListener;
    }

    public AdminOrderListAdapter(Activity activity, OrderInfo newsList, AdminOrderListFragment clickListener) {

    }

    public void setPlaceCollection(BaseItemContainer placeCollection) {
        this.placeCollection = placeCollection;
        this.newsItems = placeCollection.getBaseItems();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        NewsCardType cardType = NewsCardType.fromIndex(viewType);
//        return NewsHomeViewHolderFactory.getNewsViewHolder(parent, clickListener, cardType);

        return null;
    }

    @Override
    public int getItemViewType(int position) {
//        NewsCardType displayCardType = getContentItem(position).getCardType();
//        if (displayCardType == null) {
//            return -1;
//        }
//        return displayCardType.getIndex();
        return 0;
    }

    public BaseItem getContentItem(int position) {
        return newsItems.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseItem placeObj = newsItems.get(position);
        OrderUpdatebleViewHolder updatableViewHolder = (OrderUpdatebleViewHolder) holder;
        updatableViewHolder.updateViewHolder(context, placeObj);
    }

    @Override
    public int getItemCount() {
        return newsItems == null ? 0 : newsItems.size();
    }
}