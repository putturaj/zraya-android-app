package com.example.zraya.myApplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.Logger;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;

import java.util.ArrayList;

/**
 * Created by Rajesh on 23-Apr-16.
 */
public class DBItemSQLDao implements DBItemDao {
    private DBSQLiteHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;

    private String baseGroupVersion = Constants.EMPTY_STRING + 0;

    public DBItemSQLDao(Context context) {
        this.context = context;
        dbHelper = DBSQLiteHelper.getInstance(context);
    }

    @Override
    public void open() {
        try {
            database = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            System.out.println("DBSQLiteHelper : onCreate : Exception : " + e.getMessage());
        }
    }

    @Override
    public void close() {
        dbHelper.close();
    }

    public void updateOrderItems(String itemVersion, ArrayList<OrderInfo> baseItems) {
        OrderInfo item;
        for (int i = 0; i < baseItems.size(); i++) {
            item = baseItems.get(i);
            updateOrder(item);
        }
    }


    public void updateOrder(OrderInfo baseItem) {
        ContentValues values = setBaseItemContentValues(baseItem);
        try {
            if (database.update(dbHelper.ORDER_TABLE, values, DBSQLiteConstants.ORDER_ID + " = '" +
                    baseItem.getOrderId() + "'", null) == 0) {
                long rowId = database.insert(dbHelper.ORDER_TABLE, null, values);
                Logger.d("ZRAYA: ROW ID : ", "" + rowId);
            }
        } catch (Exception e) {
            System.out.println("ZRAYA :DBBaseItemInfoSQLDao : updateBaseItem :" + e.getMessage());
            e.printStackTrace();
        }
    }

    private ContentValues setBaseItemContentValues(OrderInfo baseItem) {
        ContentValues values = new ContentValues();
        values.put(DBSQLiteConstants.ORDER_ID, baseItem.getOrderId());
        values.put(DBSQLiteConstants.ORDER_PRODUCT_ID, baseItem.getProductID());
        values.put(DBSQLiteConstants.ORDER_USER_ID, baseItem.getUserId());
        values.put(DBSQLiteConstants.ORDER_CATEGORY_ID, baseItem.getItemCategoryID());
        values.put(DBSQLiteConstants.ORDER_STATUS, baseItem.getProductStatus());
        return values;
    }


    public void updateOfficeTableInfo(String itemVersion, ArrayList<OfficeTablePojo> officeTablepojo) {
        OfficeTablePojo officeTableinfo;
        for (int i = 0; i < officeTablepojo.size(); i++) {
            officeTableinfo = officeTablepojo.get(i);
            updateOfficeTable(officeTableinfo);
        }
    }

    public void updateOfficeTable(OfficeTablePojo baseItem) {
        ContentValues values = setOfficeTableInfo(baseItem);
        try {
            if (database.update(dbHelper.OFFICE_TABLE, values, DBSQLiteConstants.OFFICE_TABLE_ID + " = '" +
                    baseItem.getTableID() + "'", null) == 0) {
                long rowId = database.insert(dbHelper.OFFICE_TABLE, null, values);
                System.out.println("ZRAYA : ROW ID :" + rowId);
            }
        } catch (Exception e) {
            System.out.println("ZRAYA :DBBaseItemInfoSQLDao : updateBaseItem :" + e.getMessage());
            e.printStackTrace();
        }
    }

    private ContentValues setOfficeTableInfo(OfficeTablePojo mOfficeTablepojo) {

        ContentValues values = new ContentValues();
        values.put(DBSQLiteConstants.OFFICE_TABLE_ID, mOfficeTablepojo.getTableID());
        values.put(DBSQLiteConstants.OFFICE_TABLE_NAME, mOfficeTablepojo.getTableName());
        values.put(DBSQLiteConstants.OFFICE_TABLE_LENGTH, mOfficeTablepojo.getTableLength());
        values.put(DBSQLiteConstants.OFFICE_TABLE_WIDTH, mOfficeTablepojo.getTableWidth());
        values.put(DBSQLiteConstants.OFFICE_TABLE_HEIGHT, mOfficeTablepojo.getTableHeight());
        values.put(DBSQLiteConstants.OFFICE_TABLE_LEG_THICK, mOfficeTablepojo.getTableLegThick());
        values.put(DBSQLiteConstants.OFFICE_TABLE_DESCRIPTION, mOfficeTablepojo.getTableDesc());
        values.put(DBSQLiteConstants.OFFICE_TABLE_TYPE, mOfficeTablepojo.getTableType());
        values.put(DBSQLiteConstants.OFFICE_TABLE_TOP_MATERIAL, mOfficeTablepojo.getTableTopMat());
        values.put(DBSQLiteConstants.OFFICE_TABLE_BASE_MATERIAL, mOfficeTablepojo.getTableBaseMat());
        values.put(DBSQLiteConstants.OFFICE_TABLE_TOP_COLOR, mOfficeTablepojo.getTableTopColor());
        values.put(DBSQLiteConstants.OFFICE_TABLE_BASE_COLOR, mOfficeTablepojo.getTableBaseColor());
        values.put(DBSQLiteConstants.OFFICE_TABLE_IMG_URL, mOfficeTablepojo.getTableImgUrl());
        values.put(DBSQLiteConstants.OFFICE_TABLE_AMOUNT, mOfficeTablepojo.getTableAmount());
        values.put(DBSQLiteConstants.OFFICE_TABLE_PRODUCT_ID, mOfficeTablepojo.getTableProductID());
        return values;
    }


    public ArrayList<OfficeTablePojo> getAllOrders() {
        Cursor cursor = null;
        ArrayList<OfficeTablePojo> itemList = new ArrayList<OfficeTablePojo>();
        try {
            String query = "select * from " + dbHelper.OFFICE_TABLE;

            System.out.println("Zraya : DatabaseTable : getWordMatches : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
            System.out.println("Zraya : DatabaseTable : getCount : " + cursor.getCount());
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                OfficeTablePojo item = getOfficeTableItem(cursor);
                if (item != null) {
                    itemList.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            System.out.println("Zraya : DatabaseTable : getWordMatches : Exception : " + e.getMessage());
            e.printStackTrace();
        }
        return itemList;
    }

    private OfficeTablePojo getOfficeTableItem(Cursor cursor) {
        OfficeTablePojo item = new OfficeTablePojo();
        if (null == cursor || cursor.getCount() <= 0) {
            return item;
        }

        try {
            ContentValues cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            item.setTableID(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_ID));
            item.setTableName(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_NAME));
            item.setTableLength((cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_LENGTH)));
            item.setTableWidth(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_WIDTH));
            item.setTableHeight(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_HEIGHT));
            item.setTableLegThick(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_LEG_THICK));
            item.setTableDesc(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_DESCRIPTION));
            item.setTableType(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_TYPE));
            item.setTableTopMat(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_TOP_MATERIAL));
            item.setTableBaseMat(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_BASE_MATERIAL));
            item.setTableTopColor(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_TOP_COLOR));
            item.setTableBaseColor(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_BASE_COLOR));
            item.setTableImgUrl(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_IMG_URL));
            item.setTableAmount(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_AMOUNT));
            item.setTableProductID(cv.getAsString(DBSQLiteConstants.OFFICE_TABLE_PRODUCT_ID));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Zraya : DBBaseItemInfoSQLDao : getOfficeTableItem :" + e.getMessage());
        }
        return item;
    }

    public ArrayList<OrderInfo> getOrderStatuswithProId(String mProductId) {
        Cursor cursor = null;
        ArrayList<OrderInfo> orderInfo = new ArrayList<OrderInfo>();
        try {
            String query = "select * from " + dbHelper.ORDER_TABLE + " where " + DBSQLiteConstants.ORDER_PRODUCT_ID
                    + " " +
                    "like " + "'%" + mProductId + "%'";
            System.out.println("Zraya : DatabaseTable : getWordMatches : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
            System.out.println("Zraya : DatabaseTable : getCount : " + cursor.getCount());
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                OrderInfo item = getOrderItem(cursor);
                if (item != null) {
                    orderInfo.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            System.out.println("Zraya : DatabaseTable : getWordMatches : Exception : " + e.getMessage());
            e.printStackTrace();
        }
        return orderInfo;
    }

    private OrderInfo getOrderItem(Cursor cursor) {
        OrderInfo orderItem = new OrderInfo();
        if (null == cursor || cursor.getCount() <= 0) {
            return orderItem;
        }
        try {
            ContentValues cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            orderItem.setOrderId(cv.getAsString(DBSQLiteConstants.ORDER_ID));
            orderItem.setProductID(cv.getAsString(DBSQLiteConstants.ORDER_PRODUCT_ID));
            orderItem.setItemCategoryID((cv.getAsString(DBSQLiteConstants.ORDER_CATEGORY_ID)));
            orderItem.setUserId(cv.getAsString(DBSQLiteConstants.ORDER_USER_ID));
            orderItem.setProductStatus(cv.getAsString(DBSQLiteConstants.ORDER_STATUS));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Zraya : DBBaseItemInfoSQLDao : getOfficeTableItem :" + e.getMessage());
        }
        return orderItem;
    }


}
