package com.example.zraya.myApplication.beans;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

/**
 * Created by Rajesh on 29-Jun-16.
 */

/**
 * This Class will contains the Three Object
 */
public class OrderProductInfoPojo extends IJRModelData implements Cloneable {
    @SerializedName("order_info")
    private OrderInfo orderObj;
    @SerializedName("product_detail")
    private Object productObj;

    public ProductDisplayMainPojo mProductDisplayMainPojo;

    public OrderProductInfoPojo clone() throws CloneNotSupportedException {
        return (OrderProductInfoPojo) super.clone();
    }

    public OrderInfo getOrderObj() {
        return orderObj;
    }

    public void setOrderObj(OrderInfo orderObj) {
        this.orderObj = orderObj;
    }


    public ProductDisplayMainPojo getmProductDisplayMainPojo() {
        return mProductDisplayMainPojo;
    }

    public void setmProductDisplayMainPojo(ProductDisplayMainPojo mProductDisplayMainPojo) {
        this.mProductDisplayMainPojo = mProductDisplayMainPojo;
    }

    public IJRModelData getProductObject() {
        IJRModelData ijrModelData = null;
        if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_CUSTOM_OFFICE_TABLE) ||
                orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_OFFICE_TABLE)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (OfficeTablePojo) gson.fromJson(json, new TypeToken<OfficeTablePojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_WORKSTATION)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                System.out.println("Workstation Json is: " + json);
                ijrModelData = (WorkstationPojo) gson.fromJson(json, new TypeToken<WorkstationPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_OFFICE_CHAIR)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (OfficeChairPojo) gson.fromJson(json, new TypeToken<OfficeChairPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_WHITEBOARD)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (WhiteboardPojo) gson.fromJson(json, new TypeToken<WhiteboardPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_SOFAS) ||
                orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CUSTOM_SOFA)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (SofaPojo) gson.fromJson(json, new TypeToken<SofaPojo>() {
                }.getType());
//                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_PEDESTAL)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (PedestalPojo) gson.fromJson(json, new TypeToken<PedestalPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CATEGORY_CUSTOM_ORDER)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (CustomRequestPojo) gson.fromJson(json, new TypeToken<CustomRequestPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.COFFEE_TABLE)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (CoffeetablePojo) gson.fromJson(json, new TypeToken<CoffeetablePojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.CAFETARIA_TABLE)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (CafetariaPojo) gson.fromJson(json, new TypeToken<CafetariaPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (orderObj.getItemCategoryID().equalsIgnoreCase(Constants.DIRECTOR_DESK)) {
            try {
                Gson gson = new Gson();
                String json = gson.toJson(productObj);
                ijrModelData = (DirectordeskPojo) gson.fromJson(json, new TypeToken<DirectordeskPojo>() {
                }.getType());
                setProductMainDetails(ijrModelData, orderObj.getItemCategoryID());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ijrModelData;
    }

    //    Assigning the values of product to the Productdisplay Pojo
    public void setProductMainDetails(IJRModelData ijrmodel, String itemCategoryID) {
        mProductDisplayMainPojo = new ProductDisplayMainPojo();
        switch (itemCategoryID) {
//            Office Tables OR Custom Office Table
            case "1":
            case "4":
                //  Office Table details  Assigning
                OfficeTablePojo officeTablePojo;
                officeTablePojo = (OfficeTablePojo) ijrmodel;
                if (!(Utils.isEmpty(officeTablePojo.getTableID())))
                    mProductDisplayMainPojo.setId(officeTablePojo.getTableID());
                if (!(Utils.isEmpty(officeTablePojo.getTableDesc())))
                    mProductDisplayMainPojo.setDiscription(officeTablePojo.getTableDesc());
                if (!(Utils.isEmpty(officeTablePojo.getTableName())))
                    mProductDisplayMainPojo.setName(officeTablePojo.getTableName());
                if (!(Utils.isEmpty(officeTablePojo.getTableType())))
                    mProductDisplayMainPojo.setType(officeTablePojo.getTableType());
                if (!(Utils.isEmpty(officeTablePojo.getTableTopMat())))
                    mProductDisplayMainPojo.setTopmaterial(officeTablePojo.getTableTopMat());
                if (!(Utils.isEmpty(officeTablePojo.getTableBaseMat())))
                    mProductDisplayMainPojo.setBasemeterial(officeTablePojo.getTableBaseMat());
                if (!(Utils.isEmpty(officeTablePojo.getTableTopColor())))
                    mProductDisplayMainPojo.setTopcolor(officeTablePojo.getTableTopColor());
                if (!(Utils.isEmpty(officeTablePojo.getTableBaseColor())))
                    mProductDisplayMainPojo.setBasecolor(officeTablePojo.getTableBaseColor());
                if (!(Utils.isEmpty(officeTablePojo.getTableImgUrl())))
                    mProductDisplayMainPojo.setImgurl(officeTablePojo.getTableImgUrl());
                if (!(Utils.isEmpty(officeTablePojo.getTableProductID())))
                    mProductDisplayMainPojo.setProductid(officeTablePojo.getTableProductID());
                if (!(Utils.isEmpty(officeTablePojo.getTableAmount())))
                    mProductDisplayMainPojo.setAmount(officeTablePojo.getTableAmount());
                if (!(Utils.isEmpty(officeTablePojo.getTableLength())))
                    mProductDisplayMainPojo.setLength(officeTablePojo.getTableLength());
                if (!(Utils.isEmpty(officeTablePojo.getTableWidth())))
                    mProductDisplayMainPojo.setWidth(officeTablePojo.getTableWidth());
                if (!(Utils.isEmpty(officeTablePojo.getTableHeight())))
                    mProductDisplayMainPojo.setHeight(officeTablePojo.getTableHeight());
                if (!(Utils.isEmpty(officeTablePojo.getTableLegThick())))
                    mProductDisplayMainPojo.setLegthick(officeTablePojo.getTableLegThick());
                if (!(Utils.isEmpty(officeTablePojo.getBrandName())))
                    mProductDisplayMainPojo.setBrandname(officeTablePojo.getBrandName());
                if (!(Utils.isEmpty(officeTablePojo.getTableStyle())))
                    mProductDisplayMainPojo.setStyle(officeTablePojo.getTableStyle());
                if (!(Utils.isEmpty(officeTablePojo.getModelnumber())))
                    mProductDisplayMainPojo.setModelnumber(officeTablePojo.getModelnumber());
                if (!(Utils.isEmpty(officeTablePojo.getCareInstruction())))
                    mProductDisplayMainPojo.setCareinstuction(officeTablePojo.getCareInstruction());
                if (!(Utils.isEmpty(officeTablePojo.getDrawerInclude())))
                    mProductDisplayMainPojo.setDrawerinclude(officeTablePojo.getDrawerInclude());
                if (!(Utils.isEmpty(officeTablePojo.getImageUrls())))
                    mProductDisplayMainPojo.setImageUrls(officeTablePojo.getImageUrls());
                break;
//            workstation
            case "10":
//                workstation details assinging
                WorkstationPojo workstationPojo;
                workstationPojo = (WorkstationPojo) ijrmodel;
                if (!(Utils.isEmpty(workstationPojo.getWs_id())))
                    mProductDisplayMainPojo.setId(workstationPojo.getWs_id());
                if (!(Utils.isEmpty(workstationPojo.getWs_name())))
                    mProductDisplayMainPojo.setName(workstationPojo.getWs_name());
                if (!(Utils.isEmpty(workstationPojo.getWs_topcolor())))
                    mProductDisplayMainPojo.setTopcolor(workstationPojo.getWs_topcolor());
                if (!(Utils.isEmpty(workstationPojo.getWs_basecolor())))
                    mProductDisplayMainPojo.setBasecolor(workstationPojo.getWs_basecolor());
                if (!(Utils.isEmpty(workstationPojo.getWs_img_url())))
                    mProductDisplayMainPojo.setImgurl(workstationPojo.getWs_img_url());
                if (!(Utils.isEmpty(workstationPojo.getWs_productid())))
                    mProductDisplayMainPojo.setProductid(workstationPojo.getWs_productid());
                if (!(Utils.isEmpty(workstationPojo.getWs_amount())))
                    mProductDisplayMainPojo.setAmount(workstationPojo.getWs_amount());
                if (!(Utils.isEmpty(workstationPojo.getWs_partition())))
                    mProductDisplayMainPojo.setPartition(workstationPojo.getWs_partition());
                if (!(Utils.isEmpty(workstationPojo.getWs_imageUrls())))
                    mProductDisplayMainPojo.setImageUrls(workstationPojo.getWs_imageUrls());
                if (!(Utils.isEmpty(workstationPojo.getWs_meterial())))
                    mProductDisplayMainPojo.setMeterial(workstationPojo.getWs_meterial());
                if (!(Utils.isEmpty(workstationPojo.getWs_beidingcolor())))
                    mProductDisplayMainPojo.setBeidingcolor(workstationPojo.getWs_beidingcolor());
                if (!(Utils.isEmpty(workstationPojo.getWs_typesoflegs())))
                    mProductDisplayMainPojo.setTypeoflegs(workstationPojo.getWs_typesoflegs());
                if (!(Utils.isEmpty(workstationPojo.getWs_poweroutletfull())))
                    mProductDisplayMainPojo.setPoweroutletfull(workstationPojo.getWs_poweroutletfull());
                if (!(Utils.isEmpty(workstationPojo.getWs_enclosedpowerboxfour())))
                    mProductDisplayMainPojo.setEnclosedpowerboxfour(workstationPojo.getWs_enclosedpowerboxfour());
                if (!(Utils.isEmpty(workstationPojo.getWs_enclosedpowerboxsix())))
                    mProductDisplayMainPojo.setEnclosedpowerboxsix(workstationPojo.getWs_enclosedpowerboxsix());
                if (!(Utils.isEmpty(workstationPojo.getWs_singlepoweroutlet())))
                    mProductDisplayMainPojo.setSinglepoweroutlet(workstationPojo.getWs_singlepoweroutlet());
                if (!(Utils.isEmpty(workstationPojo.getWs_metaldrawer())))
                    mProductDisplayMainPojo.setMetaldrawer(workstationPojo.getWs_metaldrawer());
                if (!(Utils.isEmpty(workstationPojo.getWs_keyboarddrawer())))
                    mProductDisplayMainPojo.setKeyboarddrawer(workstationPojo.getWs_keyboarddrawer());
                if (!(Utils.isEmpty(workstationPojo.getWs_monitorstand())))
                    mProductDisplayMainPojo.setMonitorstand(workstationPojo.getWs_monitorstand());
                if (!(Utils.isEmpty(workstationPojo.getWs_shape())))
                    mProductDisplayMainPojo.setShape(workstationPojo.getWs_shape());
                if (!(Utils.isEmpty(workstationPojo.getWs_numberofworkstation())))
                    mProductDisplayMainPojo.setNumberproduct(workstationPojo.getWs_numberofworkstation());
                if (!(Utils.isEmpty(workstationPojo.getWs_sizeworkstation())))
                    mProductDisplayMainPojo.setSize(workstationPojo.getWs_sizeworkstation());
                break;
//            Office Chair
            case "3":
                OfficeChairPojo officeChairPojo;
                officeChairPojo = (OfficeChairPojo) ijrmodel;
                mProductDisplayMainPojo.setId(officeChairPojo.getChairID());
                if (!(Utils.isEmpty(officeChairPojo.getName())))
                    mProductDisplayMainPojo.setName(officeChairPojo.getName());
                if (!(Utils.isEmpty(officeChairPojo.getBrand())))
                    mProductDisplayMainPojo.setBrandname(officeChairPojo.getBrand());
                if (!(Utils.isEmpty(officeChairPojo.getType())))
                    mProductDisplayMainPojo.setType(officeChairPojo.getType());
                if (!(Utils.isEmpty(officeChairPojo.getStyle())))
                    mProductDisplayMainPojo.setStyle(officeChairPojo.getStyle());
                if (!(Utils.isEmpty(officeChairPojo.getSeatLockIncluded())))
                    mProductDisplayMainPojo.setSeatlockinclude(officeChairPojo.getSeatLockIncluded());
                if (!(Utils.isEmpty(officeChairPojo.getWheelsIncluded())))
                    mProductDisplayMainPojo.setWheelsinclude(officeChairPojo.getWheelsIncluded());
                if (!(Utils.isEmpty(officeChairPojo.getHeadSupport())))
                    mProductDisplayMainPojo.setHeadsupportinclude(officeChairPojo.getHeadSupport());
                if (!(Utils.isEmpty(officeChairPojo.getAdjustableHeight())))
                    mProductDisplayMainPojo.setAdjestableheight(officeChairPojo.getAdjustableHeight());
                if (!(Utils.isEmpty(officeChairPojo.getArmrestIncluded())))
                    mProductDisplayMainPojo.setArmrestinclude(officeChairPojo.getArmrestIncluded());
                if (!(Utils.isEmpty(officeChairPojo.getSuitableFor())))
                    mProductDisplayMainPojo.setSuitable(officeChairPojo.getSuitableFor());
                if (!(Utils.isEmpty(officeChairPojo.getCareInstructions())))
                    mProductDisplayMainPojo.setCareinstuction(officeChairPojo.getCareInstructions());
                if (!(Utils.isEmpty(officeChairPojo.getWidth())))
                    mProductDisplayMainPojo.setWidth(officeChairPojo.getWidth());
                if (!(Utils.isEmpty(officeChairPojo.getHeight())))
                    mProductDisplayMainPojo.setHeight(officeChairPojo.getHeight());
                if (!(Utils.isEmpty(officeChairPojo.getPrimaryMaterial())))
                    mProductDisplayMainPojo.setMeterial(officeChairPojo.getPrimaryMaterial());
                if (!(Utils.isEmpty(officeChairPojo.getBrandColor())))
                    mProductDisplayMainPojo.setTopcolor(officeChairPojo.getBrandColor());
                if (!(Utils.isEmpty(officeChairPojo.getAmount())))
                    mProductDisplayMainPojo.setAmount(officeChairPojo.getAmount());
                if (!(Utils.isEmpty(officeChairPojo.getChairDescription())))
                    mProductDisplayMainPojo.setDiscription(officeChairPojo.getChairDescription());
                if (!(Utils.isEmpty(officeChairPojo.getImageURL())))
                    mProductDisplayMainPojo.setImgurl(officeChairPojo.getImageURL());

                if (!(Utils.isEmpty(officeChairPojo.getImageUrls())))
                    mProductDisplayMainPojo.setImageUrls(officeChairPojo.getImageUrls());

                break;
//            White boards
            case "5":
                WhiteboardPojo whiteboardPojo;
                whiteboardPojo = (WhiteboardPojo) ijrmodel;
                mProductDisplayMainPojo.setId(whiteboardPojo.getId());
                if (!(Utils.isEmpty(whiteboardPojo.getName())))
                    mProductDisplayMainPojo.setName(whiteboardPojo.getName());
                if (!(Utils.isEmpty(whiteboardPojo.getMeterial())))
                    mProductDisplayMainPojo.setMeterial(whiteboardPojo.getMeterial());
                if (!(Utils.isEmpty(whiteboardPojo.getWidth())))
                    mProductDisplayMainPojo.setWidth(whiteboardPojo.getWidth());
                if (!(Utils.isEmpty(whiteboardPojo.getThickness())))
                    mProductDisplayMainPojo.setThickness(whiteboardPojo.getThickness());
                if (!(Utils.isEmpty(whiteboardPojo.getHeight())))
                    mProductDisplayMainPojo.setHeight(whiteboardPojo.getHeight());
                if (!(Utils.isEmpty(whiteboardPojo.getBgcolor())))
                    mProductDisplayMainPojo.setBgcolor(whiteboardPojo.getBgcolor());
                if (!(Utils.isEmpty(whiteboardPojo.getAmount())))
                    mProductDisplayMainPojo.setAmount(whiteboardPojo.getAmount());
                if (!(Utils.isEmpty(whiteboardPojo.getImgurls())))
                    mProductDisplayMainPojo.setImgurl(whiteboardPojo.getImgurls());
                if (!(Utils.isEmpty(whiteboardPojo.getProductid())))
                    mProductDisplayMainPojo.setProductid(whiteboardPojo.getProductid());
                break;
//            Home Chairs
            case "6":
                break;
//            Sofas
            case "7":
            case "15":
                SofaPojo sofaPojo;
                sofaPojo = (SofaPojo) ijrmodel;
                mProductDisplayMainPojo.setId(sofaPojo.getId());
                if (!(Utils.isEmpty(sofaPojo.getName())))
                    mProductDisplayMainPojo.setName(sofaPojo.getName());
                if (!(Utils.isEmpty(sofaPojo.getProduct_family())))
                    mProductDisplayMainPojo.setProductfamily(sofaPojo.getProduct_family());
                if (!(Utils.isEmpty(sofaPojo.getProduct_type())))
                    mProductDisplayMainPojo.setType(sofaPojo.getProduct_type());
                if (!(Utils.isEmpty(sofaPojo.getLength())))
                    mProductDisplayMainPojo.setLength(sofaPojo.getLength());
                if (!(Utils.isEmpty(sofaPojo.getWidth())))
                    mProductDisplayMainPojo.setWidth(sofaPojo.getWidth());
                if (!(Utils.isEmpty(sofaPojo.getHeight_cushion())))
                    mProductDisplayMainPojo.setCusionhieght(sofaPojo.getHeight_cushion());
                if (!(Utils.isEmpty(sofaPojo.getHeight_sofa())))
                    mProductDisplayMainPojo.setHeight(sofaPojo.getHeight_sofa());
                if (!(Utils.isEmpty(sofaPojo.getCushion_meterial())))
                    mProductDisplayMainPojo.setCusionmeterial(sofaPojo.getCushion_meterial());
                if (!(Utils.isEmpty(sofaPojo.getBase_meterial())))
                    mProductDisplayMainPojo.setBasemeterial(sofaPojo.getBase_meterial());
                if (!(Utils.isEmpty(sofaPojo.getFrame_color())))
                    mProductDisplayMainPojo.setFramecolor(sofaPojo.getFrame_color());
                if (!(Utils.isEmpty(sofaPojo.getCushion_color())))
                    mProductDisplayMainPojo.setCusioncolor(sofaPojo.getCushion_color());
                if (!(Utils.isEmpty(sofaPojo.getDiscription())))
                    mProductDisplayMainPojo.setDiscription(sofaPojo.getDiscription());
                if (!(Utils.isEmpty(sofaPojo.getAmount())))
                    mProductDisplayMainPojo.setAmount(sofaPojo.getAmount());
                if (!(Utils.isEmpty(sofaPojo.getImgurls())))
                    mProductDisplayMainPojo.setImgurl(sofaPojo.getImgurls());
                if (!(Utils.isEmpty(sofaPojo.getProductid())))
                    mProductDisplayMainPojo.setProductid(sofaPojo.getProductid());


                break;
//            Pedestel
            case "11":
                PedestalPojo pedestalPojo;
                pedestalPojo = (PedestalPojo) ijrmodel;
                mProductDisplayMainPojo.setId(pedestalPojo.getId());
                if (!(Utils.isEmpty(pedestalPojo.getName())))
                    mProductDisplayMainPojo.setName(pedestalPojo.getName());

                if (!(Utils.isEmpty(pedestalPojo.getDescription())))
                    mProductDisplayMainPojo.setDiscription(pedestalPojo.getDescription());

                if (!(Utils.isEmpty(pedestalPojo.getImgurls())))
                    mProductDisplayMainPojo.setImageUrls(pedestalPojo.getImgurls());

                if (!(Utils.isEmpty(pedestalPojo.getFamily())))
                    mProductDisplayMainPojo.setProductfamily(pedestalPojo.getFamily());

                if (!(Utils.isEmpty(pedestalPojo.getHeight())))
                    mProductDisplayMainPojo.setHeight(pedestalPojo.getHeight());

                if (!(Utils.isEmpty(pedestalPojo.getWidth())))
                    mProductDisplayMainPojo.setWidth(pedestalPojo.getWidth());

                if (!(Utils.isEmpty(pedestalPojo.getBreadth())))
                    mProductDisplayMainPojo.setLength(pedestalPojo.getBreadth());

                if (!(Utils.isEmpty(pedestalPojo.getNoDrawer())))
                    mProductDisplayMainPojo.setNumberOfdrawer(pedestalPojo.getNoDrawer());

                if (!(Utils.isEmpty(pedestalPojo.getMeterial())))
                    mProductDisplayMainPojo.setMeterial(pedestalPojo.getMeterial());

                if (!(Utils.isEmpty(pedestalPojo.getOutercolor())))
                    mProductDisplayMainPojo.setOutercolor(pedestalPojo.getOutercolor());

                if (!(Utils.isEmpty(pedestalPojo.getDrawercolor())))
                    mProductDisplayMainPojo.setDrawercolor(pedestalPojo.getDrawercolor());

                if (!(Utils.isEmpty(pedestalPojo.getWheelinclude())))
                    mProductDisplayMainPojo.setWheelsinclude(pedestalPojo.getWheelinclude());

                if (!(Utils.isEmpty(pedestalPojo.getAmount())))
                    mProductDisplayMainPojo.setAmount(pedestalPojo.getAmount());

                if (!(Utils.isEmpty(pedestalPojo.getProductid())))
                    mProductDisplayMainPojo.setProductid(pedestalPojo.getProductid());
                break;

//                custom request order

            case "19":
                CustomRequestPojo customRequestPojo;
                customRequestPojo = (CustomRequestPojo) ijrmodel;
                mProductDisplayMainPojo.setId(customRequestPojo.getId());

                if (!Utils.isEmpty(customRequestPojo.getName())) {
                    mProductDisplayMainPojo.setName(customRequestPojo.getName());
                }
                if (!Utils.isEmpty(customRequestPojo.getMeterial())) {
                    mProductDisplayMainPojo.setMeterial(customRequestPojo.getMeterial());
                }
                if (!Utils.isEmpty(customRequestPojo.getCustomImageurl())) {
                    mProductDisplayMainPojo.setImgurl(customRequestPojo.getCustomImageurl());
                }


//                cafetaria Table
            case "16":
            case "17":
                CafetariaPojo cafetariaPojo = (CafetariaPojo) ijrmodel;
                mProductDisplayMainPojo.setId(cafetariaPojo.getId());

                if (!Utils.isEmpty(cafetariaPojo.getName())) {
                    mProductDisplayMainPojo.setName(cafetariaPojo.getName());
                }
                if (!Utils.isEmpty(cafetariaPojo.getDate_created())) {
                    mProductDisplayMainPojo.setDate(cafetariaPojo.getDate_created());
                }
                if (!Utils.isEmpty(cafetariaPojo.getShape())) {
                    mProductDisplayMainPojo.setShape(cafetariaPojo.getShape());
                }
                if (!Utils.isEmpty(cafetariaPojo.getTop_meterial())) {
                    mProductDisplayMainPojo.setTopmaterial(cafetariaPojo.getTop_meterial());
                }
                if (!Utils.isEmpty(cafetariaPojo.getBase_meterial())) {
                    mProductDisplayMainPojo.setBasemeterial(cafetariaPojo.getBase_meterial());
                }
                if (!Utils.isEmpty(cafetariaPojo.getPrice())) {
                    mProductDisplayMainPojo.setAmount(cafetariaPojo.getPrice());
                }
                if (!Utils.isEmpty(cafetariaPojo.getBase_color())) {
                    mProductDisplayMainPojo.setBasecolor(cafetariaPojo.getBase_color());
                }
                if (!Utils.isEmpty(cafetariaPojo.getTop_color())) {
                    mProductDisplayMainPojo.setTopcolor(cafetariaPojo.getTop_color());
                }
                if (!Utils.isEmpty(cafetariaPojo.getLeg_id())) {
                }
                if (!Utils.isEmpty(cafetariaPojo.getCare())) {
                    mProductDisplayMainPojo.setCareinstuction(cafetariaPojo.getCare());
                }
                if (!Utils.isEmpty(cafetariaPojo.getLength())) {
                    mProductDisplayMainPojo.setLength(cafetariaPojo.getLength());
                }
                if (!Utils.isEmpty(cafetariaPojo.getBrand())) {
                    mProductDisplayMainPojo.setBrandname(cafetariaPojo.getBrand());
                }
                if (!Utils.isEmpty(cafetariaPojo.getImgurl())) {
                    mProductDisplayMainPojo.setImgurl(cafetariaPojo.getImgurl());
                }


//            case "17":
//                CoffeetablePojo coffeetablePojo = (CoffeetablePojo) ijrmodel;
            case "18":
                DirectordeskPojo directordeskPojo = (DirectordeskPojo) ijrmodel;
                mProductDisplayMainPojo.setId(directordeskPojo.getId());

                if (!Utils.isEmpty(directordeskPojo.getAmount())) {
                    mProductDisplayMainPojo.setAmount(directordeskPojo.getAmount());
                }
                if (!Utils.isEmpty(directordeskPojo.getImg_url())) {
                    mProductDisplayMainPojo.setImgurl(directordeskPojo.getImg_url());
                }
                if (!Utils.isEmpty(directordeskPojo.getName())) {
                    mProductDisplayMainPojo.setName(directordeskPojo.getName());
                }
                if (!Utils.isEmpty(directordeskPojo.getWidth())) {
                    mProductDisplayMainPojo.setWidth(directordeskPojo.getWidth());
                }
                if (!Utils.isEmpty(directordeskPojo.getDate_created())) {
                    mProductDisplayMainPojo.setDate(directordeskPojo.getDate_created());
                }
                if ((!Utils.isEmpty(directordeskPojo.getTop_meterial()))) {
                    mProductDisplayMainPojo.setTopmaterial(directordeskPojo.getTop_meterial());
                }
                if (!Utils.isEmpty(directordeskPojo.getHeight())) {
                    mProductDisplayMainPojo.setHeight(directordeskPojo.getHeight());
                }
                if (!Utils.isEmpty(directordeskPojo.getBase_color())) {
                    mProductDisplayMainPojo.setBasecolor(directordeskPojo.getBase_color());
                }
                if (!Utils.isEmpty(directordeskPojo.getBase_meterial())) {
                    mProductDisplayMainPojo.setBasemeterial(directordeskPojo.getBase_meterial());
                }
                if (!Utils.isEmpty(directordeskPojo.getLength())) {
                    mProductDisplayMainPojo.setLength(directordeskPojo.getLength());
                }
                if (!Utils.isEmpty(directordeskPojo.getTop_color())) {
                    mProductDisplayMainPojo.setTopcolor(directordeskPojo.getTop_color());
                }


            default:
                System.out.println(Constants.DISPLAY_MESSAGE + "Nothing will selected");
        }
    }
}
