package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.ImageFullPageViewPagerAdapter;
import com.example.zraya.myApplication.Utils.PageIndicator;

/**
 * Created by Raj on 11/30/2016.
 */
public class FullImageDisplayActivity extends BaseDrawerActivity {
    private ViewPager viewPagerImage;
    private ImageView mClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_page_image);
        viewPagerImage = (ViewPager) findViewById(R.id.full_page_image);
        mClose = (ImageView) findViewById(R.id.close);
        setupViewPager(viewPagerImage);
        setOnclickListener();
    }

    private void setupViewPager(ViewPager viewPagerImage) {
        Intent intent = getIntent();
        String[] imageArrays = intent.getStringArrayExtra(Constants.BUNDLE_IMAGE_ARRAY);
        ImageFullPageViewPagerAdapter viewPagerAdapter = new ImageFullPageViewPagerAdapter(this, imageArrays, viewPagerImage);
        viewPagerImage.setAdapter(viewPagerAdapter);
        PageIndicator pageIndicator = (PageIndicator) findViewById(R.id.page_indicator);
        pageIndicator.setViewPager(viewPagerImage);
    }

    private void setOnclickListener() {
        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
