package com.example.zraya.myApplication.entities;

public enum ZrayaProductsType {
    CUSTOM_OFFICE_TABLES(1, "custom_office_tables"),
    CUSTOM_WHITEBOARD(2, "custom_whiteboard"),
    OFFICE_CHAIRS(3, "office_chairs"),
    OFFICE_TABLES(4, "office_tables"),
    WHITE_BOARDS(5, "white_boards"),
    HOME_CHAIRS(6, "home_chairs"),
    SOFA(7, "sofa"),
    WORKSTATIONS(8, "workstations"),
    CUSTOM_WORKSTATION(9, "custom_workstation"),
    WORKSTATION(10, "workstation"),
    PEDESTAL(11, "pedestal"),
    CUSTOM_ORDER(12, "custom_order"),
    TABLE_TOP(13, "table_top"),
    TABLE_LEGS(14, "table_legs"),
    CUSTOM_TABLE_TOP(130, "custom_table_top"),
    CUSTOM_TABLE_LEGS(140, "custom_table_legs"),
    CUSTOM_SOFA(15, "custom_sofa"),
    CUSTOM_CAFETERIA_TABLE(16, "custom_cafeteria_table"),
    CUSTOM_COFFEE_TABLE(17, "custom_coffee_table"),
    DIRECTOR_DESK_TABLE(18, "director_desk_table"),
    CUSTOM_REQUEST(19, "custom_request"),
    CUSTOM_SHELVE(20, "custom_shelve"),
    METAL_FILE_STORAGE(202, "metal_file_storage");


    private int index;
    private String name;

    ZrayaProductsType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static ZrayaProductsType fromName(String name) {
        for (ZrayaProductsType type : ZrayaProductsType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return CUSTOM_OFFICE_TABLES;
    }

    public static ZrayaProductsType fromIndex(int index) {
        for (ZrayaProductsType type : ZrayaProductsType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return CUSTOM_OFFICE_TABLES;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
