/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.example.zraya.myApplication.beans;

/**
 * Generic api response holder for REST api calls
 *
 * @param <T>
 * @author amarjit
 */
public class ApiResponse<T> implements java.io.Serializable {
  private static final long serialVersionUID = -3521522892464348387L;
  private int code;
  private String status;
  private T data;

  public ApiResponse() {
  }

  public ApiResponse(T data) {
    this.data = data;
  }

  public ApiResponse(T data, int code) {
    this.data = data;
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setStatus(int code, String message) {
    this.code = code;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public String toString() {
    return "code : " + code + ", status : " + status + ", data : " + data;
  }
}
