package com.example.zraya.myApplication.listeners;

import com.example.zraya.myApplication.beans.IJRModelData;

public interface UpdatableViewHolder {

    void updateUI(int position, IJRModelData modelData);
}
