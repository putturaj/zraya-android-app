package com.example.zraya.myApplication.listeners;

import android.view.View;

import com.example.zraya.myApplication.beans.ParentOrdersData;


/**
 * Created by bheemesh on 2/1/16.
 */
public interface OnProductItemClickListener {
    void onItemClick(View view, ParentOrdersData placeObj);
}
