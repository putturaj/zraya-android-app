package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class OfficeChairPojo extends IJRModelData {
    @SerializedName("ChairID")
    private String ChairID;

    @SerializedName("Brand")
    private String Brand;

    @SerializedName("HeadSupport")
    private String HeadSupport;

    @SerializedName("TotalQuantityLeft")
    private String TotalQuantityLeft;

    @SerializedName("ImageURL")
    private String ImageURL;

    @SerializedName("SecondaryMaterial")
    private String SecondaryMaterial;

    @SerializedName("Type")
    private String Type;

    @SerializedName("imgDesurl3")
    private String imgDesurl3;

    @SerializedName("Style")
    private String Style;

    @SerializedName("imgDesurl2")
    private String imgDesurl2;

    @SerializedName("imgDesurl1")
    private String imgDesurl1;

    @SerializedName("Depth")
    private String Depth;

    @SerializedName("ImageName")
    private String ImageName;

    @SerializedName("BrandColor")
    private String BrandColor;

    @SerializedName("WheelsIncluded")
    private String WheelsIncluded;

    @SerializedName("productid")
    private String productid;

    @SerializedName("SuitableFor")
    private String SuitableFor;

    @SerializedName("SeatWidth")
    private String SeatWidth;

    @SerializedName("Width")
    private String Width;

    @SerializedName("ChairDescription")
    private String ChairDescription;

    @SerializedName("SeatHeight")
    private String SeatHeight;

    @SerializedName("ArmrestIncluded")
    private String ArmrestIncluded;

    @SerializedName("Amount")
    private String Amount;

    @SerializedName("AdjustableHeight")
    private String AdjustableHeight;

    @SerializedName("PrimaryMaterial")
    private String PrimaryMaterial;

    @SerializedName("Height")
    private String Height;

    @SerializedName("SeatLockIncluded")
    private String SeatLockIncluded;

    @SerializedName("Name")
    private String Name;

    @SerializedName("CategoryID")
    private String CategoryID;

    @SerializedName("CareInstructions")
    private String CareInstructions;

    @SerializedName("imageUrls")
    private String imageUrls;

    public String getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getHeadSupport() {
        return HeadSupport;
    }

    public void setHeadSupport(String HeadSupport) {
        this.HeadSupport = HeadSupport;
    }

    public String getTotalQuantityLeft() {
        return TotalQuantityLeft;
    }

    public void setTotalQuantityLeft(String TotalQuantityLeft) {
        this.TotalQuantityLeft = TotalQuantityLeft;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String ImageURL) {
        this.ImageURL = ImageURL;
    }

    public String getChairID() {
        return ChairID;
    }

    public void setChairID(String ChairID) {
        this.ChairID = ChairID;
    }

    public String getSecondaryMaterial() {
        return SecondaryMaterial;
    }

    public void setSecondaryMaterial(String SecondaryMaterial) {
        this.SecondaryMaterial = SecondaryMaterial;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getImgDesurl3() {
        return imgDesurl3;
    }

    public void setImgDesurl3(String imgDesurl3) {
        this.imgDesurl3 = imgDesurl3;
    }

    public String getStyle() {
        return Style;
    }

    public void setStyle(String Style) {
        this.Style = Style;
    }

    public String getImgDesurl2() {
        return imgDesurl2;
    }

    public void setImgDesurl2(String imgDesurl2) {
        this.imgDesurl2 = imgDesurl2;
    }

    public String getImgDesurl1() {
        return imgDesurl1;
    }

    public void setImgDesurl1(String imgDesurl1) {
        this.imgDesurl1 = imgDesurl1;
    }

    public String getDepth() {
        return Depth;
    }

    public void setDepth(String Depth) {
        this.Depth = Depth;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String ImageName) {
        this.ImageName = ImageName;
    }

    public String getBrandColor() {
        return BrandColor;
    }

    public void setBrandColor(String BrandColor) {
        this.BrandColor = BrandColor;
    }

    public String getWheelsIncluded() {
        return WheelsIncluded;
    }

    public void setWheelsIncluded(String WheelsIncluded) {
        this.WheelsIncluded = WheelsIncluded;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getSuitableFor() {
        return SuitableFor;
    }

    public void setSuitableFor(String SuitableFor) {
        this.SuitableFor = SuitableFor;
    }

    public String getSeatWidth() {
        return SeatWidth;
    }

    public void setSeatWidth(String SeatWidth) {
        this.SeatWidth = SeatWidth;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String Width) {
        this.Width = Width;
    }

    public String getChairDescription() {
        return ChairDescription;
    }

    public void setChairDescription(String ChairDescription) {
        this.ChairDescription = ChairDescription;
    }

    public String getSeatHeight() {
        return SeatHeight;
    }

    public void setSeatHeight(String SeatHeight) {
        this.SeatHeight = SeatHeight;
    }

    public String getArmrestIncluded() {
        return ArmrestIncluded;
    }

    public void setArmrestIncluded(String ArmrestIncluded) {
        this.ArmrestIncluded = ArmrestIncluded;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getAdjustableHeight() {
        return AdjustableHeight;
    }

    public void setAdjustableHeight(String AdjustableHeight) {
        this.AdjustableHeight = AdjustableHeight;
    }

    public String getPrimaryMaterial() {
        return PrimaryMaterial;
    }

    public void setPrimaryMaterial(String PrimaryMaterial) {
        this.PrimaryMaterial = PrimaryMaterial;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String Height) {
        this.Height = Height;
    }

    public String getSeatLockIncluded() {
        return SeatLockIncluded;
    }

    public void setSeatLockIncluded(String SeatLockIncluded) {
        this.SeatLockIncluded = SeatLockIncluded;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String CategoryID) {
        this.CategoryID = CategoryID;
    }

    public String getCareInstructions() {
        return CareInstructions;
    }

    public void setCareInstructions(String CareInstructions) {
        this.CareInstructions = CareInstructions;
    }

    @Override
    public String toString() {
        return "ClassPojo [Brand = " + Brand + ", HeadSupport = " + HeadSupport + ", TotalQuantityLeft = " + TotalQuantityLeft + ", ImageURL = " + ImageURL + ", ChairID = " + ChairID + ", SecondaryMaterial = " + SecondaryMaterial + ", Type = " + Type + ", imgDesurl3 = " + imgDesurl3 + ", Style = " + Style + ", imgDesurl2 = " + imgDesurl2 + ", imgDesurl1 = " + imgDesurl1 + ", Depth = " + Depth + ", ImageName = " + ImageName + ", BrandColor = " + BrandColor + ", WheelsIncluded = " + WheelsIncluded + ", productid = " + productid + ", SuitableFor = " + SuitableFor + ", SeatWidth = " + SeatWidth + ", Width = " + Width + ", ChairDescription = " + ChairDescription + ", SeatHeight = " + SeatHeight + ", ArmrestIncluded = " + ArmrestIncluded + ", Amount = " + Amount + ", AdjustableHeight = " + AdjustableHeight + ", PrimaryMaterial = " + PrimaryMaterial + ", Height = " + Height + ", SeatLockIncluded = " + SeatLockIncluded + ", Name = " + Name + ", CategoryID = " + CategoryID + ", CareInstructions = " + CareInstructions + "]";
    }
}
