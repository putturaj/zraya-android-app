package com.example.zraya.myApplication.listeners;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public interface ActivityEventListener {
    void updateActivity(boolean updateAllFragment);
}
