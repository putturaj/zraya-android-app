package com.example.zraya.myApplication.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrderStatusPojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.OrderStatusUpdatePresenter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Rajesh on 13-May-16.
 */
public class ProductStatusChangeActivity extends BaseDrawerActivity {
    //The "x" and "y" position of the "Show Button" on screen.
    Point p;

    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;
    public OfficeTablePojo mOfficeTablePojo;
    public OrderStatusPojo mOrderStatusPojo;
    UserSessionManager session;


    public OrderInfo mOrderInfoPojo;
    public OrdersData mOrderDataPojo;
    public TextView mProductStatus;
    public Button mUpdate;
    public LinearLayout mMainLinearLayout;
    public RadioButton statusAssigned, statusPurchased, statusCutting, statusAssenble, statusPolish,
            statusQA, statusDelivery, statusCompleted, statusOrdered, statusCancel, selectedRadioButtonID;
    public EditText mCommentOfStatusChange;
    public String mCommentStatusChange, mChangedStatus, mOrderID, orderStatus;
    public static String mStatusChangedValue;
    private RadioGroup radioGroup;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_change);
        mMainLinearLayout = (LinearLayout) findViewById(R.id.status_change);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);

        session = new UserSessionManager(getApplicationContext());
        getIntentData();
        mProductStatus = (TextView) findViewById(R.id.productStatus);
        mProductStatus.setText(orderStatus);
        setOnClickListener();

    }


    private void getIntentData() {
        statusOrdered = (RadioButton) findViewById(R.id.radioOrdered);
        statusAssigned = (RadioButton) findViewById(R.id.radioAssigned);
        statusPurchased = (RadioButton) findViewById(R.id.radioPurchase);
        statusCutting = (RadioButton) findViewById(R.id.radioCut);
        statusAssenble = (RadioButton) findViewById(R.id.radioAssemble);
        statusPolish = (RadioButton) findViewById(R.id.radioPolish);
        statusQA = (RadioButton) findViewById(R.id.radioQA);
        statusDelivery = (RadioButton) findViewById(R.id.radioDelivery);
        statusCompleted = (RadioButton) findViewById(R.id.radioCompleted);
        statusCancel = (RadioButton) findViewById(R.id.radioCancle);
        Intent intent = getIntent();
        mOfficeTablePojo = (OfficeTablePojo) intent.getSerializableExtra(Constants.BUNDLE_ITEM_COLLECTION);
        mOrderID = (String) intent.getSerializableExtra(Constants.BUNDLE_ORDER_ID);
        mOrderInfoPojo = (OrderInfo) intent.getSerializableExtra(Constants.BUNDLE_STATUS);
        mOrderDataPojo = (OrdersData) intent.getSerializableExtra(Constants.BUNDLE_ORDER_DATA);
        setRadioButtoncheck(mOrderInfoPojo.getProductStatus());
    }

    private void setRadioButtoncheck(String aProductStatus) {
        mToolbarTitle.setText("Status Change");
        if (aProductStatus.equals(Constants.ITEM_STATUS_ORDERED)) {
            statusOrdered.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_ASSIGNED)) {
            statusAssigned.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_PURCHASED)) {
            statusPurchased.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_CUTTING)) {
            statusCutting.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_ASSEMBLE)) {
            statusAssenble.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_POLISH)) {
            statusPolish.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_QA)) {
            statusQA.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_DELIVER)) {
            statusDelivery.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_COMPLETED)) {
            statusCompleted.setChecked(true);
        } else if (aProductStatus.equals(Constants.ITEM_STATUS_CANCELLED)) {
            statusCompleted.setChecked(true);
        }
    }

    // Get the x and y position after the button is draw on screen
    // (It's important to note that we can't get the position in the onCreate(),
    // because at that stage most probably the view isn't drawn yet, so it will return (0, 0))
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
        Button button = (Button) findViewById(R.id.btnUpdate);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        button.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }

    // The method that displays the popup.
    private void showPopup(final Activity context, Point p) {


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_layout_comment);

        // Getting a reference to Close button, and close the popup when clicked.
        mCommentOfStatusChange = (EditText) dialog.findViewById(R.id.Comment_statusChange);
        Button updateStatus = (Button) dialog.findViewById(R.id.update_status);
        dialog.show();
        updateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mCommentStatusChange = mCommentOfStatusChange.getText().toString();
                    updateStatus(mCommentStatusChange);
                    dialog.dismiss();
                } catch (Exception e) {
                    System.out.println("Status Change : pop up" + e.toString());
                }

            }
        });
    }

    private void setOnClickListener() {
        mUpdate = (Button) findViewById(R.id.btnUpdate);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group_change);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                selectedRadioButtonID = (RadioButton) findViewById(checkedId);
                mChangedStatus = selectedRadioButtonID.getText().toString();
                setConstantStatusValue(mChangedStatus);
                checkingUserType(mStatusChangedValue);
            }
        });


        mUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                selectedRadioButtonID = (RadioButton) findViewById(selectedId);

                //Open popup window
                if (p != null) {
                    showPopup(ProductStatusChangeActivity.this, p);
                }
            }
        });

        mToolBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void updateStatus(String aCommentStatusChange) {
        OrderStatusUpdatePresenter orderStatusUpdatePresenter = new OrderStatusUpdatePresenter(this);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        mOrderStatusPojo = new OrderStatusPojo();
        mOrderStatusPojo.setStatusOrderID(mOrderID);
        mOrderStatusPojo.setStatusStatus(mStatusChangedValue);
        mOrderStatusPojo.setStatusComment(aCommentStatusChange);
        mOrderStatusPojo.setStatusDate(timeStamp);
        orderStatusUpdatePresenter.requestToServer(RequestActionType.ORDER_STATUS_UPDATE, mOrderStatusPojo);
    }

    public void responseFromServer() {
        Intent intent = null;
        HashMap<String, String> user = session.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);

        switch (userType) {
            case Constants.USER:
                intent = new Intent(ProductStatusChangeActivity.this, OrderListActivity.class);
                break;
            case Constants.MARCHANT:
                intent = new Intent(ProductStatusChangeActivity.this, MarchantListActivity.class);
                break;
            case Constants.ADMIN:
                intent = new Intent(ProductStatusChangeActivity.this, AdminOrderListActivity.class);
                break;
            default:
                System.out.println("ProductStatusChangeActivity : responseFromServer : Can't find the User type");
                break;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    private void checkingUserType(String aOrderStatus) {
        HashMap<String, String> user = session.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);
        if (Constants.ADMIN.equals(userType)) {
        } else if (Constants.MARCHANT.equals(userType)) {
            if (aOrderStatus.equals(Constants.ITEM_STATUS_QA) || aOrderStatus.equals(Constants.ITEM_STATUS_DELIVER) || aOrderStatus.equals(Constants.ITEM_STATUS_COMPLETED)) {
                Toast.makeText(ProductStatusChangeActivity.this, "Your not Changing this status tell Admin to change this status ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProductStatusChangeActivity.this, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderDataPojo);
                intent.putExtra(Constants.USER_ID, mOrderInfoPojo.getUserId());
                startActivity(intent);
                finish();
            }
        }
    }

    private void setConstantStatusValue(String aChangedStatus) {
        switch (aChangedStatus) {
            case "Assigned":
                mStatusChangedValue = Constants.ITEM_STATUS_ASSIGNED;
                break;
            case "Purchase Items":
                mStatusChangedValue = Constants.ITEM_STATUS_PURCHASED;
                break;
            case "Cutting Product":
                mStatusChangedValue = Constants.ITEM_STATUS_CUTTING;
                break;
            case "Assemble Product":
                mStatusChangedValue = Constants.ITEM_STATUS_ASSEMBLE;
                break;
            case "Polishing":
                mStatusChangedValue = Constants.ITEM_STATUS_POLISH;
                break;
            case "Quantity Analise":
                mStatusChangedValue = Constants.ITEM_STATUS_QA;
                break;
            case "Delivery":
                mStatusChangedValue = Constants.ITEM_STATUS_DELIVER;
                break;
            case "Completed":
                mStatusChangedValue = Constants.ITEM_STATUS_COMPLETED;
                break;
            case "Cancel":
                mStatusChangedValue = Constants.ITEM_STATUS_CANCELLED;
                break;
            default:
                System.out.println("No Status will selected");
        }
    }
}
