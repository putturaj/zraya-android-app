package com.example.zraya.myApplication.beans;

import java.io.Serializable;

/**
 * Created by Rajesh on 03-Jun-16.
 */
public class Custompojo implements Serializable {
    private String Meterial;
    private String DesignStyle;
    private String TopMeterial;
    private String BaseMeterial;
    private String Length;
    private String Width;
    private String Height;
    private String LegThickNess;
    private String Quantity;
    private String Amount;
    private String DateOfCustomising;

    public String getMeterial() {
        return Meterial;
    }

    public void setMeterial(String meterial) {
        Meterial = meterial;
    }

    public String getDesignStyle() {
        return DesignStyle;
    }

    public void setDesignStyle(String designStyle) {
        DesignStyle = designStyle;
    }

    public String getTopMeterial() {
        return TopMeterial;
    }

    public void setTopMeterial(String topMeterial) {
        TopMeterial = topMeterial;
    }

    public String getBaseMeterial() {
        return BaseMeterial;
    }

    public void setBaseMeterial(String baseMeterial) {
        BaseMeterial = baseMeterial;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getLegThickNess() {
        return LegThickNess;
    }

    public void setLegThickNess(String legThickNess) {
        LegThickNess = legThickNess;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getDateOfCustomising() {
        return DateOfCustomising;
    }

    public void setDateOfCustomising(String dateOfCustomising) {
        DateOfCustomising = dateOfCustomising;
    }
}
