package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.activities.AdminOrdersDetailsActivity;
import com.example.zraya.myApplication.beans.AdminOrderDetailsPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class AdminOrderDetailsPresenter implements HttpListener {

    RequestActionType mRequestActionType;
    AdminOrdersDetailsActivity orderDetail;
    PostObjects mPostObjects = new PostObjects();

    public AdminOrderDetailsPresenter(AdminOrdersDetailsActivity aOrderDetailActivity) {
        this.orderDetail = aOrderDetailActivity;
    }

    public void requestToServer(RequestActionType aRequestActionType, String aProductId, String aUserID) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createAssigningOrderMerchantBody(aUserID, aProductId).toString();

        url = Constants.ADMIN_ORDER_DETAIL_URL;

        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);

    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {

                String str = new String(httpRequest.content);
                System.out.println("Response is : " + str);

                InputStream is = new ByteArrayInputStream(httpRequest.content);
                Reader reader = new InputStreamReader(is);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();

                ArrayList<AdminOrderDetailsPojo> orderItems = (ArrayList<AdminOrderDetailsPojo>) gson.fromJson(reader,
                        new TypeToken<ArrayList<AdminOrderDetailsPojo>>() {
                        }.getType());
                orderDetail.responseFromServer(orderItems);
            } catch (JsonSyntaxException e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
