package com.example.zraya.myApplication.entities;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public enum RequestActionType {
    REGISTER(200, "register"),
    LOGIN(102, "login"),
    USER_TOKEN(101, "user_token"),
    ORDER_ONGOING(103, "order_ongoing"),
    ORDER_COMPLETED(104, "order_complete"),
    ORDER_CANCELLED(106, "order_cancelled"),
    ORDER_DETAILS(107, "order_details"),
    ORDER_STATUS_UPDATE(108, "order_update_status"),
    USER_LIST(109, "admin_user_list"),
    ADMIN_ORDERS_LIST(110, "admin_order_list"),
    POST_NEWS(105, "post_news"),
    FORGOT_PASSWORD(111, "forgot_password"),
    RESET_PASSWORD(112, "reset_password"),
    ADMIN_CONTENT(113, "admin_content"),
    WORKSTATION_LIST(114, "workstation_list");

    private int index;
    private String name;

    RequestActionType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static RequestActionType fromName(String name) {
        for (RequestActionType type : RequestActionType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return REGISTER;
    }

    public static RequestActionType fromIndex(int index) {
        for (RequestActionType type : RequestActionType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return REGISTER;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
