package com.example.zraya.myApplication.viewfactory;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.zraya.R;
import com.example.zraya.myApplication.ViewHolders.CafetariaViewHolder;
import com.example.zraya.myApplication.ViewHolders.DirectordeskViewHolder;
import com.example.zraya.myApplication.ViewHolders.OfficeChairViewholder;
import com.example.zraya.myApplication.ViewHolders.OfficeTableViewholder;
import com.example.zraya.myApplication.ViewHolders.PedestalViewholder;
import com.example.zraya.myApplication.ViewHolders.SofaViewholder;
import com.example.zraya.myApplication.ViewHolders.WhiteboardViewholder;
import com.example.zraya.myApplication.ViewHolders.WorkstationViewholder;
import com.example.zraya.myApplication.entities.ZrayaProductsType;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;


/**
 * Created by  Admin : Rajesh
 */
public class ZrayaProductsViewHolderFactory {

    public static RecyclerView.ViewHolder getViewHolder(ViewGroup parent,
                                                             OnProductItemClickListener viewOnBaseItemClickListener,
                                                             ZrayaProductsType topicCardType) {
        switch (topicCardType) {
            case CUSTOM_OFFICE_TABLES:
                return new OfficeTableViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case CUSTOM_WHITEBOARD:
                return new WhiteboardViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case OFFICE_CHAIRS:
                return new OfficeChairViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case OFFICE_TABLES:
                return new OfficeTableViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case WHITE_BOARDS:
                return new WhiteboardViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case HOME_CHAIRS:
                return new OfficeChairViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case SOFA:
                return new SofaViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case WORKSTATION:
                return new WorkstationViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case PEDESTAL:
                return new PedestalViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
//            case CUSTOM_ORDER:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case TABLE_TOP:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case TABLE_LEGS:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case CUSTOM_TABLE_TOP:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case CUSTOM_TABLE_LEGS:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case CUSTOM_REQUEST:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case CUSTOM_SHELVE:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
//            case METAL_FILE_STORAGE:
//                return new OfficeTableViewholder(createView(parent, topicCardType),
//                        viewOnBaseItemClickListener);
            case CUSTOM_SOFA:
                return new SofaViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case CUSTOM_CAFETERIA_TABLE:
                return new CafetariaViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case CUSTOM_COFFEE_TABLE:
                return new CafetariaViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case DIRECTOR_DESK_TABLE:
                return new DirectordeskViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            default:
                return new OfficeTableViewholder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
        }
    }

    private static View createView(ViewGroup parent, ZrayaProductsType booksHomeCardType) {
        switch (booksHomeCardType) {
            case CUSTOM_OFFICE_TABLES:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_list_content, parent, false);
            case CUSTOM_WHITEBOARD:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_whiteboard_order, parent, false);
            case OFFICE_CHAIRS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_officechair_order, parent, false);
            case OFFICE_TABLES:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_list_content, parent, false);
            case WHITE_BOARDS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_whiteboard_order, parent, false);
            case HOME_CHAIRS:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_officechair_order, parent, false);
            case SOFA:
            case CUSTOM_SOFA:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_sofa, parent, false);
            case WORKSTATION:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.workstation_order_list_details, parent, false);
            case PEDESTAL:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_pedestal_order, parent, false);
//            case CUSTOM_ORDER:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
//            case TABLE_TOP:
//            case CUSTOM_TABLE_TOP:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
//            case TABLE_LEGS:
//            case CUSTOM_TABLE_LEGS:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
            case CUSTOM_CAFETERIA_TABLE:
            case CUSTOM_COFFEE_TABLE:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_cafetaria_table, parent, false);
            case DIRECTOR_DESK_TABLE:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_director_desk, parent, false);
//            case CUSTOM_REQUEST:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
//            case CUSTOM_SHELVE:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
//            case METAL_FILE_STORAGE:
//                return LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.order_list_content, parent, false);
            default:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.order_list_content, parent, false);
        }
    }

}
