package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Raj on 8/4/2016.
 */
public class MarchantInfoPojo implements Serializable {

    @SerializedName("mid")
    private String marchant_id;

    @SerializedName("name")
    private String marchant_name;

    @SerializedName("phoneno")
    private String marchant_Phno;

    @SerializedName("username")
    private String marchant_username;

    @SerializedName("pwd")
    private String marchant_pwd;

    @SerializedName("marchantownername")
    private String marchant_ownerName;

    @SerializedName("marchantlogo")
    private String marchant_logo;

    @SerializedName("marchantimageurl")
    private String marchant_image_url;

    @SerializedName("location")
    private String marchant_location;

    @SerializedName("marchantaddress")
    private String marchant_address;

    @SerializedName("timingsstart")
    private String marchant_starttime;

    @SerializedName("timingsend")
    private String marchant_endtime;

    @SerializedName("contactpersonMobile")
    private String marchant_contactPerson;

    private TokenPojo tokenpojo;

    public String getMarchant_id() {
        return marchant_id;
    }

    public void setMarchant_id(String marchant_id) {
        this.marchant_id = marchant_id;
    }

    public String getMarchant_name() {
        return marchant_name;
    }

    public void setMarchant_name(String marchant_name) {
        this.marchant_name = marchant_name;
    }

    public String getMarchant_Phno() {
        return marchant_Phno;
    }

    public void setMarchant_Phno(String marchant_Phno) {
        this.marchant_Phno = marchant_Phno;
    }

    public String getMarchant_username() {
        return marchant_username;
    }

    public void setMarchant_username(String marchant_username) {
        this.marchant_username = marchant_username;
    }

    public String getMarchant_pwd() {
        return marchant_pwd;
    }

    public void setMarchant_pwd(String marchant_pwd) {
        this.marchant_pwd = marchant_pwd;
    }

    public String getMarchant_ownerName() {
        return marchant_ownerName;
    }

    public void setMarchant_ownerName(String marchant_ownerName) {
        this.marchant_ownerName = marchant_ownerName;
    }

    public String getMarchant_location() {
        return marchant_location;
    }

    public void setMarchant_location(String marchant_location) {
        this.marchant_location = marchant_location;
    }

    public String getMarchant_address() {
        return marchant_address;
    }

    public void setMarchant_address(String marchant_address) {
        this.marchant_address = marchant_address;
    }

    public String getMarchant_starttime() {
        return marchant_starttime;
    }

    public void setMarchant_starttime(String marchant_starttime) {
        this.marchant_starttime = marchant_starttime;
    }

    public String getMarchant_endtime() {
        return marchant_endtime;
    }

    public void setMarchant_endtime(String marchant_endtime) {
        this.marchant_endtime = marchant_endtime;
    }

    public String getMarchant_contactPerson() {
        return marchant_contactPerson;
    }

    public void setMarchant_contactPerson(String marchant_contactPerson) {
        this.marchant_contactPerson = marchant_contactPerson;
    }

    public String getMarchant_logo() {
        return marchant_logo;
    }

    public void setMarchant_logo(String marchant_logo) {
        this.marchant_logo = marchant_logo;
    }

    public String getMarchant_image_url() {
        return marchant_image_url;
    }

    public void setMarchant_image_url(String marchant_image_url) {
        this.marchant_image_url = marchant_image_url;
    }

    public TokenPojo getTokenpojo() {
        return tokenpojo;
    }

    public void setTokenpojo(TokenPojo tokenpojo) {
        this.tokenpojo = tokenpojo;
    }
}
