package com.example.zraya.myApplication.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.adapters.ListRightDrawerAdapter;
import com.example.zraya.myApplication.beans.AdminContentDetailsPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.fragments.AdminOrderListFragment;
import com.example.zraya.myApplication.listeners.OnDrawerItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Raj on 30-Jun-16.
 */
public class AdminOrderListActivity extends BaseDrawerActivity implements NavigationView.OnNavigationItemSelectedListener, OnDrawerItemClickListener, View.OnClickListener {
    private AdminOrderListFragment mAdminOrderListFragment;
    UserInfoPojo userInfo = new UserInfoPojo();
    UserSessionManager session;
    public HashMap<String, String> user;

    boolean doubleBackToExitPressedOnce = false;

    Context mContext = this;
    AdminContentDetailsPojo adminContentDisplay = null;
    NavigationView rightNavigationView;
    ArrayList<String> mProdStatus = new ArrayList<>();
    private static ListRightDrawerAdapter mListRightDrawerAdapter;
    DrawerLayout drawer;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        showNavigationDrawer(true);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        session = new UserSessionManager(getApplicationContext());
        getLoginUserDetails();

        rightNavigationView = (NavigationView) findViewById(R.id.nav_right_view);
        rightNavigationView.setNavigationItemSelectedListener(this);
        mAdminOrderListFragment = AdminOrderListFragment.newInstance(Constants.TAB_ORDERS, userInfo
                .getUser_email(), userInfo.getUser_id());

        rightDrawerSetUp();

        getFragmentManager().beginTransaction().replace(R.id.fragment_container, mAdminOrderListFragment,
                Constants.TAB_ORDERS).commit();


        setOnclickListener();
        setTitle("Order Lists");
    }


    public void getLoginUserDetails() {
        user = session.getUserDetails();
        userInfo.setUser_email(user.get(UserSessionManager.USER_EMAIL));
        userInfo.setUser_id(user.get(UserSessionManager.USER_ID));
        userInfo.setUser_name(user.get(UserSessionManager.USER_NAME));
        userInfo.setUser_phNo(user.get(UserSessionManager.USER_PHONE));
    }

    private void setOnclickListener() {
//        mToolBackImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }


    public void responseSplashPresenter(ArrayList<AdminContentDetailsPojo> aAdminContentDetailsPojo) {
        for (int i = 0; i < aAdminContentDetailsPojo.size(); i++) {
            adminContentDisplay = aAdminContentDetailsPojo.get(i);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadAdminContent();
            }
        });
    }


    private void rightDrawerSetUp() {
        String[] rightfilters = Constants.status;
        for (int index = 0; index < rightfilters.length; index++) {
            mProdStatus.add(rightfilters[index]);
        }
        listView = (ListView) rightNavigationView.findViewById(R.id.list);
        mListRightDrawerAdapter = new ListRightDrawerAdapter(mProdStatus, getApplicationContext()
                , AdminOrderListActivity.this);
        listView.setAdapter(mListRightDrawerAdapter);
    }


    private void loadAdminContent() {
        showAdmincontent(adminContentDisplay);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void onItemClick(View view, String statusSelected) {
        int j = 0;
        String[] filtersItems = Constants.status;
        //
        String[] duplicatingStatusv = new String[20];
        duplicatingStatusv[j] = statusSelected;
        for (int index = 0; index < filtersItems.length; index++) {
            if (!filtersItems[index].equalsIgnoreCase(statusSelected)) {
                duplicatingStatusv[++j] = filtersItems[index];
            } else {
                ++index;
            }
        }

        mAdminOrderListFragment.rightStatusClick(duplicatingStatusv, user);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
    }


    //
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }
}
