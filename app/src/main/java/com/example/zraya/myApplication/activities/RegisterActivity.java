package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.AlertDialogManager;
import com.example.zraya.myApplication.Utils.CommonUtilities;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.MarchantInfoPojo;
import com.example.zraya.myApplication.beans.TokenPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.MarchantRegisterPresenter;
import com.example.zraya.myApplication.presenters.UserRegisterPresenter;

import java.util.ArrayList;

/**
 * Created by Raj on 8/1/2016.
 */
public class RegisterActivity extends AppCompatActivity {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();

    // Internet detector
    ConnectionDetector cd;

    // UI elements
    // users
    EditText mUserName, mUserphoneNo, mUserlocation, mUserpwd, mUserEmail;
    // Merchants
    EditText marName, marUsername, marPhno, marPwd, marLocation, marAddress, marOwnername, marStarttime, marEndtime, marContactpersonName;

    // Register button
    Button btnRegister;

    Spinner mUserType;
    String mSelectUserType;
    String mUserTypes[] = {Constants.REG_TYPE_USER, Constants.REG_TYPE_MARCHANT};
    ArrayAdapter<String> mAdapterBusinessType;
    LinearLayout mUserRegister, mMarchantRegister;
    TextView mTitleText;

    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;

    //     User Info
    String regName;
    String regEmail;
    String regPhone;
    String regPwd;
    String regLocation;

    //    Merchant Info
    String wstName;
    String wstUserName;
    String wstPwd;
    String wstPh;
    String wstOwnerName;
    String wstLoc;
    String wstAdd;
    String wstStTime;
    String wstEndTime;
    String wstContactNumber;

    UserInfoPojo mUserinfopojo;
    TokenPojo mTokenPojo;
    MarchantInfoPojo mMerchantinfopojo;
    LoginActivity mainActivity;
    UserSessionManager session;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        cd = new ConnectionDetector(getApplicationContext());
        session = new UserSessionManager(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);
        mToolbarTitle.setText("Register");
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(RegisterActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        setSpinner();
        // Check if GCM configuration is set
        if (CommonUtilities.SERVER_URL == null || CommonUtilities.SENDER_ID == null || CommonUtilities.SERVER_URL.length() == 0
                || CommonUtilities.SENDER_ID.length() == 0) {
            // GCM sernder id / server url is missing
            alert.showAlertDialog(RegisterActivity.this, "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
            // stop executing code by return
            return;
        }

        mTitleText = (TextView) findViewById(R.id.title_register);
        // User Edit Fields
        mUserName = (EditText) findViewById(R.id.txtName);
        mUserEmail = (EditText) findViewById(R.id.txtEmail);
        mUserpwd = (EditText) findViewById(R.id.txtPwd);
        mUserphoneNo = (EditText) findViewById(R.id.txtPh);
        mUserlocation = (EditText) findViewById(R.id.location);

        // Merchant Edit Fields
        marName = (EditText) findViewById(R.id.mar_reg_name);
        marUsername = (EditText) findViewById(R.id.mar_reg_email);
        marPwd = (EditText) findViewById(R.id.mar_reg_password);
        marPhno = (EditText) findViewById(R.id.mar_reg_phNo);
        marLocation = (EditText) findViewById(R.id.mar_reg_location);
        marOwnername = (EditText) findViewById(R.id.mar_reg_owner_name);
        marAddress = (EditText) findViewById(R.id.mar_reg_address);
        marStarttime = (EditText) findViewById(R.id.mar_reg_start_time);
        marEndtime = (EditText) findViewById(R.id.mar_reg_end_time);
        marContactpersonName = (EditText) findViewById(R.id.mar_reg_contact_phno);


        btnRegister = (Button) findViewById(R.id.btnRegister);

        mUserRegister = (LinearLayout) findViewById(R.id.register_user_layout);
        mMarchantRegister = (LinearLayout) findViewById(R.id.register_marchant_layout);

        mUserRegister.setVisibility(View.GONE);
        mMarchantRegister.setVisibility(View.GONE);
        /*
         * Click event on Register button
         * */
        btnRegister.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Read EditText dat
                if (mSelectUserType == Constants.REG_TYPE_USER) {
                    userValueRegistering();
                } else if (mSelectUserType == Constants.REG_TYPE_MARCHANT) {
                    merchantValueRegistering();
                } else {
                    alert.showAlertDialog(RegisterActivity.this, "Please select user type!", "", false);
                }
            }
        });

        mToolBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void setSpinner() {
        mUserType = (Spinner) findViewById(R.id.userType);
        mAdapterBusinessType = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, mUserTypes);
        mAdapterBusinessType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mUserType.setAdapter(mAdapterBusinessType);
        mUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                mSelectUserType = adapter.getItemAtPosition(position).toString();
                // Showing selected spinner item
                if (mSelectUserType == Constants.REG_TYPE_USER) {
                    mMarchantRegister.setVisibility(View.GONE);
                    mUserRegister.setVisibility(View.VISIBLE);
                    mTitleText.setText("User Register");
                } else if (mSelectUserType == Constants.REG_TYPE_MARCHANT) {
                    mUserRegister.setVisibility(View.GONE);
                    mMarchantRegister.setVisibility(View.VISIBLE);
                    mTitleText.setText("Marchant Register");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    private void userValueRegistering() {
        if (verifyUserValues()) {
            UserRegisterPresenter userRegisterPresenter = new UserRegisterPresenter(this);
            mUserinfopojo = new UserInfoPojo();
            TokenPojo tokenPojo = new TokenPojo();
            mUserinfopojo.setUser_name(regName);
            mUserinfopojo.setUser_email(regEmail);
            mUserinfopojo.setUser_pwd(regPwd);
            mUserinfopojo.setUser_phNo(regPhone);
            mUserinfopojo.setUser_location(regLocation);
            mUserinfopojo.setUser_isAdmin(mSelectUserType);
            tokenPojo.setToken(session.getToken());
            mUserinfopojo.setTokenpojo(tokenPojo);
//        alert.showAlertDialog(RegisterActivity.this, "Normal user", "Register", false);
            userRegisterPresenter.requestToServer(RequestActionType.REGISTER, mUserinfopojo);
        }
    }

    private boolean verifyUserValues() {
        regName = mUserName.getText().toString();
        regEmail = mUserEmail.getText().toString();
        regPwd = mUserpwd.getText().toString();
        regPhone = mUserphoneNo.getText().toString().trim();
        regLocation = mUserlocation.getText().toString().trim();

        if (Utils.isEmpty(regName)) {
            Toast.makeText(this, "Please enter the name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(regEmail)) {
            Toast.makeText(this, "Please enter the email", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(regPhone)) {
            Toast.makeText(this, "Please enter the phone number", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(regPwd)) {
            Toast.makeText(this, "Please enter the City", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(regLocation)) {
            Toast.makeText(this, "Please enter the Location", Toast.LENGTH_LONG).show();
            return false;
        }

        return false;
    }


    private void merchantValueRegistering() {
        if (verifyMerchantValues()) {
            MarchantRegisterPresenter merchantRegisterPresenter = new MarchantRegisterPresenter(this);
            mMerchantinfopojo = new MarchantInfoPojo();
            TokenPojo tokenPojo = new TokenPojo();
            mMerchantinfopojo.setMarchant_name(wstName);
            mMerchantinfopojo.setMarchant_username(wstUserName);
            mMerchantinfopojo.setMarchant_pwd(wstPwd);
            mMerchantinfopojo.setMarchant_Phno(wstPh);
            mMerchantinfopojo.setMarchant_ownerName(wstOwnerName);
            mMerchantinfopojo.setMarchant_location(wstLoc);
            mMerchantinfopojo.setMarchant_address(wstAdd);
            mMerchantinfopojo.setMarchant_starttime(wstStTime);
            mMerchantinfopojo.setMarchant_endtime(wstEndTime);
            mMerchantinfopojo.setMarchant_contactPerson(wstContactNumber);
            tokenPojo.setToken(session.getToken());
            mMerchantinfopojo.setTokenpojo(tokenPojo);
            merchantRegisterPresenter.requestToServer(RequestActionType.REGISTER, mMerchantinfopojo);
        }
    }

    private boolean verifyMerchantValues() {
        wstName = marName.getText().toString();
        wstUserName = marUsername.getText().toString();
        wstPwd = marPwd.getText().toString();
        wstPh = marPhno.getText().toString();
        wstOwnerName = marOwnername.getText().toString();
        wstLoc = marLocation.getText().toString();
        wstAdd = marAddress.getText().toString();
        wstStTime = marStarttime.getText().toString();
        wstEndTime = marEndtime.getText().toString();
        wstContactNumber = marContactpersonName.getText().toString();

        if (Utils.isEmpty(wstName)) {
            Toast.makeText(this, "Please enter the name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstUserName)) {
            Toast.makeText(this, "Please enter the Email", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstPwd)) {
            Toast.makeText(this, "Please enter the password", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstPh)) {
            Toast.makeText(this, "Please enter the phone number", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstOwnerName)) {
            Toast.makeText(this, "Please enter the owner name", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstLoc)) {
            Toast.makeText(this, "Please enter the Location", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstAdd)) {
            Toast.makeText(this, "Please enter the Address", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstStTime)) {
            Toast.makeText(this, "Please enter the Start time", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstEndTime)) {
            Toast.makeText(this, "Please enter the End Time", Toast.LENGTH_LONG).show();
            return false;
        }
        if (Utils.isEmpty(wstContactNumber)) {
            Toast.makeText(this, "Please enter the Contact Number", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public void responseLoginPresenter(ArrayList<UserInfoPojo> aBaseItems) {
//        mainActivity.responseLoginPresenter(baseItems);
        Intent intent = new Intent();
        intent.putExtra("register", aBaseItems);
        setResult(RESULT_OK, intent);
        finish();
    }
}