package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.ProductDisplayMainPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;

/**
 * Created by Raj on 11/2/2016.
 */

public class OrderListViewholder {
    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public TextView orderName;
    public TextView userName;
    public TextView dateOfOrder;
    public TextView mUserPhone;
    public TextView mUserEmail;
    public ImageView mProductImage;
    private View container;
    Context mContext;
    OrdersData mOrdersData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Utils mUtil = new Utils();
    public IJRModelData mIjrmodelData;
    public ProductDisplayMainPojo mProductDisplayPojo;

    public OrderListViewholder(View itemView, Context aContext) {
        container = itemView;
        mContext = aContext;
        orderName = (TextView) itemView.findViewById(R.id.order_name);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        dateOfOrder = (TextView) itemView.findViewById(R.id.date_of_order);
        mUserPhone = (TextView) itemView.findViewById(R.id.ph_number);
        mUserEmail = (TextView) itemView.findViewById(R.id.user_email);
        mProductImage = (ImageView) itemView.findViewById(R.id.prodImg);
    }

    public void updateUI(int position, IJRModelData modelData) {
        mOrdersData = (OrdersData) modelData;
        mIjrmodelData = (IJRModelData) mOrdersData.getProductObject();
        orderInfo = mOrdersData.getOrderObj();
        userInfoPojo = mOrdersData.getUserObj();
        mProductDisplayPojo = mOrdersData.getmProductDisplayMainPojo();

        orderName.setText(mProductDisplayPojo.getName());
        userName.setText(userInfoPojo.getUser_name());
        mUserEmail.setText(userInfoPojo.getUser_email());
        mUserPhone.setText(userInfoPojo.getUser_phNo());
        dateOfOrder.setText("" + mUtil.getDate(orderInfo.getDate()));

        if (!Utils.isEmpty(mProductDisplayPojo.getImgurl())) {
//                String url = URLEncoder.encode(Constants.baseUrl + "" + mProductDisplayPojo.getImgurl(), "utf-8");
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mProductDisplayPojo.getImgurl())).into(mProductImage);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.zrayaLogo).into(mProductImage);
        }

        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrdersData);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                mContext.startActivity(intent);
            }
        });
    }
}
