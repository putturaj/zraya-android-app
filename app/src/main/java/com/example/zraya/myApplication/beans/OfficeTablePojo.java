package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rajesh on 28-Apr-16.
 */
public class OfficeTablePojo extends IJRModelData {
    @SerializedName("ID")
    private String tableID;
    @SerializedName("Name")
    private String tableName;
    @SerializedName("Length")
    private String tableLength;
    @SerializedName("Width")
    private String tableWidth;
    @SerializedName("Height")
    private String tableHeight;
    @SerializedName("legthick")
    private String tableLegThick;
    @SerializedName("Description")
    private String tableDesc;
    @SerializedName("FinishType")
    private String tableType;

    @SerializedName("topmaterial")
    private String tableTopMat;
    @SerializedName("basematerial")
    private String tableBaseMat;
    @SerializedName("topcolor")
    private String tableTopColor;
    @SerializedName("basecolor")
    private String tableBaseColor;
    @SerializedName("ImageURL")
    private String tableImgUrl;
    @SerializedName("Amount")
    private String tableAmount;
    @SerializedName("productid")
    private String tableProductID;


    @SerializedName("BrandName")
    private String brandName;
    @SerializedName("TableStyle")
    private String tableStyle;
    @SerializedName("SuitableFor")
    private String suitableFor;
    @SerializedName("ModelNumber")
    private String modelnumber;
    @SerializedName("CareInstructions")
    private String careInstruction;
    @SerializedName("DrawerInclude")
    private String drawerInclude;
    @SerializedName("imageUrls")
    private String imageUrls;


    private OrderInfo orderProduct;

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableLength() {
        return tableLength;
    }

    public void setTableLength(String tableLength) {
        this.tableLength = tableLength;
    }

    public String getTableWidth() {
        return tableWidth;
    }

    public void setTableWidth(String tableWidth) {
        this.tableWidth = tableWidth;
    }

    public String getTableHeight() {
        return tableHeight;
    }

    public void setTableHeight(String tableHeight) {
        this.tableHeight = tableHeight;
    }

    public String getTableLegThick() {
        return tableLegThick;
    }

    public void setTableLegThick(String tableLegThick) {
        this.tableLegThick = tableLegThick;
    }

    public String getTableDesc() {
        return tableDesc;
    }

    public void setTableDesc(String tableDesc) {
        this.tableDesc = tableDesc;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getTableTopMat() {
        return tableTopMat;
    }

    public void setTableTopMat(String tableTopMat) {
        this.tableTopMat = tableTopMat;
    }

    public String getTableBaseMat() {
        return tableBaseMat;
    }

    public void setTableBaseMat(String tableBaseMat) {
        this.tableBaseMat = tableBaseMat;
    }

    public String getTableTopColor() {
        return tableTopColor;
    }

    public void setTableTopColor(String tableTopColor) {
        this.tableTopColor = tableTopColor;
    }

    public String getTableBaseColor() {
        return tableBaseColor;
    }

    public void setTableBaseColor(String tableBaseColor) {
        this.tableBaseColor = tableBaseColor;
    }

    public String getTableImgUrl() {
        return tableImgUrl;
    }

    public void setTableImgUrl(String tableImgUrl) {
        this.tableImgUrl = tableImgUrl;
    }

    public String getTableAmount() {
        return tableAmount;
    }

    public void setTableAmount(String tableAmount) {
        this.tableAmount = tableAmount;
    }

    public String getTableProductID() {
        return tableProductID;
    }

    public void setTableProductID(String tableProductID) {
        this.tableProductID = tableProductID;
    }


    public OrderInfo getOrderProduct() {
        return orderProduct;
    }

    public void setOrderProduct(OrderInfo orderProduct) {
        this.orderProduct = orderProduct;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getTableStyle() {
        return tableStyle;
    }

    public void setTableStyle(String tableStyle) {
        this.tableStyle = tableStyle;
    }

    public String getSuitableFor() {
        return suitableFor;
    }

    public void setSuitableFor(String suitableFor) {
        this.suitableFor = suitableFor;
    }

    public String getModelnumber() {
        return modelnumber;
    }

    public void setModelnumber(String modelnumber) {
        this.modelnumber = modelnumber;
    }

    public String getCareInstruction() {
        return careInstruction;
    }

    public void setCareInstruction(String careInstruction) {
        this.careInstruction = careInstruction;
    }

    public String getDrawerInclude() {
        return drawerInclude;
    }

    public void setDrawerInclude(String drawerInclude) {
        this.drawerInclude = drawerInclude;
    }

    public String getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String imageUrls) {
        this.imageUrls = imageUrls;
    }
}
