package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Raj on 7/10/2016.
 */
public class AdminContentDetailsPojo implements Serializable {
    @SerializedName("workstation_count")
    private String aWorkStationCount;
    @SerializedName("user_count")
    private String aUserCount;

    public String getaWorkStationCount() {
        return aWorkStationCount;
    }

    public void setaWorkStationCount(String aWorkStationCount) {
        this.aWorkStationCount = aWorkStationCount;
    }

    public String getaUserCount() {
        return aUserCount;
    }

    public void setaUserCount(String aUserCount) {
        this.aUserCount = aUserCount;
    }
}
