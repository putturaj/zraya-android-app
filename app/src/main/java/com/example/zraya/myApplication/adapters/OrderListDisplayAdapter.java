package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.ViewHolders.CafetariaViewHolder;
import com.example.zraya.myApplication.ViewHolders.CustomRequestViewHolder;
import com.example.zraya.myApplication.ViewHolders.DirectordeskViewHolder;
import com.example.zraya.myApplication.ViewHolders.OfficeChairViewholder;
import com.example.zraya.myApplication.ViewHolders.OfficeTableViewholder;
import com.example.zraya.myApplication.ViewHolders.PedestalViewholder;
import com.example.zraya.myApplication.ViewHolders.SofaViewholder;
import com.example.zraya.myApplication.ViewHolders.WhiteboardViewholder;
import com.example.zraya.myApplication.ViewHolders.WorkstationViewholder;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrdersData;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class OrderListDisplayAdapter extends ArrayAdapter<OrdersData> {

    private Context mContext;
    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    public String userID;

    /*************
     * CustomAdapter Constructor
     *****************/
    public OrderListDisplayAdapter(Context context, ArrayList<OrdersData> tablepojos, String userId) {
        super(context, 0, tablepojos);
        data = tablepojos;
        userID = userId;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        OrdersData ordersData;
        ordersData = (OrdersData) data.get(position);
        if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_OFFICE_TABLE) ||
                ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_OFFICE_TABLE)) {
            OfficeTableViewholder officeTableHolder;
            itemView = inflater.inflate(R.layout.order_list_content, null);
            officeTableHolder = new OfficeTableViewholder(itemView, mContext);
            officeTableHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_WORKSTATION)) {
            WorkstationViewholder workStationHolder;
            itemView = inflater.inflate(R.layout.workstation_order_list_details, null);
            workStationHolder = new WorkstationViewholder(itemView, mContext);
            workStationHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_SOFAS) ||
                ordersData.getOrderObj().getItemCategoryID().equals(Constants.CUSTOM_SOFA)) {
            SofaViewholder sofaViewholder;
            itemView = inflater.inflate(R.layout.activity_sofa, null);
            sofaViewholder = new SofaViewholder(itemView, mContext);
            sofaViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_OFFICE_CHAIR)) {
            OfficeChairViewholder officechairViewholder;
            itemView = inflater.inflate(R.layout.activity_officechair_order, null);
            officechairViewholder = new OfficeChairViewholder(itemView, mContext);
            officechairViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_WHITEBOARD)) {
            WhiteboardViewholder whiteboardViewholder;
            itemView = inflater.inflate(R.layout.activity_whiteboard_order, null);
            whiteboardViewholder = new WhiteboardViewholder(itemView, mContext);
            whiteboardViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_PEDESTAL)) {
            PedestalViewholder pedestalViewholder;
            itemView = inflater.inflate(R.layout.activity_pedestal_order, null);
            pedestalViewholder = new PedestalViewholder(itemView, mContext);
            pedestalViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CATEGORY_CUSTOM_ORDER)) {
            CustomRequestViewHolder customrequestViewholder;
            itemView = inflater.inflate(R.layout.activity_custom_upload_order, null);
            customrequestViewholder = new CustomRequestViewHolder(itemView, mContext);
            customrequestViewholder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.CAFETARIA_TABLE)) {
            CafetariaViewHolder cafetariaViewHolder;
            itemView = inflater.inflate(R.layout.activity_cafetaria_table, null);
            cafetariaViewHolder = new CafetariaViewHolder(itemView, mContext);
            cafetariaViewHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.COFFEE_TABLE)) {
            CafetariaViewHolder cafetariaViewHolder;
            itemView = inflater.inflate(R.layout.activity_cafetaria_table, null);
            cafetariaViewHolder = new CafetariaViewHolder(itemView, mContext);
            cafetariaViewHolder.updateUI(position, (IJRModelData) data.get(position));
        } else if (ordersData.getOrderObj().getItemCategoryID().equals(Constants.DIRECTOR_DESK)) {
            DirectordeskViewHolder directordeskViewHolder;
            itemView = inflater.inflate(R.layout.activity_director_desk, null);
            directordeskViewHolder = new DirectordeskViewHolder(itemView, mContext);
            directordeskViewHolder.updateUI(position, (IJRModelData) data.get(position));
        }

        return itemView;
    }
}
