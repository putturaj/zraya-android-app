package com.example.zraya.myApplication.Utils;

import com.example.zraya.myApplication.beans.MarchantInfoPojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrderStatusPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by bheemesh on 14/3/16.
 *
 * @author bheemesh
 */
public class PostObjects {

    public static JSONObject createRegisterPostBody(UserInfoPojo userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", "test");
            object.put("password", "test");
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createUserIdPostBody(String mUserId) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_ID, mUserId);
        } catch (Exception ex) {
        }
        return object;
    }

    public static JSONObject createOrderIdPostBody(String aOrderId) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ORDER_ID, aOrderId);
        } catch (Exception ex) {
        }
        return object;
    }

    public static JSONObject createOrderDataPostBody(OrderInfo aOrderInfo) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ORDER_ID, aOrderInfo.getOrderId());
            object.put(Constants.PRODUCT_ID, aOrderInfo.getProductID());
            object.put(Constants.USER_ID, aOrderInfo.getUserId());
        } catch (Exception ex) {
        }
        return object;
    }

    public static JSONObject createEmailPostBody(String aEmail) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_EMAIL, aEmail);
        } catch (Exception ex) {
        }
        return object;
    }

    public static JSONObject createResetPwdPostBody(Map<String, String> aForgotPwdValues) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.RESET_PASSWORD_TOKEN, aForgotPwdValues.get(Constants.RESET_PASSWORD_TOKEN));
            object.put(Constants.USER_EMAIL, aForgotPwdValues.get(Constants.USER_EMAIL));
            object.put(Constants.USER_ID, aForgotPwdValues.get(Constants.USER_ID));
            object.put(Constants.RESET_PASSWORD, aForgotPwdValues.get(Constants.RESET_PASSWORD));
        } catch (Exception ex) {
        }
        return object;
    }


    public static JSONObject createAdminInfoPostBody(String userEmail, String userId) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_EMAIL, userEmail);
            object.put(Constants.USER_ID, userId);
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createLoginPostBody(UserInfoPojo aUserInfoPojo) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_EMAIL, aUserInfoPojo.getUser_email());
            object.put(Constants.USER_PWD, aUserInfoPojo.getUser_pwd());
            object.put(Constants.TOKEN_TO_SERVER, aUserInfoPojo.getTokenpojo().getToken());
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createUserRegisterPostBody(UserInfoPojo userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_NAME, userDetails.getUser_name());
            object.put(Constants.USER_EMAIL, userDetails.getUser_email());
            object.put(Constants.USER_PWD, userDetails.getUser_pwd());
            object.put(Constants.USER_PHONE, userDetails.getUser_phNo());
            object.put(Constants.USER_LOCATION, userDetails.getUser_location());
            object.put(Constants.USER_IS_ADMIN, userDetails.getUser_isAdmin());
            object.put(Constants.TOKEN_TO_SERVER, userDetails.getTokenpojo().getToken());
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createMerchanRegisterPostBody(MarchantInfoPojo mMerchantInfoPojo) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.MERCHANT_ID, mMerchantInfoPojo.getMarchant_id());
            object.put(Constants.MERCHANT_NAME, mMerchantInfoPojo.getMarchant_name());
            object.put(Constants.MERCHANT_EMAIL, mMerchantInfoPojo.getMarchant_username());
            object.put(Constants.MERCHANT_PWD, mMerchantInfoPojo.getMarchant_pwd());
            object.put(Constants.MERCHANT_OWNER_NAME, mMerchantInfoPojo.getMarchant_ownerName());
            object.put(Constants.MERCHANT_PHONE, mMerchantInfoPojo.getMarchant_Phno());
            object.put(Constants.MERCHANT_ADDRESS, mMerchantInfoPojo.getMarchant_address());
            object.put(Constants.MERCHANT_LOCATION, mMerchantInfoPojo.getMarchant_location());
            object.put(Constants.MERCHANT_START_TIME, mMerchantInfoPojo.getMarchant_starttime());
            object.put(Constants.MERCHANT_END_TIME, mMerchantInfoPojo.getMarchant_endtime());
            object.put(Constants.MERCHANT_CONTACT_NUMBER, mMerchantInfoPojo.getMarchant_contactPerson());
            object.put(Constants.TOKEN_TO_SERVER, mMerchantInfoPojo.getTokenpojo().getToken());


        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createUserTokenPostBody(UserInfoPojo userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", "thatzprem");
            object.put("password", "P@ssw0rd");
        } catch (Exception ex) {

        }
        return object;
    }


    public static JSONObject createAssigningOrderMerchantBody(String mUserID, String mProductID) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.USER_ID, mUserID);
            object.put(Constants.ORDER_PRODUCT_ID, mProductID);
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createOrderStatusBody(OrderStatusPojo mOrderStatus) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.STATUS_ORDER_ID, mOrderStatus.getStatusOrderID());
            object.put(Constants.STATUS_STATUS, mOrderStatus.getStatusStatus());
            object.put(Constants.STATUS_COMMENT, mOrderStatus.getStatusComment());
            object.put(Constants.STATUS_DATE, mOrderStatus.getStatusDate());
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createAssigningOrderMerchantBody(OrderInfo mOrderinfo, String mMerchantID) {
        JSONObject object = new JSONObject();
        try {
            object.put(Constants.ORDER_ID, mOrderinfo.getOrderId());
            object.put(Constants.PRODUCT_ID, mOrderinfo.getProductID());
            object.put(Constants.USER_ID, mOrderinfo.getUserId());
            object.put(Constants.STATUS, mOrderinfo.getProductStatus());
            object.put(Constants.ORDER_CATEGORY, mOrderinfo.getItemCategoryID());
            object.put(Constants.ORDER_POSITOIN, mOrderinfo.getOrderPosition());
            object.put(Constants.IS_ASSIGN, mOrderinfo.getIsOrderAssigned());
            object.put(Constants.MERCHANT_ID, mMerchantID);

        } catch (Exception ex) {

        }
        return object;
    }

    public static String createPostNews(JSONObject obj) {
        JSONObject item = new JSONObject();
        try {

            item.put("headline", "Test Pakistan Must do Justice on Mumbai Attack: US Official");
            item.put("content", "Test The US will keep pressing Pakistan to help India get " +
                    "justice " +
                    "for the Mumbai terror attack, a senior US official said on Wednesday.");
            item.put("pub_date", null);
            item.put("agrees_count", "4");
            item.put("disagrees_count", "5");
            item.put("long_position", "12.050");
            item.put("lat_position", "17.606");

        } catch (Exception ex) {

        }
        return item.toString();
    }
}
