package com.example.zraya.myApplication.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.zraya.myApplication.activities.LoginActivity;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.HashMap;

/**
 * Created by Rajesh on 28-Jun-16.
 */
public class UserSessionManager {
    // Shared Preferences reference
    SharedPreferences pref;

    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "AndroidExamplePref";

    // All Shared Preferences Keys
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    // (make variable public to access from outside)

    public static final String USER_NAME = "user_name";
    public static final String USER_ID = "user_id";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PWD = "user_pwd";
    public static final String USER_PHONE = "user_phNo";
    public static final String USER_LOCATION = "user_location";
    public static final String USER_IS_ADMIN = "user_isAdmin";
    public static final String SET_NOTIFY = "set_notify";


    // Constructor
    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void createUserLoginSession(UserInfoPojo mUserInfoPojo) {
        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);

        // Storing name in pref
        editor.putString(USER_ID, mUserInfoPojo.getUser_id());
        editor.putString(USER_NAME, mUserInfoPojo.getUser_name());
        editor.putString(USER_EMAIL, mUserInfoPojo.getUser_email());
        editor.putString(USER_PWD, mUserInfoPojo.getUser_pwd());
        editor.putString(USER_PHONE, mUserInfoPojo.getUser_phNo());
        editor.putString(USER_LOCATION, mUserInfoPojo.getUser_location());
        editor.putString(USER_IS_ADMIN, mUserInfoPojo.getUser_isAdmin());

        // commit changes
        editor.commit();
    }

    /**
     * Check login method will check user login status
     * If false it will redirect user to login page
     * Else do anything
     */
    public boolean checkLogin() {
        // Check login status
        if (!this.isUserLoggedIn()) {

            // user is not logged in redirect him to Login Activity
//            Intent i = new Intent(_context, LoginActivity.class);
//
//            // Closing all the Activities from stack
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//            // Add new Flag to start new Activity
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
//            _context.startActivity(i);

            return true;
        }
        return false;
    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));
        user.put(USER_PWD, pref.getString(USER_PWD, null));
        user.put(USER_PHONE, pref.getString(USER_PHONE, null));
        user.put(USER_LOCATION, pref.getString(USER_LOCATION, null));
        user.put(USER_IS_ADMIN, pref.getString(USER_IS_ADMIN, null));
        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    // Check for login
    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }


    public void saveToken(Context applicationContext, String refreshedToken) {
        editor.putString(Constants.TOKEN_ID, refreshedToken);
        editor.commit();
    }


    public String getToken() {
        return pref.getString(Constants.TOKEN_ID, null);
    }

    public void saveNotificationSubscription(boolean value) {
        SharedPreferences.Editor edits = pref.edit();
        edits.putBoolean(SET_NOTIFY, value);
        edits.apply();
    }
}
