package com.example.zraya.myApplication.presenters;

import android.content.Context;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.activities.AdminOrderListActivity;
import com.example.zraya.myApplication.beans.AdminContentDetailsPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 30-Jun-16.
 */
public class AdminContentValuesPresenter implements HttpListener {
    private AdminOrderListActivity mAdminOrderListActivity;
    RequestActionType mRequestActionType;
    Context mContext;
    PostObjects mPostObjects = new PostObjects();

    public AdminContentValuesPresenter(Context context) {
        this.mContext = context;
    }


    public void requestToServer(RequestActionType aRequestActionType, UserInfoPojo aUserInfoPojo) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createLoginPostBody(aUserInfoPojo).toString();

        url = Constants.RETRIEVE_ADMIN_FULL_DETAILS;

        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);

    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present AdminContentViewDisplay handleHttpResponse : " + mRequestActionType
                .getName());
        JSONObject json_data = null;

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present AdminContentViewDisplay handleHttpResponse : Empty response");
        } else {
            System.out.println("ZRAYA : Present AdminContentViewDisplay handleHttpResponse : Got response : " + new
                    String(httpRequest.content));
            System.out.println("ZRAYA : Present AdminContentViewDisplay handleHttpResponse : Got response  CODE : " + httpRequest.responseType);
            try {
                String str = new String(httpRequest.content);
                System.out.println("Login Response : " + str);
                InputStream is = new ByteArrayInputStream(httpRequest.content);
                Reader reader = new InputStreamReader(is);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();

                ArrayList<AdminContentDetailsPojo> contentList = (ArrayList<AdminContentDetailsPojo>) gson.fromJson(reader,
                        new TypeToken<ArrayList<AdminContentDetailsPojo>>() {
                        }.getType());
                ArrayList<AdminContentDetailsPojo> adminContentDetailsPojo = (ArrayList<AdminContentDetailsPojo>) contentList;
                ((AdminOrderListActivity) mContext).responseSplashPresenter(adminContentDetailsPojo);
            } catch (JsonSyntaxException e) {
                System.out.println("JsonSyntaxException : AdminContentViewDisplay Presenter in Response" + e.toString());
            }
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
