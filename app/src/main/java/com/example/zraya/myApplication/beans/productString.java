package com.example.zraya.myApplication.beans;

import android.util.JsonReader;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.StringReader;

/**
 * Created by Rajesh on 29-Jun-16.
 */

/**
 * This Class will contains the Three Object
 */
public class productString extends IJRModelData {
    @SerializedName("order_info")
    private OrderInfo orderObj;
    @SerializedName("product_detail")
//    private OfficeTablePojo productObj;
    private Object productObj;
    @SerializedName("user_detail")
    private UserInfoPojo userObj;

    public OrderInfo getOrderObj() {
        return orderObj;
    }

    public void setOrderObj(OrderInfo orderObj) {
        this.orderObj = orderObj;
    }

//    public JSONObject getProductObj() {
//        return productObj;
//    }
//
//    public void setProductObj(JSONObject productObj) {
//        this.productObj = productObj;
//    }

    public UserInfoPojo getUserObj() {
        return userObj;
    }

    public void setUserObj(UserInfoPojo userObj) {
        this.userObj = userObj;
    }

    public IJRModelData getProductObject() {
        IJRModelData ijrModelData = null;
        if (orderObj.getItemCategoryID().equalsIgnoreCase("0")) {
            Gson gson = new Gson();
            ijrModelData = gson.fromJson(productObj.toString(), OfficeTablePojo.class);

        } else if (orderObj.getItemCategoryID().equalsIgnoreCase("8")) {
            try {
//                GsonBuilder builder = new GsonBuilder();
//                Gson gson = builder.enableComplexMapKeySerialization().create();
//                Gson gson = new Gson();
                byte ptext[] = productObj.toString().trim().getBytes();
                String value = new String(ptext, "UTF-8");
//                String jsonObj = gson.toJson(value);

                Gson gson = new Gson();
                JsonReader reader = new JsonReader(new StringReader(value));
                reader.setLenient(true);
                WorkstationPojo userinfo1;
                userinfo1 = (WorkstationPojo) gson.fromJson(String.valueOf(reader), WorkstationPojo.class);

//                ijrModelData = (WorkstationPojo) gson.fromJson(value,
//                        new TypeToken<WorkstationPojo>() {}.getType());

                System.out.println("JSON COMPLE");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ijrModelData;
    }
}
