package com.example.zraya.myApplication.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by bheemesh on 1/1/16.
 */
public class Utils {

    private static Application application;
    private static Context appContext;

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application context) {
        application = context;
    }

    public static Context getApplicationContext() {
        return appContext;
    }

    public static void setApplicationContext(Context context) {
        appContext = context;
    }

    // Check if a string is empty or null
    public static boolean isEmpty(String str) {
        return !(str != null && !(str.trim()).equals(Constants.EMPTY_STRING));
    }

    /**
     * method to check email and redirecting user to next screen
     */
    public static int getDpFromPixels(int pixel, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pixel / scale);
    }

    public static boolean isEmptyWithoutTrim(String str) {
        if (str != null && (str.length() > 0)) {
            return false;
        }
        return true;
    }

    /**
     * method to check email and redirecting user to next screen
     */
    public static boolean validateEmailAddress(String input) {
        return Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    /**
     * This method convets dp unit to equivalent device specific value in
     * pixels.
     */
    public static int getPixelFromDP(int dp, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static int getDeviceScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDeviceScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static Drawable getDrawable(Context context, int resId) {
        if (Build.VERSION.SDK_INT < 21) {
            return context.getResources().getDrawable(resId);
        } else {
            return context.getDrawable(resId);
        }
    }

    public static boolean isAppInstalled(Activity activity, String packageName) {
        try {
            activity.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (Exception e) {
            // some other exception and still return false
            return false;
        }
    }

    public static void showKeyBoard(Context context, EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        editText.requestFocus();
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(Context context, EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context
                .INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static Bitmap decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);
        return bitmap;
    }

    /**
     * Get the human readable String representation of time
     *
     * @param unix_time
     * @return String format of time eg 13:20
     * @throws NullPointerException
     * @throws IllegalArgumentException
     */
    public String convertFromUnix(String unix_time) throws NullPointerException, IllegalArgumentException {
        java.util.Date dateTime = new java.util.Date(Long.parseLong(unix_time) * 1000);
        System.out.println("Ordered Date is : " + dateTime);

        return "" + dateTime;
    }

    public Bitmap convertURLBitmap(String imgUrl) throws IOException {
        URL url = new URL(imgUrl);
        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        return bmp;
    }


    public static Date getDate(String timeStamp) {
        long dv = Long.valueOf(timeStamp) * 1000;
        Date df = new Date(dv);
        return df;

    }

    public static String getDateFormat(Date aDate) {
        String result = new SimpleDateFormat("dd-MM-yyyy hh:mma").format(aDate);
        return result;
    }

    public static Date getDateAfterSomeDays(Date aDate, int tenDays) {
        Date deliverDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(aDate);
        c.add(Calendar.DATE, tenDays);
        deliverDate = c.getTime();
        return deliverDate;
    }

    public static String getUrlEncode(String aUrl) throws UnsupportedEncodingException {
        String url = null;
        if (!isEmpty(aUrl)) {
            String fullUrl = Constants.baseUrl + "" + aUrl;
            url = fullUrl.replaceAll(" ", "%20");
            url = URLEncoder.encode(url, "utf-8");
            url = URLDecoder.decode(url, "utf-8");
        }
        return url;
    }


    public static String getDateFormatBySeperate(String inputDate, String inputFormat, String
            outputFormat) {
        try {

            Locale appLocale = new Locale(Locale.ENGLISH.getLanguage());
            DateFormat originalFormat = new SimpleDateFormat(inputFormat, appLocale);
            DateFormat targetFormat = new SimpleDateFormat(outputFormat);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getOnlyDate(String inputDate, String inputFormat, String dateFormat) {
        try {

            Locale appLocale = new Locale(Locale.ENGLISH.getLanguage());
            DateFormat originalFormat = new SimpleDateFormat(inputFormat, appLocale);
            DateFormat targetFormat = new SimpleDateFormat(dateFormat);
            Date dateObject = originalFormat.parse(inputDate);
            String formattedDate = targetFormat.format(dateObject);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


}
