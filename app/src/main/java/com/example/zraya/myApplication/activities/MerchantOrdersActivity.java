package com.example.zraya.myApplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.adapters.MarchantOrderListTabAdapter;
import com.example.zraya.myApplication.beans.MerchantDetailsPojo;

/**
 * Created by Raj on 22-Dec-16.
 */

public class MerchantOrdersActivity extends BaseDrawerActivity {
    private ViewPager mViewPager;
    private MarchantOrderListTabAdapter mAdapter;
    UserSessionManager mSession;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_orders);
        mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        mSession = new UserSessionManager(getApplicationContext());
        setupViewPager(mViewPager);
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTextColorResource(R.color.color_white);
    }


    private void setupViewPager(ViewPager aViewPager) {
        Intent intent = getIntent();
        MerchantDetailsPojo merchantDetailsPojo = (MerchantDetailsPojo) intent.getSerializableExtra(Constants.BUNDLE_ITEM_COLLECTION);
        String tabTitles[] = new String[]{Constants.TAB_ON_GOING_ORDERS, Constants.TAB_COMPLETED};
        mAdapter = new MarchantOrderListTabAdapter(getFragmentManager());
        mAdapter.setTabs(tabTitles);
        mAdapter.setUserEmail(merchantDetailsPojo.getmMarchantInfo().getMarchant_username());
        mAdapter.setUserID(merchantDetailsPojo.getmUserID());
        aViewPager.setAdapter(mAdapter);
    }
}
