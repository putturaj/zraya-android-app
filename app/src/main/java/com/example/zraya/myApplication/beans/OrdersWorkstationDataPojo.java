package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rajesh on 29-Jun-16.
 */

/**
 * This Class will contains the Three Object
 */
public class OrdersWorkstationDataPojo implements Serializable {
    @SerializedName("order_info")
    private OrderInfo orderObj;
    @SerializedName("product_detail")
    private WorkstationPojo workstationPojo;
    @SerializedName("user_detail")
    private UserInfoPojo userObj;

    public OrderInfo getOrderObj() {
        return orderObj;
    }

    public void setOrderObj(OrderInfo orderObj) {
        this.orderObj = orderObj;
    }

    public WorkstationPojo getWorkstationPojo() {
        return workstationPojo;
    }

    public void setWorkstationPojo(WorkstationPojo workstationPojo) {
        this.workstationPojo = workstationPojo;
    }

    public UserInfoPojo getUserObj() {
        return userObj;
    }

    public void setUserObj(UserInfoPojo userObj) {
        this.userObj = userObj;
    }
}
