package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.activities.AdminOrderListActivity;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.fragments.AdminOrderListFragment;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class AdminOrderListPresenter implements HttpListener {

    AdminOrderListFragment mAdminOrderListFragment;
    RequestActionType requestActionType;
    ArrayList<OrdersData> orderLists;
    PostObjects postObjects = new PostObjects();
    String[] mStatus;


    public AdminOrderListPresenter(AdminOrderListFragment frag) {
        this.mAdminOrderListFragment = frag;
    }


    public void requestToServer(RequestActionType aRequestActionType, String aUserEmail, String aUserID, String[] duplicatingStatus) {
        this.requestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();
        mStatus = duplicatingStatus;

        String postData = postObjects.createAdminInfoPostBody(aUserEmail, aUserID).toString();

        switch (aRequestActionType) {
            case ADMIN_ORDERS_LIST:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.ADMIN_ORDERS_URL;
                break;
            default:
                return;
        }
        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + requestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content, "UTF-8");
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();
                ArrayList<OrdersData> orderItems = (ArrayList<OrdersData>) gson.fromJson(str,
                        new TypeToken<ArrayList<OrdersData>>() {
                        }.getType());
                quickSort(orderItems, mStatus);
                orderLists = orderItems;
            } catch (Exception e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
        mAdminOrderListFragment.responseOrderListServer(orderLists);
    }

    public void quickSort(ArrayList<OrdersData> orderLists, String[] selectedProductStatus) {

        int pIndex = 0;

        for (int index = 0; index < selectedProductStatus.length; index++) {
            if (pIndex < orderLists.size()) {
                pIndex = partition(orderLists, pIndex, selectedProductStatus[index]);
            } else {
                break;
            }
        }

        for (int index = 0; index < orderLists.size(); index++) {
            System.out.println("TEST :: " + ((OrdersData) orderLists.get(index)).getOrderObj()
                    .getProductStatus());
        }
    }

    private int partition(ArrayList<OrdersData> orderLists, int start, String prodStatus) {
        int pIndex = start;
        try {
            for (int index = 0; index < orderLists.size(); index++) {
                OrdersData ordersData = orderLists.get(index);
                if (ordersData.getOrderObj().getProductStatus().equalsIgnoreCase(prodStatus)) {
                    OrdersData temp = ((OrdersData) orderLists.get(index)).clone();
                    orderLists.set(index, ((OrdersData) orderLists.get(pIndex)).clone());
                    orderLists.set(pIndex, temp);
                    pIndex++;
                }
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return pIndex;
    }


    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
