package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.BaseItem;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.dao.DBItemSQLDao;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.fragments.MarchantOrderListFragment;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class MarchantOrderListFragmentPresenter implements HttpListener {

    MarchantOrderListFragment frag;
    RequestActionType requestActionType;
    ArrayList<OrdersData> baseItems;
    PostObjects postObjects = new PostObjects();

    public MarchantOrderListFragmentPresenter(MarchantOrderListFragment frag) {
        this.frag = frag;
    }

    public void loadPlaces() {
        DBItemSQLDao database = new DBItemSQLDao(Utils.getApplication());
        database.open();
        ArrayList<BaseItem> baseItems = null;
//        baseItems = database.getAllBaseItems();

        for (int i = 0; i < baseItems.size(); i++) {
            if (frag.mPageTabName.equals(Constants.TAB_LOCATION)) {
//                ((BaseItem) baseItems.get(i)).setCardType(NewsCardType.NORMAL);
            } else {
//                ((BaseItem) baseItems.get(i)).setCardType(NewsCardType.WITH_CAT_LOCATION);
            }
        }
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + requestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content, "UTF-8");
                System.out.println("Response : " + str);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();

                ArrayList<OrdersData> orderData = (ArrayList<OrdersData>) gson.fromJson(str,
                        new TypeToken<ArrayList<OrdersData>>() {
                        }.getType());
                baseItems = orderData;
            } catch (Exception e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
        frag.responseServer(baseItems);
    }

    public void requestToServer(RequestActionType requestActionType, String mUserID) {
        this.requestActionType = requestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = postObjects.createUserIdPostBody(mUserID).toString();

        switch (requestActionType) {
            case ORDER_ONGOING:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.MARCHANT_ORDER_ONGING_URL;
                break;
            case ORDER_COMPLETED:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.MARCHANT_ORDER_COMPLETED_URL;
                break;
            default:
                return;
        }


        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
