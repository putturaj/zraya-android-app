package com.example.zraya.myApplication.Utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.activities.MerchantsFullListActivity;
import com.example.zraya.myApplication.activities.UserOrderListActivity;
import com.example.zraya.myApplication.beans.AdminContentDetailsPojo;

import java.util.HashMap;

/**
 * Created by bheemesh on 26/1/16.
 *
 * @author bheemesh
 */
public class NavigationDrawer extends LinearLayout {

    //------ Top menu items -----------
    private TextView userName;
    private ImageView profileImage;
    private Button signIn;
    private DrawerLayout drawerLayout;

    //------ Mid menu items -----------
    private LinearLayout home, profile, mAdminPannel, mWorkshop, mUsers, mOrders;

    public NavigationDrawer(Context context) {
        super(context);
        init();
    }

    public NavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavigationDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.navigation_drawer_layout, null);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams
                .MATCH_PARENT);
        view.setLayoutParams(lp);
        addView(view);
        initProfileUI();
        initSettingMenu();
        setClickListeners();
    }

    public void setNavigationDrawer(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }

    private void initProfileUI() {
        userName = (TextView) findViewById(R.id.tv_user_name);
        profileImage = (ImageView) findViewById(R.id.iv_profile_image);
        signIn = (Button) findViewById(R.id.btn_sign_in_out);
    }

    private void initSettingMenu() {
        home = (LinearLayout) findViewById(R.id.nv_home);
        profile = (LinearLayout) findViewById(R.id.nv_profile);
        mAdminPannel = (LinearLayout) findViewById(R.id.nv_admin_pannel);
        mWorkshop = (LinearLayout) findViewById(R.id.nv_workshop);
        mUsers = (LinearLayout) findViewById(R.id.nv_orders_users);
        mOrders = (LinearLayout) findViewById(R.id.nv_orders);

        setItemImages();
        setTitleUI();

    }

    private void setClickListeners() {
        signIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), SignOnActivity.class);
//                callActivity(intent);
            }
        });

        home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), ProfileActivity.class);
//                callActivity(intent);
            }
        });

        profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), ProfileActivity.class);
//                callActivity(intent);
            }
        });
        mAdminPannel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mWorkshop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MerchantsFullListActivity.class);
                callActivity(intent);
            }
        });

        mUsers.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserOrderListActivity.class);
                callActivity(intent);
            }
        });
    }

    private void callActivity(Intent intent) {
        drawerLayout.closeDrawer(this);
        getContext().startActivity(intent);
    }


    private void setItemImages() {
        ((ImageView) home.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) profile.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) mAdminPannel.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) mWorkshop.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) mUsers.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_user);
        ((ImageView) mOrders.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_user);
    }

    private void setTitleUI() {
        ((TextView) home.findViewById(R.id.item_name)).setText("Home");
        ((TextView) profile.findViewById(R.id.item_name)).setText("Profile");
        ((TextView) mAdminPannel.findViewById(R.id.item_name)).setText("Admin Panel");
        ((TextView) mWorkshop.findViewById(R.id.item_name)).setText("Work shops");
        ((TextView) mUsers.findViewById(R.id.item_name)).setText("Users");
        ((TextView) mOrders.findViewById(R.id.item_name)).setText("Orders");

        ((TextView) home.findViewById(R.id.item_subname)).setText("15");
        ((TextView) profile.findViewById(R.id.item_subname)).setText("24");
        ((TextView) mAdminPannel.findViewById(R.id.item_subname)).setText("");
        ((TextView) mWorkshop.findViewById(R.id.item_subname)).setText("");
        ((TextView) mUsers.findViewById(R.id.item_subname)).setText("0");
        ((TextView) mOrders.findViewById(R.id.item_subname)).setText("0");


        home.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
        profile.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
    }

    public void showAdminContent(AdminContentDetailsPojo adminContentDetailsPojo) {
        mAdminPannel.setVisibility(View.VISIBLE);
        mWorkshop.setVisibility(View.VISIBLE);
        mAdminPannel.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
        mWorkshop.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
        mUsers.findViewById(R.id.item_subname).setVisibility(VISIBLE);
        mOrders.findViewById(R.id.item_subname).setVisibility(VISIBLE);

        ((TextView) mWorkshop.findViewById(R.id.item_subname)).setText(adminContentDetailsPojo.getaWorkStationCount());
    }

    public void displayUserInfoInNavigationDrawer(HashMap<String, String> aUser) {
        if (!aUser.isEmpty()) {
            userName.setText(aUser.get(UserSessionManager.USER_NAME));
            userName.setAllCaps(true);
            signIn.setVisibility(View.GONE);
        }
    }
}
