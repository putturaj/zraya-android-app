package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class ShelvesPojo extends IJRModelData {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("width")
    private String width;

    @SerializedName("height")
    private String height;

    @SerializedName("num_col")
    private String num_col;

    @SerializedName("num_rows")
    private String num_rows;

    @SerializedName("material")
    private String material;

    @SerializedName("color")
    private String color;

    @SerializedName("img_url")
    private String imgUrl;

    @SerializedName("amount")
    private String amount;

    @SerializedName("date_created")
    private String dateCreated;

    @SerializedName("productid")
    private String productid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getNum_col() {
        return num_col;
    }

    public void setNum_col(String num_col) {
        this.num_col = num_col;
    }

    public String getNum_rows() {
        return num_rows;
    }

    public void setNum_rows(String num_rows) {
        this.num_rows = num_rows;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }
}