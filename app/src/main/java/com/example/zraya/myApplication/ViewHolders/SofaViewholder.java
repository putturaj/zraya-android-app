package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.ProductDisplayMainPojo;
import com.example.zraya.myApplication.beans.SofaPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by puttu on 24-Mar-17.
 */

public class SofaViewholder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private View parentView;
    private OnProductItemClickListener onProductItemClickListener;

    ImageView mImageView;
    TextView mName, mCusionmeterial, mLength, mWidth,
            mAssinedWorshop, mOrderStatus, mQuantity, mDeliveryDate;
//    private View container;
    Context mContext;
    UserSessionManager mSession;
    String mIsMerchant;

    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    SofaPojo mSofaPojos = null;
    ProductDisplayMainPojo mProductDisplayMainPojo;

    public SofaViewholder(View itemView, Context aContext) {
        super(itemView);
//        container = itemView;
//        mContext = aContext;
//        mSession = new UserSessionManager(mContext);
//        HashMap<String, String> user = mSession.getUserDetails();
//        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);
//
//        mImageView = (ImageView) container.findViewById(R.id.prodImg);
//        mName = (TextView) container.findViewById(R.id.product_name);
//        mCusionmeterial = (TextView) container.findViewById(R.id.cusion);
//        mLength = (TextView) container.findViewById(R.id.length);
//        mWidth = (TextView) container.findViewById(R.id.width);
//        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
//        mOrderStatus = (TextView) itemView.findViewById(R.id.order_status);
//        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
//        mDeliveryDate = (TextView) itemView.findViewById(R.id.delivery_date);
    }

    public SofaViewholder(View productView, OnProductItemClickListener viewOnBaseItemClickListener) {
        super(productView);
        this.onProductItemClickListener = viewOnBaseItemClickListener;
        this.parentView = productView;
        this.mContext = productView.getContext();

        mImageView = (ImageView) productView.findViewById(R.id.prodImg);
        mName = (TextView) productView.findViewById(R.id.product_name);
        mCusionmeterial = (TextView) productView.findViewById(R.id.cusion);
        mLength = (TextView) productView.findViewById(R.id.length);
        mWidth = (TextView) productView.findViewById(R.id.width);
        mAssinedWorshop = (TextView) productView.findViewById(R.id.assined_marchant);
        mOrderStatus = (TextView) productView.findViewById(R.id.order_status);
        mQuantity = (TextView) productView.findViewById(R.id.quantity);
        mDeliveryDate = (TextView) productView.findViewById(R.id.delivery_date);
    }
    @Override
    public void updateUI(int position, IJRModelData ijrModelData) {
        mOrderData = (OrdersData) ijrModelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mSofaPojos = (SofaPojo) mOrderData.getProductObject();

        Date date = Utils.getDate(orderInfo.getDate());
        mName.setText(mSofaPojos.getName());
        mCusionmeterial.setText(mSofaPojos.getCushion_meterial());
        mLength.setText(mSofaPojos.getLength());
        mWidth.setText(mSofaPojos.getWidth());

        if (null != mSofaPojos.getImgurls()) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mSofaPojos.getImgurls())).into(mImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.DEFAULT_SOFA_IMG_URL).into(mImageView);
        }


        mDeliveryDate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants
                .SEVEN_DAYS)));
        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mOrderStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
            mOrderStatus.setText("Unassigned");
        }


        //Assigning Quantity
        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText('1');
        }


        parentView.setClickable(true);
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mSofaPojos);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }
}
