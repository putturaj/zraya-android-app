package com.example.zraya.myApplication.beans;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

/**
 * Created by Rajesh on 29-Jun-16.
 */

/**
 * This Class will contains the Three Object
 */
public class ParentOrdersData extends IJRModelData implements Cloneable {
    @SerializedName("parentOrder")
    private ParentOrderInfo parentOrderdetails;
    @SerializedName("user_detail")
    private UserInfoPojo userObj;

    public ParentOrdersData clone() throws CloneNotSupportedException {
        return (ParentOrdersData) super.clone();
    }

    public ParentOrderInfo getOrderObj() {
        return parentOrderdetails;
    }

    public void setOrderObj(ParentOrderInfo orderObj) {
        this.parentOrderdetails = orderObj;
    }

    public UserInfoPojo getUserObj() {
        return userObj;
    }

    public void setUserObj(UserInfoPojo userObj) {
        this.userObj = userObj;
    }


}
