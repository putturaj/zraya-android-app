package com.example.zraya.myApplication.dao;

/**
 * Created by Rajesh on 23-Apr-16.
 */
public class DBSQLiteConstants {
    // ------ ORDER RELATED VARIABLES --------------

    public static final String ORDER_ID = "order_id";
    public static final String ORDER_PRODUCT_ID = "product_id";
    public static final String ORDER_USER_ID = "user_id";
    public static final String ORDER_CATEGORY_ID = "category_id";
    public static final String ORDER_STATUS = "status";
    public static final String ORDER_QUANTITY = "quantity";

    //    ------- OFFICE TABLE VARIABLES
    public static final String OFFICE_TABLE_ID = "office_table_id";
    public static final String OFFICE_TABLE_NAME = "office_table_name";
    public static final String OFFICE_TABLE_LENGTH = "office_table_length";
    public static final String OFFICE_TABLE_WIDTH = "office_table_width";
    public static final String OFFICE_TABLE_HEIGHT = "office_table_height";
    public static final String OFFICE_TABLE_LEG_THICK = "office_table_leg_thick";
    public static final String OFFICE_TABLE_DESCRIPTION = "office_table_description";
    public static final String OFFICE_TABLE_TYPE = "office_table_type";
    public static final String OFFICE_TABLE_TOP_MATERIAL = "office_table_top_material";
    public static final String OFFICE_TABLE_BASE_MATERIAL = "office_table_base_material";
    public static final String OFFICE_TABLE_TOP_COLOR = "office_table_top_color";
    public static final String OFFICE_TABLE_BASE_COLOR = "office_table_base_color";
    public static final String OFFICE_TABLE_IMG_URL = "office_table_img_url";
    public static final String OFFICE_TABLE_AMOUNT = "office_table_amount";
    public static final String OFFICE_TABLE_PRODUCT_ID = "product_id";

    //    ------- STATUS DB TABLE VARIABLES
    public static final String STATUS_ID = "status_id";
    public static final String STATUS_ORDER_ID = "status_order_id";
    public static final String STATUS_STATUS = "status_status";
    public static final String STATUS_COMMENT = "status_comment";
    public static final String STATUS_DATE = "status_date";

    //    ------- ORDER STAGES OF PRODUCTS
    public static final String ON_GOING = "on_going";
    public static final String COMPLETED = "completed";
    public static final String CANCELLED = "cancelled";


}
