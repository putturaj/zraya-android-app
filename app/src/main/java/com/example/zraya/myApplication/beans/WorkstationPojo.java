package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Rajesh on 28-Apr-16.
 */
public class WorkstationPojo extends IJRModelData {
    @SerializedName("ID")
    private String ws_id;

    @SerializedName("Name")
    private String ws_name;

    @SerializedName("ImageUrls")
    private String ws_img_url;

    @SerializedName("imageUrls")
    private String ws_imageUrls;

    @SerializedName("TopColor")
    private String ws_topcolor;

    @SerializedName("totalamount")
    private String ws_amount;

    @SerializedName("BaseColor")
    private String ws_basecolor;

    @SerializedName("partitionTable")
    private String ws_partition;

    @SerializedName("productid")
    private String ws_productid;

    @SerializedName("Material")
    private String ws_meterial;

    @SerializedName("BeidingColor")
    private String ws_beidingcolor;

    @SerializedName("TypeOfLegs")
    private String ws_typesoflegs;

    @SerializedName("powerOutletfull")
    private String ws_poweroutletfull;

    @SerializedName("EnclosedPowerBoxFour")
    private String ws_enclosedpowerboxfour;

    @SerializedName("EnclosedPowerBoxSix")
    private String ws_enclosedpowerboxsix;

    @SerializedName("SinglePowerOutlet")
    private String ws_singlepoweroutlet;

    @SerializedName("MetalDrawer")
    private String ws_metaldrawer;

    @SerializedName("KeyboardDrawer")
    private String ws_keyboarddrawer;

    @SerializedName("MonitorStand")
    private String ws_monitorstand;

    @SerializedName("Shape")
    private String ws_shape;

    @SerializedName("NumberOfWorkstation")
    private String ws_numberofworkstation;

    @SerializedName("SizeWorkStation")
    private String ws_sizeworkstation;

    @SerializedName("dateoforder")
    private String orderDate;


    public String getWs_meterial() {
        return ws_meterial;
    }

    public void setWs_meterial(String ws_meterial) {
        this.ws_meterial = ws_meterial;
    }

    public String getWs_beidingcolor() {
        return ws_beidingcolor;
    }

    public void setWs_beidingcolor(String ws_beidingcolor) {
        this.ws_beidingcolor = ws_beidingcolor;
    }

    public String getWs_typesoflegs() {
        return ws_typesoflegs;
    }

    public void setWs_typesoflegs(String ws_typesoflegs) {
        this.ws_typesoflegs = ws_typesoflegs;
    }

    public String getWs_poweroutletfull() {
        return ws_poweroutletfull;
    }

    public void setWs_poweroutletfull(String ws_poweroutletfull) {
        this.ws_poweroutletfull = ws_poweroutletfull;
    }

    public String getWs_enclosedpowerboxfour() {
        return ws_enclosedpowerboxfour;
    }

    public void setWs_enclosedpowerboxfour(String ws_enclosedpowerboxfour) {
        this.ws_enclosedpowerboxfour = ws_enclosedpowerboxfour;
    }

    public String getWs_enclosedpowerboxsix() {
        return ws_enclosedpowerboxsix;
    }

    public void setWs_enclosedpowerboxsix(String ws_enclosedpowerboxsix) {
        this.ws_enclosedpowerboxsix = ws_enclosedpowerboxsix;
    }

    public String getWs_singlepoweroutlet() {
        return ws_singlepoweroutlet;
    }

    public void setWs_singlepoweroutlet(String ws_singlepoweroutlet) {
        this.ws_singlepoweroutlet = ws_singlepoweroutlet;
    }

    public String getWs_metaldrawer() {
        return ws_metaldrawer;
    }

    public void setWs_metaldrawer(String ws_metaldrawer) {
        this.ws_metaldrawer = ws_metaldrawer;
    }

    public String getWs_keyboarddrawer() {
        return ws_keyboarddrawer;
    }

    public void setWs_keyboarddrawer(String ws_keyboarddrawer) {
        this.ws_keyboarddrawer = ws_keyboarddrawer;
    }

    public String getWs_monitorstand() {
        return ws_monitorstand;
    }

    public void setWs_monitorstand(String ws_monitorstand) {
        this.ws_monitorstand = ws_monitorstand;
    }

    public String getWs_shape() {
        return ws_shape;
    }

    public void setWs_shape(String ws_shape) {
        this.ws_shape = ws_shape;
    }

    public String getWs_numberofworkstation() {
        return ws_numberofworkstation;
    }

    public void setWs_numberofworkstation(String ws_numberofworkstation) {
        this.ws_numberofworkstation = ws_numberofworkstation;
    }

    public String getWs_sizeworkstation() {
        return ws_sizeworkstation;
    }

    public void setWs_sizeworkstation(String ws_sizeworkstation) {
        this.ws_sizeworkstation = ws_sizeworkstation;
    }

    public String getWs_id() {
        return ws_id;
    }

    public void setWs_id(String ws_id) {
        this.ws_id = ws_id;
    }

    public String getWs_name() {
        return ws_name;
    }

    public void setWs_name(String ws_name) {
        this.ws_name = ws_name;
    }

    public String getWs_img_url() {
        return ws_img_url;
    }

    public void setWs_img_url(String ws_img_url) {
        this.ws_img_url = ws_img_url;
    }

    public String getWs_topcolor() {
        return ws_topcolor;
    }

    public void setWs_topcolor(String ws_topcolor) {
        this.ws_topcolor = ws_topcolor;
    }

    public String getWs_basecolor() {
        return ws_basecolor;
    }

    public void setWs_basecolor(String ws_basecolor) {
        this.ws_basecolor = ws_basecolor;
    }

    public String getWs_partition() {
        return ws_partition;
    }

    public void setWs_partition(String ws_partition) {
        this.ws_partition = ws_partition;
    }

    public String getWs_amount() {
        return ws_amount;
    }

    public void setWs_amount(String ws_amount) {
        this.ws_amount = ws_amount;
    }

    public String getWs_productid() {
        return ws_productid;
    }

    public void setWs_productid(String ws_productid) {
        this.ws_productid = ws_productid;
    }

    public String getWs_imageUrls() {
        return ws_imageUrls;
    }

    public void setWs_imageUrls(String ws_imageUrls) {
        this.ws_imageUrls = ws_imageUrls;
    }
}
