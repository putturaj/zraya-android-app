package com.example.zraya.myApplication.beans;

/**
 * Created by Raj on 9/26/2016.
 */
public class ProductDisplayMainPojo extends IJRModelData implements Cloneable {
    private String id;
    private String name;
    private String length;
    private String width;
    private String height;
    private String legthick;
    private String discription;
    private String type;
    private String topmaterial;
    private String basemeterial;
    private String topcolor;
    private String basecolor;
    private String imgurl;
    private String amount;
    private String productid;

    private String brandname;
    private String style;
    private String suitable;
    private String modelnumber;
    private String careinstuction;
    private String drawerinclude;
    private String partition;
    private String powercable;
    private String senderspace;
    private String designtype;
    private String date;
    //    Array of Image URls
    private String imageUrls;


    private String meterial;
    private String beidingcolor;
    private String typeoflegs;
    private String poweroutletfull;
    private String eclosedpoweroutletfull;
    private String enclosedpowerboxfour;
    private String enclosedpowerboxsix;
    private String singlepoweroutlet;
    private String metaldrawer;
    private String keyboarddrawer;
    private String monitorstand;
    private String shape;
    private String numberproduct;
    private String size;

    //    Office Chair Details
    private String seatlockinclude;
    private String wheelsinclude;
    private String headsupportinclude;
    private String adjestableheight;
    private String armrestinclude;

    // white board details
    private String thickness;
    private String cornerType;
    private String bgcolor;

    // Sofa Details
    private String productfamily;
    private String cusionhieght;
    private String framecolor;
    private String cusioncolor;
    private String cusionmeterial;

    // pedestal details
    private String numberOfdrawer;
    private String outercolor;
    private String drawercolor;
    // Custom Order Details
    private String deliverdate;

    public String getDeliverdate() {
        return deliverdate;
    }

    public void setDeliverdate(String deliverdate) {
        this.deliverdate = deliverdate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMeterial() {
        return meterial;
    }

    public void setMeterial(String meterial) {
        this.meterial = meterial;
    }

    public String getBeidingcolor() {
        return beidingcolor;
    }

    public void setBeidingcolor(String beidingcolor) {
        this.beidingcolor = beidingcolor;
    }

    public String getTypeoflegs() {
        return typeoflegs;
    }

    public void setTypeoflegs(String typeoflegs) {
        this.typeoflegs = typeoflegs;
    }

    public String getPoweroutletfull() {
        return poweroutletfull;
    }

    public void setPoweroutletfull(String poweroutletfull) {
        this.poweroutletfull = poweroutletfull;
    }

    public String getEclosedpoweroutletfull() {
        return eclosedpoweroutletfull;
    }

    public void setEclosedpoweroutletfull(String eclosedpoweroutletfull) {
        this.eclosedpoweroutletfull = eclosedpoweroutletfull;
    }

    public String getEnclosedpowerboxfour() {
        return enclosedpowerboxfour;
    }

    public void setEnclosedpowerboxfour(String enclosedpowerboxfour) {
        this.enclosedpowerboxfour = enclosedpowerboxfour;
    }

    public String getEnclosedpowerboxsix() {
        return enclosedpowerboxsix;
    }

    public void setEnclosedpowerboxsix(String enclosedpowerboxsix) {
        this.enclosedpowerboxsix = enclosedpowerboxsix;
    }

    public String getSinglepoweroutlet() {
        return singlepoweroutlet;
    }

    public void setSinglepoweroutlet(String singlepoweroutlet) {
        this.singlepoweroutlet = singlepoweroutlet;
    }

    public String getMetaldrawer() {
        return metaldrawer;
    }

    public void setMetaldrawer(String metaldrawer) {
        this.metaldrawer = metaldrawer;
    }

    public String getKeyboarddrawer() {
        return keyboarddrawer;
    }

    public void setKeyboarddrawer(String keyboarddrawer) {
        this.keyboarddrawer = keyboarddrawer;
    }

    public String getMonitorstand() {
        return monitorstand;
    }

    public void setMonitorstand(String monitorstand) {
        this.monitorstand = monitorstand;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getNumberproduct() {
        return numberproduct;
    }

    public void setNumberproduct(String numberproduct) {
        this.numberproduct = numberproduct;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getLegthick() {
        return legthick;
    }

    public void setLegthick(String legthick) {
        this.legthick = legthick;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTopmaterial() {
        return topmaterial;
    }

    public void setTopmaterial(String topmaterial) {
        this.topmaterial = topmaterial;
    }

    public String getBasemeterial() {
        return basemeterial;
    }

    public void setBasemeterial(String basemeterial) {
        this.basemeterial = basemeterial;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public String getBasecolor() {
        return basecolor;
    }

    public void setBasecolor(String basecolor) {
        this.basecolor = basecolor;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getSuitable() {
        return suitable;
    }

    public void setSuitable(String suitable) {
        this.suitable = suitable;
    }

    public String getModelnumber() {
        return modelnumber;
    }

    public void setModelnumber(String modelnumber) {
        this.modelnumber = modelnumber;
    }

    public String getCareinstuction() {
        return careinstuction;
    }

    public void setCareinstuction(String careinstuction) {
        this.careinstuction = careinstuction;
    }

    public String getDrawerinclude() {
        return drawerinclude;
    }

    public void setDrawerinclude(String drawerinclude) {
        this.drawerinclude = drawerinclude;
    }

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public String getPowercable() {
        return powercable;
    }

    public void setPowercable(String powercable) {
        this.powercable = powercable;
    }

    public String getSenderspace() {
        return senderspace;
    }

    public void setSenderspace(String senderspace) {
        this.senderspace = senderspace;
    }

    public String getDesigntype() {
        return designtype;
    }

    public void setDesigntype(String designtype) {
        this.designtype = designtype;
    }

    public String getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String imageUrls) {
        this.imageUrls = imageUrls;
    }


    //    Office Chair getter and setter methods
    public String getSeatlockinclude() {
        return seatlockinclude;
    }

    public void setSeatlockinclude(String seatlockinclude) {
        this.seatlockinclude = seatlockinclude;
    }

    public String getWheelsinclude() {
        return wheelsinclude;
    }

    public void setWheelsinclude(String wheelsinclude) {
        this.wheelsinclude = wheelsinclude;
    }

    public String getHeadsupportinclude() {
        return headsupportinclude;
    }

    public void setHeadsupportinclude(String headsupportinclude) {
        this.headsupportinclude = headsupportinclude;
    }

    public String getAdjestableheight() {
        return adjestableheight;
    }

    public void setAdjestableheight(String adjestableheight) {
        this.adjestableheight = adjestableheight;
    }

    public String getArmrestinclude() {
        return armrestinclude;
    }

    public void setArmrestinclude(String armrestinclude) {
        this.armrestinclude = armrestinclude;
    }

    //    white Board getter and setter methods
    public String getThickness() {
        return thickness;
    }

    public void setThickness(String thickness) {
        this.thickness = thickness;
    }

    public String getCornerType() {
        return cornerType;
    }

    public void setCornerType(String cornerType) {
        this.cornerType = cornerType;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }


    //    Sofa getter and setter methods
    public String getProductfamily() {
        return productfamily;
    }

    public void setProductfamily(String productfamily) {
        this.productfamily = productfamily;
    }

    public String getCusionhieght() {
        return cusionhieght;
    }

    public void setCusionhieght(String cusionhieght) {
        this.cusionhieght = cusionhieght;
    }

    public String getFramecolor() {
        return framecolor;
    }

    public void setFramecolor(String framecolor) {
        this.framecolor = framecolor;
    }

    public String getCusioncolor() {
        return cusioncolor;
    }

    public void setCusioncolor(String cusioncolor) {
        this.cusioncolor = cusioncolor;
    }

    public String getCusionmeterial() {
        return cusionmeterial;
    }

    public void setCusionmeterial(String cusionmeterial) {
        this.cusionmeterial = cusionmeterial;
    }
//    getter and setter methods for pedestal

    public String getNumberOfdrawer() {
        return numberOfdrawer;
    }

    public void setNumberOfdrawer(String numberOfdrawer) {
        this.numberOfdrawer = numberOfdrawer;
    }

    public String getOutercolor() {
        return outercolor;
    }

    public void setOutercolor(String outercolor) {
        this.outercolor = outercolor;
    }

    public String getDrawercolor() {
        return drawercolor;
    }

    public void setDrawercolor(String drawercolor) {
        this.drawercolor = drawercolor;
    }
}
