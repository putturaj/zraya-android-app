package com.example.zraya.myApplication.presenters;

import android.content.Context;
import android.content.Intent;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.activities.AdminOrderListActivity;
import com.example.zraya.myApplication.activities.MarchantListActivity;
import com.example.zraya.myApplication.activities.OrderListActivity;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import static com.example.zraya.myApplication.Utils.Utils.getApplicationContext;

/**
 * Created by puttu on 03-Apr-17.
 */

public class AssignOrderMerchantPresenter implements HttpListener {
    RequestActionType mRequestActionType;
    PostObjects mPostObjects = new PostObjects();
    public String mMerchantID;
    Context mContext;
    UserSessionManager session;

    public AssignOrderMerchantPresenter(Context aContext) {
        mContext = aContext;
        session = new UserSessionManager(mContext);
    }

    public void requestToServer(RequestActionType aRequestActionType, OrdersData aOrderDatapojo, String marchant_id) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        mMerchantID = marchant_id;
        OrderInfo orderInfo = aOrderDatapojo.getOrderObj();
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createAssigningOrderMerchantBody(orderInfo, mMerchantID).toString();

        url = Constants.ORDER_ASSIGNING_URL;

        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content);
                System.out.println("Response is : " + str);
                responseAction();
            } catch (JsonSyntaxException e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
    }

    private void responseAction() {
        Intent intent = null;
        HashMap<String, String> user = session.getUserDetails();
        String userType = user.get(UserSessionManager.USER_IS_ADMIN);

        switch (userType) {
            case Constants.USER:
                intent = new Intent(mContext, OrderListActivity.class);
                break;
            case Constants.MARCHANT:
                intent = new Intent(mContext, MarchantListActivity.class);
                break;
            case Constants.ADMIN:
                intent = new Intent(mContext, AdminOrderListActivity.class);
                break;
            default:
                System.out.println("ProductStatusChangeActivity : responseFromServer : Can't find the User type");
                break;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }


    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
