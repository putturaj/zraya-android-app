package com.example.zraya.myApplication.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.fragments.OrderListFragment;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public class OrderListTabAdapter extends FragmentStatePagerAdapter {
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    private String tabTitles[] = new String[]{Constants.TAB_ON_GOING_ORDERS, Constants.TAB_COMPLETED, Constants.TAB_CANCELLED};
    private UserInfoPojo userDetails;
    private String userEmail;
    private String userID;

    public OrderListTabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setTabs(String[] tabsList) {
        this.tabTitles = tabsList;
    }

    @Override
    public Fragment getItem(int index) {
        return OrderListFragment.newInstance(tabTitles[index], userEmail, userID);
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return tabTitles.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public void setUserDetails(UserInfoPojo userDetails) {
        this.userDetails = userDetails;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}

