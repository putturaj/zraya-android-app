package com.example.zraya.myApplication.presenters;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.activities.ProductStatusChangeActivity;
import com.example.zraya.myApplication.beans.OrderStatusPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Jun-16.
 */
public class OrderStatusUpdatePresenter implements HttpListener {
    RequestActionType mRequestActionType;
    ProductStatusChangeActivity proStatusUpdate;
    PostObjects mPostObjects = new PostObjects();

    public OrderStatusUpdatePresenter(ProductStatusChangeActivity aProductStatusChangeActivity) {
        proStatusUpdate = aProductStatusChangeActivity;
    }


    public void requestToServer(RequestActionType aRequestActionType, OrderStatusPojo aOrderStatusPojo) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createOrderStatusBody(aOrderStatusPojo).toString();

        url = Constants.ORDER_UPDATE_STATUS_URL;

        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {

                String str = new String(httpRequest.content);
                System.out.println("Response is : " + str);

                proStatusUpdate.responseFromServer();
            } catch (JsonSyntaxException e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
