package com.example.zraya.myApplication.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.fragments.AdminOrderListFragment;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public class AdminOrderListTabAdapter extends FragmentStatePagerAdapter {
    SparseArray<AdminOrderListFragment> registeredFragments = new SparseArray<AdminOrderListFragment>();

    private String tabTitles[] = new String[]{Constants.TAB_USERS, Constants.TAB_ORDERS};
    private UserInfoPojo mUserDetails;
    private String userEmail;
    private String userID;

    public AdminOrderListTabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setTabs(String[] tabsList) {
        this.tabTitles = tabsList;
    }

    @Override
    public Fragment getItem(int index) {
        return AdminOrderListFragment.newInstance(tabTitles[index], userEmail, userID);
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return tabTitles.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, (AdminOrderListFragment) fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public AdminOrderListFragment getFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public void setmUserDetails(UserInfoPojo mUserDetails) {
        this.mUserDetails = mUserDetails;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}

