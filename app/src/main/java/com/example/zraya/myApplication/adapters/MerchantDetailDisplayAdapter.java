package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.zraya.R;
import com.example.zraya.myApplication.ViewHolders.MerchantViewholder;
import com.example.zraya.myApplication.beans.MerchantDetailsPojo;
import com.example.zraya.myApplication.beans.OrdersData;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class MerchantDetailDisplayAdapter extends ArrayAdapter<MerchantDetailsPojo> {

    private Context mContext;
    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    public String userID;
    public OrdersData mOrdersData;
    public String isAssignOrder;

    /*************
     * CustomAdapter Constructor
     *****************/
    public MerchantDetailDisplayAdapter(Context context, ArrayList<MerchantDetailsPojo> aMerchantPojos, String userId, OrdersData aOrderdData, String aAssignOrder) {
        super(context, 0, aMerchantPojos);
        data = aMerchantPojos;
        userID = userId;
        mOrdersData = aOrderdData;
        isAssignOrder = aAssignOrder;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        MerchantDetailsPojo mercahntDetailsPojo;
        mercahntDetailsPojo = (MerchantDetailsPojo) data.get(position);
        MerchantViewholder merchantViewHolder;
        itemView = inflater.inflate(R.layout.activity_merchant_detail, null);
        merchantViewHolder = new MerchantViewholder(itemView, mContext);
        merchantViewHolder.updateUI(position, mercahntDetailsPojo, mOrdersData);
        return itemView;
    }
}
