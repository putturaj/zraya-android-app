package com.example.zraya.myApplication.dao;

import com.example.zraya.myApplication.beans.OrderStatusPojo;

import java.util.ArrayList;

/**
 * Created by Rajesh on 18-May-16.
 */
public interface DBStatusDao {


    void open();

    void close();


    void updateStatus(String newsVersion, ArrayList<OrderStatusPojo> statusObj);

}
