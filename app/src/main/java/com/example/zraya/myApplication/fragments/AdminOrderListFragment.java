package com.example.zraya.myApplication.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.AlertDialogManager;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.adapters.AdminOrderListAdapterNew;
import com.example.zraya.myApplication.adapters.AdminOrderListDisplayAdapter;
import com.example.zraya.myApplication.adapters.AdminUserListDisplayAdapter;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.ParentOrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.listeners.OnDrawerItemClickListener;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.presenters.AdminOrderListPresenter;
import com.example.zraya.myApplication.presenters.AdminUserListPresenter;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Rajesh on 26-Apr-16.
 */
public class AdminOrderListFragment extends Fragment implements View.OnClickListener,
        OnDrawerItemClickListener, OnProductItemClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String USER_EMAIL = "abc@gmail.com";
    public static final String USER_ID = "USER_ID";
    private String mPageTabName;
    private String mUserEmail;
    private String mUserID;
    private RecyclerView mRecyclerView;
    private boolean isVisible;
    private TextView mFragmentTitle;
//    public ListView mListView;


    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayouts;
    private ImageView error_image;
    private TextView error_description, error_action_refresh;

    public AdminOrderListAdapterNew mOrderListDisplayAdapter;
    public AdminUserListDisplayAdapter mAdminUserListAdapter;
    private AdminOrderListPresenter mAdminOrderListPresenter;
    private AdminUserListPresenter mAdminUserListPresenter;
    public ArrayList<OrdersData> mCustomListViewValuesArr = new ArrayList<OrdersData>();
    public ArrayList<UserInfoPojo> mUserListValueArr = new ArrayList<UserInfoPojo>();
    AlertDialogManager mAlertDialog;

    public static AdminOrderListFragment newInstance(String aPageTitle, String aUserEmail, String aUserID) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, aPageTitle);
        args.putString(USER_EMAIL, aUserEmail);
        args.putString(USER_ID, aUserID);
        AdminOrderListFragment fragment = new AdminOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            mPageTabName = getArguments().getString(ARG_PAGE);
            mUserEmail = getArguments().getString(USER_EMAIL);
            mUserID = getArguments().getString(USER_ID);
        }
//        loadOrdersItems();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        String strtext = getArguments().getString(Constants.USER_ID);
        View view = inflater.inflate(R.layout.frag_home, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        progressErrorContainer = (RelativeLayout) view.findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayouts = (RelativeLayout) view.findViewById(R.id.rv_error_layout);
        error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        error_description = (TextView) view.findViewById(R.id.tv_error_description);
        error_action_refresh = (TextView) view.findViewById(R.id.tv_error_action_text);
        error_action_refresh.setOnClickListener(this);
        progressErrorContainer.setVisibility(View.VISIBLE);
        mAdminOrderListPresenter = new AdminOrderListPresenter(this);
        mAdminUserListPresenter = new AdminUserListPresenter(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()
                .getBaseContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

//        if (mPageTabName.equals(Constants.TAB_USERS)) {
//            mListView = (ListView) view.findViewById(R.id.list);
//            mAdminUserListPresenter.requestToServer(RequestActionType.USER_LIST, mUserEmail, mUserID);
//        } else if (mPageTabName.equals(Constants.TAB_ORDERS)) {
//            mListView = (ListView) view.findViewById(R.id.list);
            mAdminOrderListPresenter.requestToServer(RequestActionType.ADMIN_ORDERS_LIST,
                    mUserEmail, mUserID, Constants.status);
//        }
        return view;
    }

    public void responseUserListServer(final ArrayList<UserInfoPojo> aUserInfoPojos) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // your stuff to update the UI
//                if (aUserInfoPojos != null && aUserInfoPojos.size() > 0) {
//                    progressBar.setVisibility(View.GONE);
//                    errorLayouts.setVisibility(View.GONE);
//                    mUserListValueArr = aUserInfoPojos;
//                    mAdminUserListAdapter = new AdminUserListDisplayAdapter(getActivity(), mUserListValueArr, mUserID);
//                    mListView.setAdapter(mAdminUserListAdapter);
//                } else {
//                    progressBar.setVisibility(View.GONE);
//                    errorLayouts.setVisibility(View.VISIBLE);
//                    if (mPageTabName.equals(Constants.TAB_USERS)) {
//                        error_description.setText("No user Found");
//                    } else if (mPageTabName.equals(Constants.TAB_ORDERS)) {
//                        error_description.setText("No orders ");
//                    }
//                }
            }
        });
    }

    public void responseOrderListServer(final ArrayList<OrdersData> aOrderListPojo) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (aOrderListPojo != null && aOrderListPojo.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.GONE);
                    mCustomListViewValuesArr = aOrderListPojo;
                    mOrderListDisplayAdapter = new AdminOrderListAdapterNew(getActivity(),
                            mCustomListViewValuesArr, mUserID, AdminOrderListFragment.this);
                    mRecyclerView.setAdapter(mOrderListDisplayAdapter);
                } else {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.VISIBLE);
                    if (mPageTabName.equals(Constants.TAB_USERS)) {
                        error_description.setText("No user Found");
                    } else if (mPageTabName.equals(Constants.TAB_ORDERS)) {
                        error_description.setText("No orders ");
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == error_action_refresh) {
            if (mPageTabName.equals(Constants.TAB_USERS)) {
                mAdminUserListPresenter.requestToServer(RequestActionType.USER_LIST, mUserEmail, mUserID);
            } else if (mPageTabName.equals(Constants.TAB_ORDERS)) {
                mAdminOrderListPresenter.requestToServer(RequestActionType.ADMIN_ORDERS_LIST,
                        mUserEmail, mUserID, Constants.status);
            }
        }
    }

    public void rightStatusClick(String[] duplicatingStatusv, HashMap<String, String> user) {

        mAdminOrderListPresenter.quickSort(mCustomListViewValuesArr, duplicatingStatusv);
        if (mOrderListDisplayAdapter != null) {
            mOrderListDisplayAdapter.setOrderData(mCustomListViewValuesArr);
        }
    }


    @Override
    public void onItemClick(View view, String ordersData) {

    }

    @Override
    public void onItemClick(View view, ParentOrdersData placeObj) {

    }
}