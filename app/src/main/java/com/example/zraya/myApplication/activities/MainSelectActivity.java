package com.example.zraya.myApplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.TokenPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.dao.DBItemSQLDao;
import com.example.zraya.myApplication.dao.DBStatusSQLDao;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.MainActivityLoginPresenter;

import java.util.ArrayList;


/**
 * Created by Rajesh on 12-Apr-16.
 */

public class MainSelectActivity extends BaseDrawerActivity implements View.OnClickListener {

    public static String name = "";
    public static String email;

    private Button mShopZraya, mAdmin, mUser, mWorkshop;
    protected TextView title;
    protected ImageView icon;

    String aUserEmail, aUserPwd;
    Context context = this;
    UserSessionManager session;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_users);
        showNavigationDrawer(true);
        session = new UserSessionManager(getApplicationContext());
        initViews();
    }


    private void initViews() {
        title = (TextView) findViewById(R.id.title);
        icon = (ImageView) findViewById(R.id.icon);
        mShopZraya = (Button) findViewById(R.id.shopping);
        mAdmin = (Button) findViewById(R.id.admin);
        mUser = (Button) findViewById(R.id.user);
        mWorkshop = (Button) findViewById(R.id.workstation);
        mWorkshop.setOnClickListener(this);
        mAdmin.setOnClickListener(this);
        mUser.setOnClickListener(this);
        mShopZraya.setOnClickListener(this);

    }

    public void onClick(View v) {
        Intent intent = null;
        if (v == mWorkshop) {
            intent = new Intent(MainSelectActivity.this, ShopHomeZraya.class);
        } else if (v == mAdmin) {
            intent = new Intent(MainSelectActivity.this, LoginActivity.class);
        } else if (v == mUser) {
            intent = new Intent(MainSelectActivity.this, LoginActivity.class);
        } else if (v == mShopZraya) {
            intent = new Intent(MainSelectActivity.this, LoginActivity.class);
        }

        startActivity(intent);
    }


    public void loginUser(String aEmail, String aPwd) {
        if (!(Utils.isEmpty(aEmail) && Utils.isEmpty(aPwd))) {
            MainActivityLoginPresenter mainActivityLoginPresenter = new MainActivityLoginPresenter(context);
            UserInfoPojo aUserInfoPojo = new UserInfoPojo();
            TokenPojo tokenPojo = new TokenPojo();
            aUserEmail = aEmail;
            aUserPwd = aPwd;
            aUserInfoPojo.setUser_email(aUserEmail);
            aUserInfoPojo.setUser_pwd(aUserPwd);
            tokenPojo.setToken(session.getToken());
            aUserInfoPojo.setTokenpojo(tokenPojo);
            mainActivityLoginPresenter.requestToServer(RequestActionType.LOGIN, aUserInfoPojo);
        } else {
            Toast.makeText(MainSelectActivity.this, "Email And Password fields is Empty", Toast.LENGTH_SHORT).show();
        }
    }
}
