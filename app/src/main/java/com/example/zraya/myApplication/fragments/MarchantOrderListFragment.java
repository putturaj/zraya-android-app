package com.example.zraya.myApplication.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.adapters.MarchantOrderListDisplayAdapter;
import com.example.zraya.myApplication.adapters.OrderListAdapter;
import com.example.zraya.myApplication.adapters.WorkstationOrderListDisplayAdapter;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.OrdersWorkstationDataPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.MarchantOrderListFragmentPresenter;

import org.apache.poi.hssf.model.ConvertAnchor;

import java.util.ArrayList;


/**
 * Created by Rajesh on 26-Apr-16.
 */
public class MarchantOrderListFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String USER_EMAIL = "abc@gmail.com";
    public static final String USER_ID = "USER_ID";
    private OrderListAdapter mOrderListAdapter;
    public OrderInfo mOrderInfo;
    public String mPageTabName;
    public String userEmail;
    public String userID;
    private RecyclerView recyclerView;
    private boolean isVisible;
    private TextView mFragmentTitle;
    public ListView mListView;

    public MarchantOrderListDisplayAdapter mMarchantOrderListDisplayAdapter;
    public WorkstationOrderListDisplayAdapter mWorkstationOrderListDisplayAdapter;
    private MarchantOrderListFragmentPresenter mMarchantOrderListFragmentPresenter;
    public ArrayList<OrdersData> mCustomListOrderData = new ArrayList<OrdersData>();
    public ArrayList<OrdersWorkstationDataPojo> mOrdersWorkstationorderData = new ArrayList<OrdersWorkstationDataPojo>();

    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayouts;
    private ImageView error_image;
    private TextView error_description, error_action_refresh;


    public static MarchantOrderListFragment newInstance(String aPageTitle, String aUserEmail, String aUserID) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, aPageTitle);
        args.putString(USER_EMAIL, aUserEmail);
        args.putString(USER_ID, aUserID);
        MarchantOrderListFragment fragment = new MarchantOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            mPageTabName = getArguments().getString(ARG_PAGE);
            userEmail = getArguments().getString(USER_EMAIL);
            userID = getArguments().getString(USER_ID);
        }
//        loadOrdersItems();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        String strtext = getArguments().getString(Constants.USER_ID);
        System.out.println("FRAGMENT onCreateView USER ID : " + strtext);


        View view = inflater.inflate(R.layout.frag_home, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        mMarchantOrderListFragmentPresenter = new MarchantOrderListFragmentPresenter(this);

        progressErrorContainer = (RelativeLayout) view.findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayouts = (RelativeLayout) view.findViewById(R.id.rv_error_layout);
        error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        error_description = (TextView) view.findViewById(R.id.tv_error_description);
        error_action_refresh = (TextView) view.findViewById(R.id.tv_error_action_text);
        error_action_refresh.setOnClickListener(this);
        progressErrorContainer.setVisibility(View.VISIBLE);

        if (mPageTabName.equals(Constants.TAB_ON_GOING_ORDERS)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mMarchantOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_ONGOING, userID);
        } else if (mPageTabName.equals(Constants.TAB_COMPLETED)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mMarchantOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_COMPLETED, userID);
        }
        return view;
    }


    public void responseServer(final ArrayList<OrdersData> aOrderData) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // your stuff to update the UI
                if (aOrderData != null && aOrderData.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.GONE);
                    mCustomListOrderData = aOrderData;
                    mMarchantOrderListDisplayAdapter = new MarchantOrderListDisplayAdapter(getActivity(), mCustomListOrderData, userID);
                    mListView.setAdapter(mMarchantOrderListDisplayAdapter);
                } else {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.VISIBLE);
                    if (mPageTabName.equals(Constants.TAB_ON_GOING_ORDERS)) {
                        error_description.setText("No order assigned to you");
                    } else if (mPageTabName.equals(Constants.TAB_COMPLETED)) {
                        error_description.setText("No completed order from your side");
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == error_action_refresh) {
            mMarchantOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_ONGOING, userID);
        }
    }
}