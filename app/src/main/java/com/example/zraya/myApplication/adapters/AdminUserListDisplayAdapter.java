package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.activities.UserDetailsActivity;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class AdminUserListDisplayAdapter extends ArrayAdapter<UserInfoPojo> {
    private Context mContext;
    /***********
     * Declare Used Variables
     *********/

    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    public String userID;
    UserInfoPojo userInfoPojo = null;

    /*************
     * CustomAdapter Constructor
     *****************/
    public AdminUserListDisplayAdapter(Context aContext, ArrayList<UserInfoPojo> aUserinfopojo, String aUserId) {
        super(aContext, 0, aUserinfopojo);
        data = aUserinfopojo;
        userID = aUserId;
        inflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = aContext;
    }

    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public static class ViewHolder {
        public TextView userName;
        public TextView userPh;
        public TextView userEmail;
        public TextView userNoOrders;
    }

    /******
     * Depends upon data size called for each row , Create each ListView row
     *****/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        ViewHolder holder;
        userInfoPojo = null;
        userInfoPojo = (UserInfoPojo) data.get(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = itemView = inflater.inflate(R.layout.user_list_content, null);
        }

        // Lookup view for data population
        holder = new ViewHolder();
        holder.userName = (TextView) itemView.findViewById(R.id.user_name);
        holder.userPh = (TextView) itemView.findViewById(R.id.user_phNum);
        holder.userEmail = (TextView) itemView.findViewById(R.id.user_email);
        holder.userNoOrders = (TextView) itemView.findViewById(R.id.user_no_orders);
        if (data.size() <= 0) {
            holder.userName.setText("No Data");
        } else {
            /***** Get each Model object from Arraylist ********/

            /************  Set Model values in Holder elements ***********/
            holder.userName.setText(userInfoPojo.getUser_name());
            holder.userPh.setText(userInfoPojo.getUser_phNo());
            holder.userEmail.setText(userInfoPojo.getUser_email());
            holder.userNoOrders.setText(userInfoPojo.getUser_orders_count());
        }
        itemView.setClickable(true);
        itemView.setOnClickListener(new OnItemClickListener(position, userInfoPojo));
        return itemView;
    }


    /*********
     * Called when Item click in ListView
     ************/
    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;
        private UserInfoPojo userInfo;

        OnItemClickListener(int position, UserInfoPojo mUserInfo) {
            mPosition = position;
            userInfo = mUserInfo;
        }

        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent(mContext, UserDetailsActivity.class);
            intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, userInfo);
            intent.putExtra(Constants.USER_ID, userInfo.getUser_id());
            mContext.startActivity(intent);
        }
    }
}
