package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.CustomRequestPojo;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by puttu on 24-Mar-17.
 */

public class CustomRequestViewHolder {
    private View container;
    UserSessionManager mSession;
    String mIsMerchant;

    ImageView mImageView;
    TextView mName, mDod, mDeliveryDate, mAssinedWorshop, mQuantity, mStatus;

    CustomRequestPojo mCustomPojo = null;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public CustomRequestViewHolder(View itemView, Context mContext) {
        container = itemView;
        this.mContext = mContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mImageView = (ImageView) container.findViewById(R.id.prodImg);
        mName = (TextView) container.findViewById(R.id.custo_upload_name);
        mDod = (TextView) container.findViewById(R.id.dod);

        mDeliveryDate = (TextView) container.findViewById(R.id.delivery_date);
        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
        mStatus = (TextView) itemView.findViewById(R.id.order_status);
    }

    public void updateUI(int position, IJRModelData ijrModelData) {
        mOrderData = (OrdersData) ijrModelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mCustomPojo = (CustomRequestPojo) mOrderData.getProductObject();

        Date date = Utils.getDate(orderInfo.getDate());
        mName.setText(mCustomPojo.getName());
        mDod.setText(mCustomPojo.getDod());

        if (null != mCustomPojo.getFloorplan()) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mCustomPojo.getFloorplan())).into(mImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.zrayaLogo).into(mImageView);
        }
        mDeliveryDate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants
                .SEVEN_DAYS)));

        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
            mStatus.setText("Unassigned");
        }

        //Setting Quantity

        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText('1');
        }


        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mCustomPojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }
}
