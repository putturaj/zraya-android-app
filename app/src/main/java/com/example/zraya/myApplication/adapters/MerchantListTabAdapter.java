package com.example.zraya.myApplication.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.fragments.MerchantListFragment;

/**
 * Created by Rajesh on 19-dec-2016
 */
public class MerchantListTabAdapter extends FragmentStatePagerAdapter {
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    private String tabTitles[] = new String[]{Constants.MERCHANT_LIST};
    private UserInfoPojo userDetails;
    private String userEmail;
    private String userID;
    private OrdersData mOrderData;
    private String mAssignOrder;

    public MerchantListTabAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setTabs(String[] tabsList) {
        this.tabTitles = tabsList;
    }

    @Override
    public Fragment getItem(int index) {
        return MerchantListFragment.newInstance(tabTitles[index], userEmail, userID, mOrderData, mAssignOrder);
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return tabTitles.length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public void setUserDetails(UserInfoPojo userDetails) {
        this.userDetails = userDetails;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setOrderData(OrdersData orderData) {
        this.mOrderData = orderData;
    }

    public void setAssignOrder(String assignOrder) {
        this.mAssignOrder = assignOrder;
    }
}

