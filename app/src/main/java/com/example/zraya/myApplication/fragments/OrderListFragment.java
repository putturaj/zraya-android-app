package com.example.zraya.myApplication.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.AlertDialogManager;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.adapters.OrderListAdapter;
import com.example.zraya.myApplication.adapters.OrderListDisplayAdapter;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.OrderListFragmentPresenter;

import java.util.ArrayList;


/**
 * Created by Rajesh on 26-Apr-16.
 */
public class OrderListFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String USER_EMAIL = "abc@gmail.com";
    public static final String USER_ID = "USER_ID";
    private OrderListAdapter mOrderListAdapter;
    public OrderInfo mOrderInfoPojo;
    public String pageTabName;
    public String userEmail;
    public String userID;
    private RecyclerView mRecyclerView;
    private boolean isVisible;
    private TextView mFragmentTitle;
    public ListView mListView;

    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayouts;
    private ImageView error_image;
    private TextView error_description, error_action_refresh;

    public OrderListDisplayAdapter mOrderListDisplayAdapter;
    private OrderListFragmentPresenter mOrderListFragmentPresenter;
    public ArrayList<OrdersData> mCustomListOrderData = new ArrayList<OrdersData>();
    AlertDialogManager mAlertDialog;

    public static OrderListFragment newInstance(String aPageTitle, String aUserEmail, String aUserID) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, aPageTitle);
        args.putString(USER_EMAIL, aUserEmail);
        args.putString(USER_ID, aUserID);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            pageTabName = getArguments().getString(ARG_PAGE);
            userEmail = getArguments().getString(USER_EMAIL);
            userID = getArguments().getString(USER_ID);
        }
//        loadOrdersItems();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        String strtext = getArguments().getString(Constants.USER_ID);

        View view = inflater.inflate(R.layout.frag_home, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        progressErrorContainer = (RelativeLayout) view.findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayouts = (RelativeLayout) view.findViewById(R.id.rv_error_layout);
        error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        error_description = (TextView) view.findViewById(R.id.tv_error_description);
        error_action_refresh = (TextView) view.findViewById(R.id.tv_error_action_text);
        error_action_refresh.setOnClickListener(this);
        progressErrorContainer.setVisibility(View.VISIBLE);
        mOrderListFragmentPresenter = new OrderListFragmentPresenter(this);

        if (pageTabName.equals(Constants.TAB_ON_GOING_ORDERS)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_ONGOING, userID);
        } else if (pageTabName.equals(Constants.TAB_COMPLETED)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_COMPLETED, userID);
        } else if (pageTabName.equals(Constants.TAB_CANCELLED)) {
            mListView = (ListView) view.findViewById(R.id.list);
            mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_CANCELLED, userID);
        }
        return view;
    }

    public void responseServer(final ArrayList<OrdersData> aOrderData) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // your stuff to update the UI
                if (aOrderData != null && aOrderData.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    mCustomListOrderData = aOrderData;
                    mOrderListDisplayAdapter = new OrderListDisplayAdapter(getActivity(), mCustomListOrderData, userID);
                    mListView.setAdapter(mOrderListDisplayAdapter);
                } else {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.VISIBLE);
                    if (pageTabName.equals(Constants.TAB_ON_GOING_ORDERS)) {
                        error_description.setText("No on-going orders found");
                    } else if (pageTabName.equals(Constants.TAB_COMPLETED)) {
                        error_description.setText("No completed orders found");
                    } else if (pageTabName.equals(Constants.TAB_CANCELLED)) {
                        error_description.setText("No canceled orders found");
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == error_action_refresh) {
            if (pageTabName.equals(Constants.TAB_ON_GOING_ORDERS)) {
                mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_ONGOING, userID);
            } else if (pageTabName.equals(Constants.TAB_COMPLETED)) {
                mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_COMPLETED, userID);
            } else if (pageTabName.equals(Constants.TAB_CANCELLED)) {
                mOrderListFragmentPresenter.requestToServer(RequestActionType.ORDER_CANCELLED, userID);
            }
        }
    }
}