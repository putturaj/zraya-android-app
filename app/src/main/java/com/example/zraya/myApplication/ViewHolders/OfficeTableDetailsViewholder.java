package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.activities.ProductStatusChangeActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.squareup.picasso.Picasso;

/**
 * Created by Raj on 9/23/2016.
 */
public class OfficeTableDetailsViewholder {
    public TextView mExtraDetails, mItemName, mItemdesc, mItemTopmat, mItemBasemat, mItemAmount, mItemTopcolor, mItemBasecolor,
            mItemLength, mItemWidth, mItemHeight, mItemLegthick, ordered, assign, purchase, cutting, assemble, polish, qa, deliver, completed, cancle;
    public HorizontalScrollView mHorizontalScrollStatus;
    public Button mStatusChange, retry;
    public ImageView mHeaderImage;
    private View container;
    public RelativeLayout mProgressContent;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    OfficeTablePojo mOfficeTablePojo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;
    public LinearLayout mOrderContent, mNoInternet;
    StringBuffer mExtraDetailBuffer = new StringBuffer();

    public OfficeTableDetailsViewholder(View itemView, Context aContext) {
        mContext = aContext;
        mHorizontalScrollStatus = (HorizontalScrollView) itemView.findViewById(R.id.horizontal_scroll_id);
        retry = (Button) itemView.findViewById(R.id.retry);
        mStatusChange = (Button) itemView.findViewById(R.id.status_change);
        mHeaderImage = (ImageView) itemView.findViewById(R.id.htab_header);
        mItemName = (TextView) itemView.findViewById(R.id.item_name);
        mItemdesc = (TextView) itemView.findViewById(R.id.item_desc);
        mItemWidth = (TextView) itemView.findViewById(R.id.item_width);
        mItemTopmat = (TextView) itemView.findViewById(R.id.item_top_mat);
        mItemBasemat = (TextView) itemView.findViewById(R.id.item_base_mat);
        mItemTopcolor = (TextView) itemView.findViewById(R.id.item_top_color);
        mItemBasecolor = (TextView) itemView.findViewById(R.id.item_base_color);
        mItemLength = (TextView) itemView.findViewById(R.id.item_length);
        mItemLegthick = (TextView) itemView.findViewById(R.id.item_leg_thick);
        mItemHeight = (TextView) itemView.findViewById(R.id.item_height);
        mItemAmount = (TextView) itemView.findViewById(R.id.item_amount);
//        mExtraDetails = (TextView) itemView.findViewById(R.id.extra_Details);
        mProgressContent = (RelativeLayout) itemView.findViewById(R.id.progress_container);
        mOrderContent = (LinearLayout) itemView.findViewById(R.id.content_container);
        mNoInternet = (LinearLayout) itemView.findViewById(R.id.no_internet);
        // Status of the Orders
        ordered = (TextView) itemView.findViewById(R.id.ordered);
        assign = (TextView) itemView.findViewById(R.id.assigned);
        purchase = (TextView) itemView.findViewById(R.id.purchase);
        cutting = (TextView) itemView.findViewById(R.id.cutting);
        assemble = (TextView) itemView.findViewById(R.id.assemble);
        polish = (TextView) itemView.findViewById(R.id.polish);
        qa = (TextView) itemView.findViewById(R.id.qa);
        deliver = (TextView) itemView.findViewById(R.id.deliver);
        completed = (TextView) itemView.findViewById(R.id.completed);
        cancle = (TextView) itemView.findViewById(R.id.cancelled);
    }

    public void updateUI(IJRModelData modelData) {
        mOrderData = (OrdersData) modelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mOfficeTablePojo = (OfficeTablePojo) mOrderData.getProductObject();
        mProgressContent.setVisibility(View.GONE);
        mOrderContent.setVisibility(View.VISIBLE);

        mItemName.setText(mOfficeTablePojo.getTableName());
        mItemdesc.setText(mOfficeTablePojo.getTableDesc());
        mItemWidth.setText(mOfficeTablePojo.getTableWidth());
        mItemLength.setText(mOfficeTablePojo.getTableLength());
        mItemLegthick.setText(mOfficeTablePojo.getTableLegThick());
        mItemHeight.setText(mOfficeTablePojo.getTableHeight());
        mItemTopmat.setText(mOfficeTablePojo.getTableTopMat());
        mItemBasemat.setText(mOfficeTablePojo.getTableBaseMat());
        mItemBasecolor.setText(mOfficeTablePojo.getTableBaseColor());
        mItemTopcolor.setText(mOfficeTablePojo.getTableTopColor());
        mItemAmount.setText(mOfficeTablePojo.getTableAmount());

        // Image Loading through URL
        if (null != mOfficeTablePojo.getTableImgUrl()) {
            Picasso.with(mContext).load(Constants.baseUrl + "" + mOfficeTablePojo.getTableImgUrl()).into(mHeaderImage);
        } else {
            Picasso.with(mContext).load(Constants.zrayaLogo).into(mHeaderImage);
        }


        if (null != mOfficeTablePojo.getTableType()) {
            mExtraDetailBuffer.append("Type : " + mOfficeTablePojo.getTableType() + "  ");
        }
        if (null != mOfficeTablePojo.getBrandName()) {
            mExtraDetailBuffer.append("Brand Name : " + mOfficeTablePojo.getTableType() + "  ");
        }
        if (null != mOfficeTablePojo.getTableStyle()) {
            mExtraDetailBuffer.append("Table Style : " + mOfficeTablePojo.getTableStyle() + "  ");
        }
        if (null != mOfficeTablePojo.getSuitableFor()) {
            mExtraDetailBuffer.append("Suitable : " + mOfficeTablePojo.getSuitableFor() + "  ");
        }
        if (null != mOfficeTablePojo.getCareInstruction()) {
            mExtraDetailBuffer.append("Care Instruction : " + mOfficeTablePojo.getCareInstruction() + "  ");
        }
        if (null != mOfficeTablePojo.getDrawerInclude()) {
            mExtraDetailBuffer.append("Drawer Included : " + mOfficeTablePojo.getDrawerInclude() + "  ");
        }
//        mExtraDetails.setText(mExtraDetailBuffer);

        mStatusChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductStatusChangeActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mOfficeTablePojo);
                intent.putExtra(Constants.BUNDLE_ORDER_ID, orderInfo.getOrderId());
                intent.putExtra(Constants.BUNDLE_STATUS, orderInfo);
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }
}
