package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class PedestalPojo extends IJRModelData {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("breadth")
    private String breadth;

    @SerializedName("wheelinclude")
    private String wheelinclude;

    @SerializedName("meterial")
    private String meterial;

    @SerializedName("width")
    private String width;

    @SerializedName("NoDrawer")
    private String NoDrawer;

    @SerializedName("amount")
    private String amount;

    @SerializedName("height")
    private String height;

    @SerializedName("productid")
    private String productid;

    @SerializedName("description")
    private String description;

    @SerializedName("imgurls")
    private String imgurls;

    @SerializedName("family")
    private String family;

    @SerializedName("drawerinclude")
    private String drawerinclude;

    @SerializedName("outercolor")
    private String outercolor;

    @SerializedName("drawercolor")
    private String drawercolor;

//    @SerializedName("imageUrls")
//    private String imageUrls;
//
//    public String getImageUrls() {
//        return imageUrls;
//    }
//
//    public void setImageUrls(String imageUrls) {
//        this.imageUrls = imageUrls;
//    }

    public String getBreadth() {
        return breadth;
    }

    public void setBreadth(String breadth) {
        this.breadth = breadth;
    }

    public String getWheelinclude() {
        return wheelinclude;
    }

    public void setWheelinclude(String wheelinclude) {
        this.wheelinclude = wheelinclude;
    }

    public String getMeterial() {
        return meterial;
    }

    public void setMeterial(String meterial) {
        this.meterial = meterial;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getNoDrawer() {
        return NoDrawer;
    }

    public void setNoDrawer(String NoDrawer) {
        this.NoDrawer = NoDrawer;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgurls() {
        return imgurls;
    }

    public void setImgurls(String imgurls) {
        this.imgurls = imgurls;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDrawerinclude() {
        return drawerinclude;
    }

    public void setDrawerinclude(String drawerinclude) {
        this.drawerinclude = drawerinclude;
    }

    public String getOutercolor() {
        return outercolor;
    }

    public void setOutercolor(String outercolor) {
        this.outercolor = outercolor;
    }

    public String getDrawercolor() {
        return drawercolor;
    }

    public void setDrawercolor(String drawercolor) {
        this.drawercolor = drawercolor;
    }

    @Override
    public String toString() {
        return "ClassPojo [breadth = " + breadth + ", wheelinclude = " + wheelinclude + ", meterial = " + meterial + ", width = " + width + ", NoDrawer = " + NoDrawer + ", amount = " + amount + ", id = " + id + ", height = " + height + ", productid = " + productid + ", description = " + description + ", imgurls = " + imgurls + ", family = " + family + ", name = " + name + ", drawerinclude = " + drawerinclude + ", outercolor = " + outercolor + ", drawercolor = " + drawercolor + "]";
    }
}
