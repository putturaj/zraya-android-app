package com.example.zraya.myApplication.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.AlertDialogManager;
import com.example.zraya.myApplication.Utils.CommonUtilities;
import com.example.zraya.myApplication.adapters.MerchantDetailDisplayAdapter;
import com.example.zraya.myApplication.beans.MerchantDetailsPojo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.MerchantListFragmentPresenter;

import java.util.ArrayList;


/**
 * Created by Rajesh on 26-Apr-16.
 */
public class MerchantListFragment extends Fragment implements View.OnClickListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String USER_EMAIL = "abc@gmail.com";
    public static final String USER_ID = "USER_ID";
    public static final String ORDERS_DATA = null;
    public static final String ASSIGN_ORDER = "assign";
    public OrdersData mOrdersDataPojo;
    public String pageTabName;
    public String userEmail;
    public String userID;
    private RecyclerView mRecyclerView;
    public String isAssignOrder;
    boolean isVisible;
    private TextView mFragmentTitle;
    public ListView mListView;

    public MerchantDetailDisplayAdapter mMerchantDetailDisplayAdapter;
    private MerchantListFragmentPresenter mWorkstationListFragmentPresenter;
    public ArrayList<MerchantDetailsPojo> mMerchantDetialsPojo = new ArrayList<MerchantDetailsPojo>();
    AlertDialogManager mAlertDialog;


    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayouts;
    private ImageView error_image;
    private TextView error_description, error_action_refresh;

    public static MerchantListFragment newInstance(String aPageTitle, String aUserEmail, String aUserID, OrdersData mOrderData, String aAssignOrder) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, aPageTitle);
        args.putString(USER_EMAIL, aUserEmail);
        args.putString(USER_ID, aUserID);
        args.putString(ASSIGN_ORDER, aAssignOrder);
        args.putSerializable(ORDERS_DATA, mOrderData);
        MerchantListFragment fragment = new MerchantListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            userEmail = getArguments().getString(USER_EMAIL);
            userID = getArguments().getString(USER_ID);
            mOrdersDataPojo = (OrdersData) getArguments().getSerializable(ORDERS_DATA);
            isAssignOrder = (String) getArguments().getString(ASSIGN_ORDER);
        }
//        loadOrdersItems();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_home, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_container);
        mWorkstationListFragmentPresenter = new MerchantListFragmentPresenter(this);
        mListView = (ListView) view.findViewById(R.id.list);

        progressErrorContainer = (RelativeLayout) view.findViewById(R.id.rv_error_container);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayouts = (RelativeLayout) view.findViewById(R.id.rv_error_layout);
        error_image = (ImageView) view.findViewById(R.id.iv_error_image);
        error_description = (TextView) view.findViewById(R.id.tv_error_description);
        error_action_refresh = (TextView) view.findViewById(R.id.tv_error_action_text);
        error_action_refresh.setOnClickListener(this);
        progressErrorContainer.setVisibility(View.VISIBLE);
        mWorkstationListFragmentPresenter.requestToServer(RequestActionType.WORKSTATION_LIST, userID);
        return view;
    }

    public void responseWorstationListPresenter(final ArrayList<MerchantDetailsPojo> aOrderData) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                mMerchantDetialsPojo = aOrderData;
                if (aOrderData != null && aOrderData.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    ArrayList<MerchantDetailsPojo> mer = new ArrayList<MerchantDetailsPojo>();
                    mMerchantDetialsPojo = CommonUtilities.sortArrayList(aOrderData);
                    mMerchantDetailDisplayAdapter = new MerchantDetailDisplayAdapter(getActivity(), mMerchantDetialsPojo, userID, mOrdersDataPojo, isAssignOrder);
                    mListView.setAdapter(mMerchantDetailDisplayAdapter);
                } else {
                    progressBar.setVisibility(View.GONE);
                    errorLayouts.setVisibility(View.VISIBLE);
                    error_description.setText("No merchant found");
//                    Toast.makeText(getActivity(), "No merchant found", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        if (v == error_action_refresh) {
            mWorkstationListFragmentPresenter.requestToServer(RequestActionType.WORKSTATION_LIST, userID);
        }
    }
}