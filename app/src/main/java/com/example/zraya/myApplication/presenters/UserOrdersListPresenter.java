package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.beans.ParentOrdersData;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.fragments.UserOrdersListFragment;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class UserOrdersListPresenter implements HttpListener {

    UserOrdersListFragment frag;
    RequestActionType mRequestActionType;
    ArrayList<ParentOrdersData> baseItems;
    PostObjects mPostObjects = new PostObjects();

    public UserOrdersListPresenter(UserOrdersListFragment frag) {
        this.frag = frag;
    }


    public void requestToServer(RequestActionType aRequestActionType, String aUserEmail, String aUserID) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = mPostObjects.createAdminInfoPostBody(aUserEmail, aUserID).toString();

        switch (aRequestActionType) {
            case USER_LIST:
                headers.put("Authorization", "Token 69c883f304207cb5adb50e5529e749badce247d5");
                headers.put("Content-Type", "application/json");
                HttpEngine.getInstance().addHeaderValues(headers);
                url = Constants.USER_ORDERS_LIST_URL;
                break;
            default:
                return;
        }

        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str = new String(httpRequest.content);
                System.out.println("Response is : " + str);
                InputStream is = new ByteArrayInputStream(httpRequest.content);
                Reader reader = new InputStreamReader(is);

                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();

                ArrayList<ParentOrdersData> orderItems = (ArrayList<ParentOrdersData>) gson
                        .fromJson(reader, new TypeToken<ArrayList<ParentOrdersData>>() {
                        }.getType());
                baseItems = orderItems;
            } catch (JsonSyntaxException e) {
                System.out.println("JsonSyntaxException : in Response" + e.toString());
            }
        }
        frag.responseUserOrdersFrag(baseItems);
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
