package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.beans.WorkstationPojo;
import com.example.zraya.myApplication.listeners.OnProductItemClickListener;
import com.example.zraya.myApplication.listeners.UpdatableViewHolder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Raj on 9/21/2016.
 */
public class WorkstationViewholder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private View parentView;
    private OnProductItemClickListener onProductItemClickListener;


    public ImageView mWorksatitionImage;
    public TextView mWorksationName;
    public TextView mAssinedWorshop;
    public TextView mOrderStatus;
    public TextView mQuantity;
    public TextView mDeliveryDate;

    private View container;
    UserSessionManager mSession;
    String mIsMerchant;

    WorkstationPojo mWorkstationPojo = null;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public WorkstationViewholder(View itemView, Context aContext) {
        super(itemView);
        container = itemView;
        mContext = aContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mWorksationName = (TextView) itemView.findViewById(R.id.ws_table_name);
        mWorksatitionImage = (ImageView) itemView.findViewById(R.id.prodImg);
        mAssinedWorshop = (TextView) itemView.findViewById(R.id.assined_marchant);
        mOrderStatus = (TextView) itemView.findViewById(R.id.order_status);
        mQuantity = (TextView) itemView.findViewById(R.id.quantity);
        mDeliveryDate = (TextView) itemView.findViewById(R.id.delivery_date);
    }

    public WorkstationViewholder(View productView, OnProductItemClickListener viewOnBaseItemClickListener) {
        super(productView);
        this.onProductItemClickListener = viewOnBaseItemClickListener;
        this.parentView = productView;
        this.mContext = productView.getContext();

        mWorksationName = (TextView) productView.findViewById(R.id.ws_table_name);
        mWorksatitionImage = (ImageView) productView.findViewById(R.id.prodImg);
        mAssinedWorshop = (TextView) productView.findViewById(R.id.assined_marchant);
        mOrderStatus = (TextView) productView.findViewById(R.id.order_status);
        mQuantity = (TextView) productView.findViewById(R.id.quantity);
        mDeliveryDate = (TextView) productView.findViewById(R.id.delivery_date);

    }

    @Override
    public void updateUI(int position, IJRModelData modelData) {
        StringBuffer sb = new StringBuffer();
        StringBuffer colorfeilds = new StringBuffer();
        mOrderData = (OrdersData) modelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mWorkstationPojo = (WorkstationPojo) mOrderData.getProductObject();

        Date date = Utils.getDate(orderInfo.getDate());

        mWorksationName.setText(mWorkstationPojo.getWs_name());
        sb.append(mWorkstationPojo.getWs_meterial() + " top ");
        sb.append(" " + mWorkstationPojo.getWs_typesoflegs() + " Leg types");
        colorfeilds.append(mWorkstationPojo.getWs_topcolor() + " top, " + mWorkstationPojo.getWs_basecolor() + " base, "
                + mWorkstationPojo.getWs_beidingcolor() + " Beiding Color");


        // Worksthop Name Displaying if it Assined to any Workshop
        if (null != orderInfo.getMarchantInfo()) {
            mAssinedWorshop.setText(orderInfo.getMarchantInfo().getMarchant_name() + " Workshop");
            mOrderStatus.setText(orderInfo.getProductStatus());
        } else {
            mAssinedWorshop.setText(" No");
            mOrderStatus.setText("Unassigned");
        }


        if (!Utils.isEmpty(orderInfo.getQuantity())) {
            mQuantity.setText(orderInfo.getQuantity());
        } else {
            mQuantity.setText('1');
        }


        if (null != mWorkstationPojo.getWs_img_url()) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mWorkstationPojo.getWs_img_url())).into(mWorksatitionImage);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.DEFAULT_WORKSTATION_IMG_URL).into(mWorksatitionImage);
        }

        mDeliveryDate.setText(Utils.getDateFormat(Utils.getDateAfterSomeDays(date, Constants
                .SEVEN_DAYS)));


        parentView.setClickable(true);
        parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mWorkstationPojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }

}
