package com.example.zraya.myApplication.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.ForgotPasswordActivityPresenter;

/**
 * Created by Raj on 10/3/2016.
 */
public class ForgotPasswordActivity extends BaseDrawerActivity implements View.OnClickListener {
    // Internet detector
    ConnectionDetector cd;
    TextView mForgotPasswordEmail;
    Button mSubmit;

    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        cd = new ConnectionDetector(getApplicationContext());
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("");
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBackImage = (ImageView) findViewById(R.id.back_button);
        setSupportActionBar(toolbar);
        mForgotPasswordEmail = (TextView) findViewById(R.id.forgot_password_email);
        mSubmit = (Button) findViewById(R.id.login);
        mSubmit.setOnClickListener(this);
        mToolBackImage.setOnClickListener(this);
        mToolbarTitle.setText("Forgot Password");
    }

    public void onClick(View v) {
        String email;
        if (v == mSubmit) {
            email = mForgotPasswordEmail.getText().toString();
            if (!Utils.isEmpty(email)) {
                sendforgotPasswordEmail(email);
            } else {
                Toast.makeText(ForgotPasswordActivity.this, "Please Enter the email address !! ", Toast.LENGTH_SHORT).show();
            }
        } else if (v == mToolBackImage) {
            finish();
        }
    }


    private void sendforgotPasswordEmail(String email) {
        ForgotPasswordActivityPresenter forgotPasswordActivityPresenter = new ForgotPasswordActivityPresenter(this);
        forgotPasswordActivityPresenter.requestToServer(RequestActionType.FORGOT_PASSWORD, email);
    }


    public void responseLoginPresenter(String aUserinfo) {
//        System.out.println("Forgot Password response");
//        Toast.makeText(ForgotPasswordActivity.this, "Check Your mail for Reset Password !! ", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
//        startActivity(intent);
        finish();
    }
}