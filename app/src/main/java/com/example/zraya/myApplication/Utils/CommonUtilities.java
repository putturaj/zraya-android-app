package com.example.zraya.myApplication.Utils;

import android.content.Context;
import android.content.Intent;

import com.example.zraya.myApplication.beans.MerchantDetailsPojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Raj on 8/1/2016.
 */
public class CommonUtilities {
    // give your server registration url here
    public static final String SERVER_URL = "http://test.zraya.com/AppRequest/register.php";

    // Google project id
    public static String SENDER_ID = "1469975447444";

    /**
     * Tag used on log messages.
     */
    public static final String TAG = "AndroidHive GCM";

    public static final String DISPLAY_MESSAGE_ACTION = "com.example.zraya.myApplication.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p/>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static ArrayList<MerchantDetailsPojo> sortArrayList(ArrayList<MerchantDetailsPojo> aMerchantDetialspojo) {
        ArrayList<MerchantDetailsPojo> merchantDetailsPojos = new ArrayList<MerchantDetailsPojo>();
        if (aMerchantDetialspojo.size() > 0 && aMerchantDetialspojo.size() > 0) {
            merchantDetailsPojos = aMerchantDetialspojo;
            Collections.sort(merchantDetailsPojos, new Comparator<MerchantDetailsPojo>() {
                public int compare(MerchantDetailsPojo obj1, MerchantDetailsPojo obj2) {
                    // TODO Auto-generated method stub

                    if (Integer.parseInt(obj1.getmOrderCount()) == Integer.parseInt(obj2.getmOrderCount())) {
                        return 0;
                    } else if (Integer.parseInt(obj1.getmOrderCount()) < Integer.parseInt(obj2.getmOrderCount())) {
                        return 1;
                    } else {
                        return -1;
                    }

//                    return obj1.getmOrderCount().compareToIgnoreCase(obj2.getmOrderCount());
                }
            });
        }
        return merchantDetailsPojos;
    }
}
