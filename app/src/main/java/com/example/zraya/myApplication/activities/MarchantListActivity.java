package com.example.zraya.myApplication.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.adapters.MarchantOrderListTabAdapter;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.HashMap;


/**
 * Created by Rajesh on 22-Apr-16.
 */
public class MarchantListActivity extends BaseDrawerActivity {
    private ViewPager mViewPager;
    private MarchantOrderListTabAdapter mAdapter;
    UserSessionManager mSession;
    private Toolbar toolbar;
    private TextView mToolbarTitle;
    private ImageView mToolBackImage;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        showNavigationDrawer(true);
//        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        toolbar.setTitle("");
//        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
//        mToolBackImage = (ImageView) findViewById(R.id.back_button);
//        setSupportActionBar(toolbar);


        mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        mSession = new UserSessionManager(getApplicationContext());
        setupViewPager(mViewPager);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTextColorResource(R.color.color_white);
        setTitle("Orders Lists");
    }

    private void setupViewPager(ViewPager aViewPager) {
//        Bundle extras = getIntent().getExtras();
//        String userEmail = extras.getString(Constants.USER_EMAIL);
//        String userID = extras.getString(Constants.USER_ID);


//        mToolbarTitle.setText("Orders List");
        // get user data from session
        HashMap<String, String> user = mSession.getUserDetails();
        String userEmail = user.get(UserSessionManager.USER_EMAIL);
        String userID = user.get(UserSessionManager.USER_ID);

        String tabTitles[] = new String[]{Constants.TAB_ON_GOING_ORDERS, Constants.TAB_COMPLETED};
        mAdapter = new MarchantOrderListTabAdapter(getFragmentManager());
        mAdapter.setTabs(tabTitles);
        UserInfoPojo userInfo = new UserInfoPojo();
        mAdapter.setUserDetails(userInfo);
        mAdapter.setUserEmail(userEmail);
        mAdapter.setUserID(userID);
        aViewPager.setAdapter(mAdapter);
        setOnclickListener();
        showUserDetails(user);
    }


    @Override
    public void onBackPressed() {
        doubleClickExit();
    }

    private void setOnclickListener() {
//        mToolBackImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doubleClickExit();
//            }
//        });
    }

    public void doubleClickExit() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
