package com.example.zraya.myApplication.beans;

import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.Utils;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public class ParentOrderInfo extends IJRModelData implements Serializable, Cloneable {

    @SerializedName("poID")
    private String parentOrderID;

    @SerializedName("uid")
    private String userId;

    @SerializedName("date")
    private String date;

    @SerializedName("orderCount")
    private String orderCount;

    @SerializedName("orderposition")
    private String orderPosition;

    @SerializedName("products")
    private ArrayList<OrderProductInfoPojo> ordersData;

    public ArrayList<OrderProductInfoPojo> getOrdersData() {
        return ordersData;
    }

    public void setOrdersData(ArrayList<OrderProductInfoPojo> ordersData) {
        this.ordersData = ordersData;
    }

    public String getParentOrderID() {
        return parentOrderID;
    }

    public void setParentOrderID(String parentOrderID) {
        this.parentOrderID = parentOrderID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getOrderPosition() {
        return orderPosition;
    }

    public void setOrderPosition(String orderPosition) {
        this.orderPosition = orderPosition;
    }

    public ParentOrderInfo clone() throws CloneNotSupportedException {
        return (ParentOrderInfo) super.clone();
    }

}
