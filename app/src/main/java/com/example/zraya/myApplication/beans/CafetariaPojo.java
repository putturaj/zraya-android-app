package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class CafetariaPojo extends IJRModelData {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("design_id")
    private String design_id;

    @SerializedName("date_created")
    private String date_created;

    @SerializedName("sub_name")
    private String sub_name;

    @SerializedName("shape")
    private String shape;

    @SerializedName("need_instalation")
    private String need_instalation;

    @SerializedName("top_meterial")
    private String top_meterial;

    @SerializedName("category")
    private String category;

    @SerializedName("price")
    private String price;

    @SerializedName("productid")
    private String productid;

    @SerializedName("base_color")
    private String base_color;

    @SerializedName("leg_id")
    private String leg_id;

    @SerializedName("base_meterial")
    private String base_meterial;

    @SerializedName("care")
    private String care;

    @SerializedName("length")
    private String length;

    @SerializedName("top_color")
    private String top_color;

    @SerializedName("brand")
    private String brand;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("design_name")
    private String design_name;

    @SerializedName("imgurl")
    private String imgurl;

    public String getDesign_id() {
        return design_id;
    }

    public void setDesign_id(String design_id) {
        this.design_id = design_id;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getSub_name() {
        return sub_name;
    }

    public void setSub_name(String sub_name) {
        this.sub_name = sub_name;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getNeed_instalation() {
        return need_instalation;
    }

    public void setNeed_instalation(String need_instalation) {
        this.need_instalation = need_instalation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTop_meterial() {
        return top_meterial;
    }

    public void setTop_meterial(String top_meterial) {
        this.top_meterial = top_meterial;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getBase_color() {
        return base_color;
    }

    public void setBase_color(String base_color) {
        this.base_color = base_color;
    }

    public String getLeg_id() {
        return leg_id;
    }

    public void setLeg_id(String leg_id) {
        this.leg_id = leg_id;
    }

    public String getBase_meterial() {
        return base_meterial;
    }

    public void setBase_meterial(String base_meterial) {
        this.base_meterial = base_meterial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCare() {
        return care;
    }

    public void setCare(String care) {
        this.care = care;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getTop_color() {
        return top_color;
    }

    public void setTop_color(String top_color) {
        this.top_color = top_color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDesign_name() {
        return design_name;
    }

    public void setDesign_name(String design_name) {
        this.design_name = design_name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    @Override
    public String toString() {
        return "ClassPojo [design_id = " + design_id + ", date_created = " + date_created + ", sub_name = " + sub_name + ", shape = " + shape + ", need_instalation = " + need_instalation + ", id = " + id + ", top_meterial = " + top_meterial + ", category = " + category + ", price = " + price + ", productid = " + productid + ", base_color = " + base_color + ", leg_id = " + leg_id + ", base_meterial = " + base_meterial + ", name = " + name + ", care = " + care + ", length = " + length + ", top_color = " + top_color + ", brand = " + brand + ", quantity = " + quantity + ", design_name = " + design_name + ", imgurl = " + imgurl + "]";
    }
}