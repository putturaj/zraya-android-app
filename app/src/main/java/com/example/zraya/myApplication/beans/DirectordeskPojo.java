package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DirectordeskPojo extends IJRModelData {
    @SerializedName("img_url")
    private String img_url;

    @SerializedName("subname")
    private String subname;

    @SerializedName("width")
    private String width;

    @SerializedName("date_created")
    private String date_created;

    @SerializedName("amount")
    private String amount;

    @SerializedName("id")
    private String id;

    @SerializedName("top_meterial")
    private String top_meterial;


    @SerializedName("height")
    private String height;

    @SerializedName("productid")
    private String productid;

    @SerializedName("base_color")
    private String base_color;

    @SerializedName("producct_sub_name")
    private String producct_sub_name;

    @SerializedName("name")
    private String name;

    @SerializedName("base_meterial")
    private String base_meterial;

    @SerializedName("length")
    private String length;

    @SerializedName("top_color")
    private String top_color;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTop_meterial() {
        return top_meterial;
    }

    public void setTop_meterial(String top_meterial) {
        this.top_meterial = top_meterial;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getBase_color() {
        return base_color;
    }

    public void setBase_color(String base_color) {
        this.base_color = base_color;
    }

    public String getProducct_sub_name() {
        return producct_sub_name;
    }

    public void setProducct_sub_name(String producct_sub_name) {
        this.producct_sub_name = producct_sub_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBase_meterial() {
        return base_meterial;
    }

    public void setBase_meterial(String base_meterial) {
        this.base_meterial = base_meterial;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getTop_color() {
        return top_color;
    }

    public void setTop_color(String top_color) {
        this.top_color = top_color;
    }

    @Override
    public String toString() {
        return "ClassPojo [img_url = " + img_url + ", subname = " + subname + ", width = " + width + ", date_created = " + date_created + ", amount = " + amount + ", id = " + id + ", top_meterial = " + top_meterial + ",height = " + height + ", productid = " + productid + ", base_color = " + base_color + ", producct_sub_name = " + producct_sub_name + ", name = " + name + ", base_meterial = " + base_meterial + ", length = " + length + ", top_color = " + top_color + "]";
    }
}
