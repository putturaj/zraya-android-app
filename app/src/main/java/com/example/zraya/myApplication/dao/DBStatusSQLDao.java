package com.example.zraya.myApplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.zraya.myApplication.beans.OrderStatusPojo;

import java.util.ArrayList;

/**
 * Created by Rajesh on 18-May-16.
 */
public class DBStatusSQLDao implements DBStatusDao {

    private DBSQLiteHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;

    public DBStatusSQLDao(Context context) {
        this.context = context;
        dbHelper = DBSQLiteHelper.getInstance(context);
    }

    @Override
    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        dbHelper.close();
    }


    public void updateStatus(String itemVersion, ArrayList<OrderStatusPojo> aStatusPojo) {
        OrderStatusPojo orderStatuspojo;
        for (int i = 0; i < aStatusPojo.size(); i++) {
            orderStatuspojo = aStatusPojo.get(i);
            updateOrderStatusTable(orderStatuspojo);
        }
    }


    public void updateOrderStatusTable(OrderStatusPojo mStatusInfo) {
        ContentValues values = setOrderStatusInfo(mStatusInfo);
        try {
            if (database.update(dbHelper.ORDER_STATUS, values, DBSQLiteConstants.STATUS_ID + " = '" +
                    mStatusInfo.getStatusID() + "'", null) == 0) {
                long rowId = database.insert(dbHelper.ORDER_STATUS, null, values);
                System.out.println("ZRAYA :STATUS ROW ID :" + rowId);
            }
        } catch (Exception e) {
            System.out.println("ZRAYA :DBBaseItemInfoSQLDao : updateBaseItem :" + e.getMessage());
            e.printStackTrace();
        }
    }

    private ContentValues setOrderStatusInfo(OrderStatusPojo mStatusInfoPojo) {
        ContentValues values = new ContentValues();
        values.put(DBSQLiteConstants.ORDER_ID, mStatusInfoPojo.getStatusID());
        values.put(DBSQLiteConstants.ORDER_PRODUCT_ID, mStatusInfoPojo.getStatusOrderID());
        values.put(DBSQLiteConstants.ORDER_USER_ID, mStatusInfoPojo.getStatusStatus());
        values.put(DBSQLiteConstants.ORDER_CATEGORY_ID, mStatusInfoPojo.getStatusComment());
        values.put(DBSQLiteConstants.ORDER_STATUS, mStatusInfoPojo.getStatusDate());
        return values;
    }
}
