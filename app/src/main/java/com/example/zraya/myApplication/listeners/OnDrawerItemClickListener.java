package com.example.zraya.myApplication.listeners;

import android.view.View;

import com.example.zraya.myApplication.beans.OrdersData;


/**
 * Created by bheemesh on 3/nov/2018
 */
public interface OnDrawerItemClickListener {
    void onItemClick(View view, String ordersData);
}
