package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public class OrderInfo implements Serializable, Entity<String>, Cloneable {

    @SerializedName("orderId")
    private String OrderId;

    @SerializedName("productid")
    private String productID;

    @SerializedName("userid")
    private String userId;

    @SerializedName("productstatus")
    private String productStatus;

    @SerializedName("category")
    private String itemCategoryID;

    @SerializedName("orderDate")
    private String date;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("orderposition")
    private String orderPosition;

    @SerializedName("isOrderAssigned")
    private String isOrderAssigned;

    @SerializedName("marchantInfo")
    private MarchantInfoPojo marchantInfo;


    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderPosition() {
        return orderPosition;
    }

    public void setOrderPosition(String orderPosition) {
        this.orderPosition = orderPosition;
    }

    @Override
    public String getOrderId() {
        return OrderId;
    }

    @Override
    public void setOrderId(String id) {
        this.OrderId = id;
    }


    public void setProductID(String mProductID) {
        this.productID = mProductID;
    }

    public String getProductID() {
        return productID;
    }

    public void setItemCategoryID(String mItemCategoryID) {
        this.itemCategoryID = mItemCategoryID;
    }

    public String getItemCategoryID() {
        return itemCategoryID;
    }

    public void setUserId(String mUserId) {
        this.userId = mUserId;
    }

    public String getUserId() {
        return userId;
    }

    public String getIsOrderAssigned() {
        return isOrderAssigned;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setIsOrderAssigned(String isOrderAssigned) {
        this.isOrderAssigned = isOrderAssigned;
    }

    public MarchantInfoPojo getMarchantInfo() {
        return marchantInfo;
    }

    public void setMarchantInfo(MarchantInfoPojo marchantInfo) {
        this.marchantInfo = marchantInfo;
    }

    public OrderInfo clone() throws CloneNotSupportedException {
        return (OrderInfo)super.clone();
    }
}
