package com.example.zraya.myApplication.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.activities.LoginActivity;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONObject;

public class CloudMessageService extends FirebaseMessagingService {
    private static final String TAG = "Tag";

    public CloudMessageService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String message = "";
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            message = remoteMessage.getData().get("message");
            Log.d(TAG, "Message data payload: " + message);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        try {
//            JSONObject jsunArrObj = new JSONObject(message);
            JSONArray jsunArrObj = new JSONArray(message);
            JSONObject jsunObj = (JSONObject) jsunArrObj.get(0);
            mTitle = jsunObj.getString("title");
            mName = jsunObj.getString("name");
            mStatus = jsunObj.getString("status");
            mOrder_id = jsunObj.getString("order_id");
            sendNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String mTitle;
    String mStatus;
    String mName;
    String mOrder_id;


    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification() {

        Intent intent = new Intent(this, OrderDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra(Constants.GCM_ORDER_TITLE, mTitle);
        intent.putExtra(Constants.GCM_ORDER_NAME, mName);
        intent.putExtra(Constants.GCM_ORDER_STATUS, mStatus);
        intent.putExtra(Constants.GCM_ORDER_ID, mOrder_id);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

// this is a my insertion looking for a solution
        int icon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.drawable.zraya_logo : R.drawable.zraya_logo;
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(mName)
                .setContentText(mStatus)
                .setContentInfo(getBaseContext().getResources().getString(R.string.app_name))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
