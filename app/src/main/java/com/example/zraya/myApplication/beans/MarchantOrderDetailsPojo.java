package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Raj on 7/10/2016.
 */
public class MarchantOrderDetailsPojo implements Serializable {
    @SerializedName("order_detail")
    private OrderInfo aOrderInfo;
    @SerializedName("product_detail")
    private OfficeTablePojo aOfficeTablepojo;

    public OrderInfo getaOrderInfo() {
        return aOrderInfo;
    }

    public void setaOrderInfo(OrderInfo aOrderInfo) {
        this.aOrderInfo = aOrderInfo;
    }

    public OfficeTablePojo getaOfficeTablepojo() {
        return aOfficeTablepojo;
    }

    public void setaOfficeTablepojo(OfficeTablePojo aOfficeTablepojo) {
        this.aOfficeTablepojo = aOfficeTablepojo;
    }
}
