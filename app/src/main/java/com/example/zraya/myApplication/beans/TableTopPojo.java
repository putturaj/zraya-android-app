package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 24-Mar-17.
 */

public class TableTopPojo extends IJRModelData {
    @SerializedName("tabletopID")
    private String id;

    @SerializedName("title")
    private String name;

    @SerializedName("topcolor")
    private String topColor;

    @SerializedName("beidingcolor")
    private String beiding_color;

    @SerializedName("length")
    private String length;

    @SerializedName("width")
    private String width;

    @SerializedName("imageurl")
    private String imageurl;

    @SerializedName("productid")
    private String productid;

    @SerializedName("category")
    private String category;

    @SerializedName("amount")
    private String price;

    @SerializedName("imageurls")
    private String imageurls;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTopColor() {
        return topColor;
    }

    public void setTopColor(String topColor) {
        this.topColor = topColor;
    }

    public String getBeiding_color() {
        return beiding_color;
    }

    public void setBeiding_color(String beiding_color) {
        this.beiding_color = beiding_color;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageurls() {
        return imageurls;
    }

    public void setImageurls(String imageurls) {
        this.imageurls = imageurls;
    }
}