package com.example.zraya.myApplication.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Window;

import com.astuetz.PagerSlidingTabStrip;
import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.adapters.OrderListTabAdapter;
import com.example.zraya.myApplication.beans.UserInfoPojo;

import java.util.HashMap;


/**
 * Created by Rajesh on 22-Apr-16.
 */
public class OrderListActivity extends Activity {
    private ViewPager mViewPager;
    private OrderListTabAdapter mAdapter;
    UserSessionManager mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        mViewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        mSession = new UserSessionManager(getApplicationContext());
        setupViewPager(mViewPager);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTextColorResource(R.color.color_white);
    }

    private void setupViewPager(ViewPager aViewPager) {
//        Bundle extras = getIntent().getExtras();
//        String userEmail = extras.getString(Constants.USER_EMAIL);
//        String userID = extras.getString(Constants.USER_ID);


        // get user data from session
        HashMap<String, String> user = mSession.getUserDetails();
        System.out.println("TOKEN ID " + mSession.getToken());
        String userEmail = user.get(UserSessionManager.USER_EMAIL);
        String userID = user.get(UserSessionManager.USER_ID);

        String tabTitles[] = new String[]{Constants.TAB_ON_GOING_ORDERS, Constants.TAB_COMPLETED, Constants.TAB_CANCELLED};
        mAdapter = new OrderListTabAdapter(getFragmentManager());
        mAdapter.setTabs(tabTitles);
        UserInfoPojo userInfo = new UserInfoPojo();
        mAdapter.setUserDetails(userInfo);
        mAdapter.setUserEmail(userEmail);
        mAdapter.setUserID(userID);
        aViewPager.setAdapter(mAdapter);
    }

}
