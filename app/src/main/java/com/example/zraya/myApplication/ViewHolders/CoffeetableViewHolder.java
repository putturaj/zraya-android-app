package com.example.zraya.myApplication.ViewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.CafetariaPojo;
import com.example.zraya.myApplication.beans.IJRModelData;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersData;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by puttu on 24-Mar-17.
 */

public class CoffeetableViewHolder {
    UserSessionManager mSession;
    String mIsMerchant;

    ImageView mImageView;
    TextView mName, mLength, mOrderdate;


    private View container;
    CafetariaPojo mCafetariaPojo = null;
    OrdersData mOrderData = null;
    OrderInfo orderInfo = null;
    UserInfoPojo userInfoPojo = null;
    Context mContext;

    public CoffeetableViewHolder(View itemView, Context mContext) {
        container = itemView;
        this.mContext = mContext;
        mSession = new UserSessionManager(mContext);
        HashMap<String, String> user = mSession.getUserDetails();
        mIsMerchant = user.get(UserSessionManager.USER_IS_ADMIN);

        mImageView = (ImageView) container.findViewById(R.id.prodImg);

        mName = (TextView) container.findViewById(R.id.name);

        mLength = (TextView) container.findViewById(R.id.width);
        mOrderdate = (TextView) container.findViewById(R.id.ordered_date);
    }

    public void updateUI(int position, IJRModelData ijrModelData) {
        mOrderData = (OrdersData) ijrModelData;
        orderInfo = mOrderData.getOrderObj();
        userInfoPojo = mOrderData.getUserObj();
        mCafetariaPojo = (CafetariaPojo) mOrderData.getProductObject();


        Date date = Utils.getDate(orderInfo.getDate());
        mName.setText(mCafetariaPojo.getName());
        mLength.setText(mCafetariaPojo.getLength());


        if (null != mCafetariaPojo.getImgurl() && !Utils.isEmpty(mCafetariaPojo.getImgurl())) {
            try {
                Picasso.with(mContext).load(Utils.getUrlEncode(mCafetariaPojo.getImgurl())).into(mImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            Picasso.with(mContext).load(Constants.zrayaLogo).into(mImageView);
        }
        mOrderdate.setText(orderInfo.getDate());


        container.setClickable(true);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, mCafetariaPojo);
                intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
                intent.putExtra(Constants.BUNDLE_ORDER_DATA, mOrderData);
                mContext.startActivity(intent);
            }
        });
    }
}
