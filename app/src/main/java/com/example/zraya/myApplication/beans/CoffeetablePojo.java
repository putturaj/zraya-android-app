package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

public class CoffeetablePojo extends IJRModelData {
    @SerializedName("basemeterial")
    private String basemeterial;

    @SerializedName("width")
    private String width;

    @SerializedName("imageUrls")
    private String[] imageUrls;

    @SerializedName("date_created")
    private String date_created;

    @SerializedName("basecolor")
    private String basecolor;

    @SerializedName("coffeetableID")
    private String coffeetableID;

    @SerializedName("shape")
    private String shape;

    @SerializedName("topmeterial")
    private String topmeterial;

    @SerializedName("Name")
    private String Name;

    @SerializedName("amount")
    private String amount;

    @SerializedName("category")
    private String category;

    @SerializedName("height")
    private String height;

    @SerializedName("topcolor")
    private String topcolor;

    @SerializedName("productid")
    private String productid;

    @SerializedName("name")
    private String name;

    @SerializedName("length")
    private String length;

    @SerializedName("brand")
    private String brand;

    @SerializedName("imageurl")
    private String imageurl;

    @SerializedName("storage_include")
    private String storage_include;


    public String getBasemeterial() {
        return basemeterial;
    }

    public void setBasemeterial(String basemeterial) {
        this.basemeterial = basemeterial;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String[] getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(String[] imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getBasecolor() {
        return basecolor;
    }

    public void setBasecolor(String basecolor) {
        this.basecolor = basecolor;
    }

    public String getCoffeetableID() {
        return coffeetableID;
    }

    public void setCoffeetableID(String coffeetableID) {
        this.coffeetableID = coffeetableID;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getTopmeterial() {
        return topmeterial;
    }

    public void setTopmeterial(String topmeterial) {
        this.topmeterial = topmeterial;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getStorage_include() {
        return storage_include;
    }

    public void setStorage_include(String storage_include) {
        this.storage_include = storage_include;
    }

    @Override
    public String toString() {
        return "ClassPojo [basemeterial = " + basemeterial + ", width = " + width + ", imageUrls = " + imageUrls + ", date_created = " + date_created + ", basecolor = " + basecolor + ", coffeetableID = " + coffeetableID + ", shape = " + shape + ", topmeterial = " + topmeterial + ", Name = " + Name + ", amount = " + amount + ", category = " + category + ", height = " + height + ", topcolor = " + topcolor + ", productid = " + productid + ", name = " + name + ", length = " + length + ", brand = " + brand + ", imageurl = " + imageurl + ", storage_include = " + storage_include + "]";
    }
}
