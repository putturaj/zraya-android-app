package com.example.zraya.myApplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrdersWorkstationDataPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.beans.WorkstationPojo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class WorkstationOrderListDisplayAdapter extends ArrayAdapter<OrdersWorkstationDataPojo> {

    private Context mContext;
    /***********
     * Declare Used Variables
     *********/

    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    public String userID;
    OrdersWorkstationDataPojo mOrderWorkstationOrderData = null;
    WorkstationPojo aWorkstationPojo = null;
    OrderInfo aOrderInfo = null;
    UserInfoPojo aOrderUserInfo = null;


    /*************
     * CustomAdapter Constructor
     *****************/
    public WorkstationOrderListDisplayAdapter(Context aContext, ArrayList<OrdersWorkstationDataPojo> aWorkstationOrderPojo, String aUserId) {
        super(aContext, 0, aWorkstationOrderPojo);
        data = aWorkstationOrderPojo;
        userID = aUserId;
        inflater = (LayoutInflater) aContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = aContext;
    }


    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public static class ViewHolder {

        public ImageView mWorksatitionImage;
        public TextView mWorksationName;
        public TextView mWorkstationTopMat;
        //        public TextView mWorkstationBaseMat;
        public TextView mWorkStationPerSeatSpace;
        public TextView mWorkstationPartition;
        public TextView mColorsFields;
        public TextView mWorkstationBasecolor;
        public TextView mWorkstationDeliverydate;
        public TextView mWorkstationPrice;

    }

    /******
     * Depends upon data size called for each row , Create each ListView row
     *****/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StringBuffer colorfeilds = new StringBuffer();
        View itemView = convertView;
        ViewHolder holder;
        aWorkstationPojo = null;
        mOrderWorkstationOrderData = (OrdersWorkstationDataPojo) data.get(position);
        aWorkstationPojo = mOrderWorkstationOrderData.getWorkstationPojo();
        aOrderInfo = mOrderWorkstationOrderData.getOrderObj();
        aOrderUserInfo = mOrderWorkstationOrderData.getUserObj();

        colorfeilds.append(aWorkstationPojo.getWs_topcolor() + " top, " + aWorkstationPojo.getWs_basecolor() + " base, " +
                aWorkstationPojo.getWs_beidingcolor() + " Beiding Color");

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = itemView = inflater.inflate(R.layout.workstation_order_list_details, null);
        }
        // Lookup view for data population
        holder = new ViewHolder();
        holder.mWorksationName = (TextView) itemView.findViewById(R.id.ws_table_name);
        holder.mWorksatitionImage = (ImageView) itemView.findViewById(R.id.prodImg);
        if (data.size() <= 0) {
            holder.mWorksationName.setText("No Data");
        } else {
            /***** Get each Model object from Arraylist ********/

            /************  Set Model values in Holder elements ***********/
            holder.mWorksationName.setText(aWorkstationPojo.getWs_name());
            holder.mWorkstationPartition.setText(aWorkstationPojo.getWs_partition());
            holder.mColorsFields.setText(colorfeilds.toString());
            holder.mWorkstationDeliverydate.setText("NO date Now");
            if (null != aWorkstationPojo.getWs_img_url()) {
                Picasso.with(mContext).load(Constants.baseUrl + "" + aWorkstationPojo.getWs_img_url()).into(holder.mWorksatitionImage);
            } else {
                Picasso.with(mContext).load(Constants.zrayaLogo).into(holder.mWorksatitionImage);
            }
        }


        itemView.setClickable(true);
        itemView.setOnClickListener(new OnItemClickListener(position, mOrderWorkstationOrderData));
        return itemView;
    }


    /*********
     * Called when Item click in ListView
     ************/
    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;
        private OrdersWorkstationDataPojo orderData;
        private WorkstationPojo workstationPojo;
        private OrderInfo orderInfo;
        private UserInfoPojo userInfoPojo;

        OnItemClickListener(int position, OrdersWorkstationDataPojo aWorkstationOrdersDataPojo) {
            mPosition = position;
            orderData = aWorkstationOrdersDataPojo;
            workstationPojo = orderData.getWorkstationPojo();
            orderInfo = orderData.getOrderObj();
            userInfoPojo = orderData.getUserObj();
        }

        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent(mContext, OrderDetailsActivity.class);
            intent.putExtra(Constants.BUNDLE_ORDER_DATA, orderData);
            intent.putExtra(Constants.USER_ID, userInfoPojo.getUser_id());
            mContext.startActivity(intent);
        }
    }
}
