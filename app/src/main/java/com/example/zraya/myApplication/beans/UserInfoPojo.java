package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rajesh on 03-Jun-16.
 */
public class UserInfoPojo implements Serializable, Cloneable {

    @SerializedName("UserID")
    private String user_id;

    @SerializedName("UserName")
    private String user_name;

    @SerializedName("UserEmail")
    private String user_email;

    @SerializedName("Password")
    private String user_pwd;

    @SerializedName("UserPhoneNo")
    private String user_phNo;

    @SerializedName("UserLocation")
    private String user_location;

    @SerializedName("Comment")
    private String user_comment;

    @SerializedName("IsAdmin")
    private String user_isAdmin;

    @SerializedName("ordersCount")
    private String user_orders_count;

    private TokenPojo tokenpojo;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_phNo() {
        return user_phNo;
    }

    public void setUser_phNo(String user_phNo) {
        this.user_phNo = user_phNo;
    }

    public String getUser_location() {
        return user_location;
    }

    public void setUser_location(String user_location) {
        this.user_location = user_location;
    }

    public String getUser_comment() {
        return user_comment;
    }

    public void setUser_comment(String user_comment) {
        this.user_comment = user_comment;
    }

    public String getUser_isAdmin() {
        return user_isAdmin;
    }

    public void setUser_isAdmin(String user_isAdmin) {
        this.user_isAdmin = user_isAdmin;
    }

    public String getUser_orders_count() {
        return user_orders_count;
    }

    public void setUser_orders_count(String user_orders_count) {
        this.user_orders_count = user_orders_count;
    }


    public TokenPojo getTokenpojo() {
        return tokenpojo;
    }

    public void setTokenpojo(TokenPojo tokenpojo) {
        this.tokenpojo = tokenpojo;
    }
    public UserInfoPojo clone() throws CloneNotSupportedException {
        return (UserInfoPojo)super.clone();
    }

}
