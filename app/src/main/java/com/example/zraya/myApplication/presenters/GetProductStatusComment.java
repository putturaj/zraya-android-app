package com.example.zraya.myApplication.presenters;


import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.PostObjects;
import com.example.zraya.myApplication.activities.OrderDetailsActivity;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.beans.OrderStatusPojo;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.http.HttpEngine;
import com.example.zraya.myApplication.http.HttpListener;
import com.example.zraya.myApplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 27-Apr-16.
 */
public class GetProductStatusComment implements HttpListener {

    RequestActionType mRequestActionType;
    OrderDetailsActivity orderDetail;
    PostObjects mPostObjects = new PostObjects();

    public GetProductStatusComment(OrderDetailsActivity aOrderDetailActivity) {
        this.orderDetail = aOrderDetailActivity;
    }

    public void requestToServer(RequestActionType aRequestActionType, OrderInfo aOrderData) {
        this.mRequestActionType = aRequestActionType;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;
        Map<String, String> headers = new HashMap<String, String>();

        String postData = Constants.EMPTY_STRING;
        postData = mPostObjects.createOrderDataPostBody(aOrderData).toString();
        url = Constants.GET_PRODUCT_STATUS_COMMENT;
        httpRequest = new HttpRequest(url, postData, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        JSONObject json_data;
        System.out.println("ZRAYA : Present in the handleHttpResponse : " + mRequestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            ArrayList<OrderStatusPojo> orderItems = null;
            orderDetail.responseGetProductStatusComment(orderItems);
            System.out.println("ZRAYA : Present in the handleHttpResponse : Empty response");
        } else {
            try {
                String str1 = new String(httpRequest.content);
                System.out.println("Response is : " + str1);
                String str = new String(httpRequest.content, "UTF-8");
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.enableComplexMapKeySerialization().create();
                ArrayList<OrderStatusPojo> orderItems = (ArrayList<OrderStatusPojo>) gson.fromJson(str,
                        new TypeToken<ArrayList<OrderStatusPojo>>() {
                        }.getType());
                orderDetail.responseGetProductStatusComment(orderItems);
            } catch (Exception e) {
                System.out.println("Exception : in Response" + e.toString());
            }
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("ZRAYA : Present in the handleHttpException : " + e.getMessage());
    }
}
