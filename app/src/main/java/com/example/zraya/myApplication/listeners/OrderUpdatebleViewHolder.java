package com.example.zraya.myApplication.listeners;

import android.content.Context;

import com.example.zraya.myApplication.beans.BaseItem;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public interface OrderUpdatebleViewHolder {
    void updateViewHolder(Context context, BaseItem newsItem);
}
