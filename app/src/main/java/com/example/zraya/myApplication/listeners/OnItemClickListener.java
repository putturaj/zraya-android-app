package com.example.zraya.myApplication.listeners;

import android.view.View;

import com.example.zraya.myApplication.beans.OfficeTablePojo;
import com.example.zraya.myApplication.beans.OrderInfo;
import com.example.zraya.myApplication.entities.HomeActionType;

/**
 * Created by Rajesh on 26-Apr-16.
 */
public interface OnItemClickListener {
    void onItemClick(View view, OfficeTablePojo newsObj, HomeActionType actionType);
}
