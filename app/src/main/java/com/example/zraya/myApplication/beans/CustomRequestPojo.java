package com.example.zraya.myApplication.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by puttu on 01-Jun-17.
 */

public class CustomRequestPojo extends IJRModelData {
    @SerializedName("customUploadOrderID")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("meterial")
    private String meterial;
    @SerializedName("floorplanurl")
    private String floorplan;
    @SerializedName("customImageurl")
    private String customImageurl;
    @SerializedName("dod")
    private String dod;
    @SerializedName("requirement")
    private String requirement;

    public String getMeterial() {
        return meterial;
    }

    public void setMeterial(String meterial) {
        this.meterial = meterial;
    }

    public String getId() {
        return id;

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloorplan() {
        return floorplan;
    }

    public void setFloorplan(String floorplan) {
        this.floorplan = floorplan;
    }

    public String getCustomImageurl() {
        return customImageurl;
    }

    public void setCustomImageurl(String customImageurl) {
        this.customImageurl = customImageurl;
    }

    public String getDod() {
        return dod;
    }

    public void setDod(String dod) {
        this.dod = dod;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }
}
