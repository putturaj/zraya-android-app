package com.example.zraya.myApplication.http;

/**
 * POJO for Proxy settings
 */

public class NHProxy {
    public String currentAPNName;
    public String proxyAddress;
    public String proxyPort;
}
