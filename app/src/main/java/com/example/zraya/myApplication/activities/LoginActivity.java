package com.example.zraya.myApplication.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zraya.R;
import com.example.zraya.myApplication.Utils.ConnectionDetector;
import com.example.zraya.myApplication.Utils.Constants;
import com.example.zraya.myApplication.Utils.UserSessionManager;
import com.example.zraya.myApplication.Utils.Utils;
import com.example.zraya.myApplication.beans.TokenPojo;
import com.example.zraya.myApplication.beans.UserInfoPojo;
import com.example.zraya.myApplication.dao.DBItemSQLDao;
import com.example.zraya.myApplication.dao.DBStatusSQLDao;
import com.example.zraya.myApplication.entities.RequestActionType;
import com.example.zraya.myApplication.presenters.MainActivityLoginPresenter;

import java.util.ArrayList;


/**
 * Created by Rajesh on 12-Apr-16.
 */

public class LoginActivity extends BaseDrawerActivity implements View.OnClickListener {

    public static String name = "";
    public static String email;

    EditText mLoginEmail, mLoginPassword;
    Button mSingin;
    TextView mRegistration, mForgotPwd;
    protected TextView title;
    protected ImageView icon;

    DBItemSQLDao sqlNewsDao;
    DBStatusSQLDao sqlStatusDao;
    String aUserEmail, aUserPwd;
    Context context = this;
    UserSessionManager session;

    // Connection detector
    ConnectionDetector cd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        showNavigationDrawer(true);
        session = new UserSessionManager(getApplicationContext());
        creatingDBtables();
        initViews();
    }


    private void initViews() {
        title = (TextView) findViewById(R.id.title);
        icon = (ImageView) findViewById(R.id.icon);

        mLoginEmail = (EditText) findViewById(R.id.LoginEmail);
        mLoginPassword = (EditText) findViewById(R.id.loginPassword);
        mRegistration = (TextView) findViewById(R.id.new_registration);
        mForgotPwd = (TextView) findViewById(R.id.login_forgot_password);

        mSingin = (Button) findViewById(R.id.login);
        mSingin.setOnClickListener(this);
        mRegistration.setOnClickListener(this);
        mForgotPwd.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == mSingin) {
            aUserEmail = mLoginEmail.getText().toString();
            aUserPwd = mLoginPassword.getText().toString();
            loginUser(aUserEmail, aUserPwd);
        } else if (v == mRegistration) {
            Intent intent = null;
            intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivityForResult(intent, 1004);
        } else if (v == mForgotPwd) {
            Intent intent = null;
            intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
            startActivityForResult(intent, 1004);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<UserInfoPojo> userRegister = null;
        if (resultCode == RESULT_OK) {
            userRegister = (ArrayList<UserInfoPojo>) data.getSerializableExtra("register");
            responseLoginPresenter(userRegister);
        }
    }

    public void loginUser(String aEmail, String aPwd) {
        if (!(Utils.isEmpty(aEmail) && Utils.isEmpty(aPwd))) {
            MainActivityLoginPresenter mainActivityLoginPresenter = new MainActivityLoginPresenter(context);
            UserInfoPojo aUserInfoPojo = new UserInfoPojo();
            TokenPojo tokenPojo = new TokenPojo();
            aUserEmail = aEmail;
            aUserPwd = aPwd;
            aUserInfoPojo.setUser_email(aUserEmail);
            aUserInfoPojo.setUser_pwd(aUserPwd);
            tokenPojo.setToken(session.getToken());
            aUserInfoPojo.setTokenpojo(tokenPojo);
            mainActivityLoginPresenter.requestToServer(RequestActionType.LOGIN, aUserInfoPojo);
        } else {
            Toast.makeText(LoginActivity.this, "Email And Password fields is Empty", Toast.LENGTH_SHORT).show();
        }
    }

    public void responseLoginPresenter(ArrayList<UserInfoPojo> aUserinfo) {
        UserInfoPojo userPojo = null;
        for (int i = 0; i < aUserinfo.size(); i++) {
            userPojo = aUserinfo.get(i);
            session.createUserLoginSession(userPojo);
        }
        checkingUserType(userPojo);
    }

    private void checkingUserType(UserInfoPojo aUserPojo) {
        Intent intent = null;
        if (aUserPojo.getUser_isAdmin().equals(Constants.USER)) {
            intent = new Intent(LoginActivity.this, OrderListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, aUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, aUserPojo.getUser_id());
        } else if (aUserPojo.getUser_isAdmin().equals(Constants.ADMIN)) {
            intent = new Intent(LoginActivity.this, AdminOrderListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, aUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, aUserPojo.getUser_id());
        } else if (aUserPojo.getUser_isAdmin().equals(Constants.MARCHANT)) {
            intent = new Intent(LoginActivity.this, MarchantListActivity.class);
            intent.putExtra(Constants.USER_EMAIL, aUserPojo.getUser_email());
            intent.putExtra(Constants.USER_ID, aUserPojo.getUser_id());
        }
        startActivity(intent);
        finish();
    }

    //    DB Initialisation
    private void creatingDBtables() {
        sqlNewsDao = new DBItemSQLDao(getApplicationContext());
        sqlStatusDao = new DBStatusSQLDao(getApplicationContext());
        sqlNewsDao.open();
        sqlStatusDao.open();
    }
}
